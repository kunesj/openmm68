OpenMM68
========
OpenMM68 is (very experimental) attempt at recreating the engine used by "Might and Magic VI", "Might and Magic VII", "Might and Magic VIII" games.

### License
GNU GPL3 (see LICENSE.txt for more information)

### Status
- Rewriting from old buggy Python2 version
- Can render exterior terrain, models, sprites. + some animation and model attributes. [screenshot_1](http://imgur.com/0nvW1aO), [screenshot_2](http://imgur.com/iXtUydQ)

Instalation
-----------
### Required Python Libraries
Python 3, Numpy, Pillow, Panda3D

Requires Panda3D library that is compilled for Python 3!

### Linux (Ubuntu)

Install dependencies
```
make install_dep
```

If there is already [Panda3D](https://www.panda3d.org/) release that is compilled for Python 3, download and install it.

If there is not, follow steps for building it yourself at [Panda3D GitHub](https://github.com/panda3d/panda3d/blob/master/README.md). It's easy.

Install application
```
make install
```

### Linux (Other)

Look into Makefile too see what libraries you need to install (dont forget Panda3D compilled for Python3). After that install application with:
```
make install
```

### Windows / OSX

Fully Manual Installation.

Good luck with that on Windows.
```
¯\_(ツ)_/¯
```

Usage
-----
Some subpackages can be run by themselves.

### OpenMM68
```
usage: openmm68 [-h] [-d {50,40,30,20,10,1}] [--debug_lod {50,40,30,20,10,1}]
                [paths [paths ...]]

OpenMM68

positional arguments:
  paths                 Paths to resource archives

optional arguments:
  -h, --help            show this help message and exit
  -d {50,40,30,20,10,1}, --debug {50,40,30,20,10,1}
                        Set global debug level [CRITICAL=50, ERROR=40,
                        WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level
                        is WARNING.
  --debug_lod {50,40,30,20,10,1}
                        Set openmm68.lod module debug level. Overrides global
                        debug level.
```

### OpenMM68.VFSManager
```
usage: openmm68-vfs [-h] [-l] [-e EXTRACT] [-d {50,40,30,20,10,1}]
                    [paths [paths ...]]

openmm68.vfs.manager

positional arguments:
  paths                 Paths to resource archives

optional arguments:
  -h, --help            show this help message and exit
  -l, --list            List files in VFS
  -e EXTRACT, --extract EXTRACT
                        Extract file with specifited path from VFS to current
                        dir
  -d {50,40,30,20,10,1}, --debug {50,40,30,20,10,1}
                        Set global debug level [CRITICAL=50, ERROR=40,
                        WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level
                        is WARNING.
```

### OpenMM68.ResourceSystem
```
usage: openmm68-resource [-h] [-l] [-e EXTRACT]
                         [--extractbitmap EXTRACTBITMAP]
                         [--extractsprite EXTRACTSPRITE]
                         [-d {50,40,30,20,10,1}]
                         [paths [paths ...]]

openmm68.resource.resourcesystem

positional arguments:
  paths                 Paths to resource archives

optional arguments:
  -h, --help            show this help message and exit
  -l, --list            List paths to resource archives
  -e EXTRACT, --extract EXTRACT
                        Extract file with specifited path to current dir
  --extractbitmap EXTRACTBITMAP
                        Extract bitmap with specifited path to current dir
  --extractsprite EXTRACTSPRITE
                        Extract sprite with specifited path to current dir
  -d {50,40,30,20,10,1}, --debug {50,40,30,20,10,1}
                        Set global debug level [CRITICAL=50, ERROR=40,
                        WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level
                        is WARNING.
```

### OpenMM68.Lod
```
usage: openmm68-lod [-h] [-i] [-l] [-e EXTRACT]
                    [--extractbitmap EXTRACTBITMAP]
                    [--extractsprite EXTRACTSPRITE] [--extractall]
                    [-d {50,40,30,20,10,1}]
                    lodfile

openmm68.lod.lodfile

positional arguments:
  lodfile               Path to LOD file

optional arguments:
  -h, --help            show this help message and exit
  -i, --info            Print info about LOD file
  -l, --list            List files in LOD file
  -e EXTRACT, --extract EXTRACT
                        Extract file with specifited filename from LOD file to
                        current dir
  --extractbitmap EXTRACTBITMAP
                        Extract bitmap file with specifited filename from LOD
                        file to current dir and convert it to PNG
  --extractsprite EXTRACTSPRITE
                        Extract sprite file with specifited filename from LOD
                        file to current dir and convert it to PNG. Requires
                        palette "pall???" file in current directory
  --extractall          Extracts all files from LOD file into folder with same
                        name
  -d {50,40,30,20,10,1}, --debug {50,40,30,20,10,1}
                        Set global debug level [CRITICAL=50, ERROR=40,
                        WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level
                        is WARNING.
```

### OpenMM68.ScriptEngine.EvtDecompiler
```
usage: evt_decompiler.py [-h] [-l] [-e EVTFILE] [-v MMVERSION] [-a]
                         [--extract] [--dump] [--decompile]
                         [-d {50,40,30,20,10,1}]
                         [paths [paths ...]]

openmm68.scriptengine.evt

positional arguments:
  paths                 Paths to resource archives

optional arguments:
  -h, --help            show this help message and exit
  -l, --list            List paths to resources
  -e EVTFILE, --evtfile EVTFILE
                        Path to evt file
  -v MMVERSION, --mmversion MMVERSION
                        Version of might and magic used by evt files (6,7,8)
  -a, --allevtfiles     Process all evt files
  --extract             Extract evt file to current dir
  --dump                Dump data from evt file to current dir
  --decompile           Decompile evt file to current dir
  -d {50,40,30,20,10,1}, --debug {50,40,30,20,10,1}
                        Set global debug level [CRITICAL=50, ERROR=40,
                        WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level
                        is WARNING.
```
