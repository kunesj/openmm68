CREDITS
=======

* LOD and ODM loading code is based on 'Might & Magic 6-8 map viewer'
    URL: https://github.com/angeld29/mm_mapview2
    License: GNU GPL2
    Author: Angel
        Email: angel.d.death@gmail.com
        WWW: http://sites.google.com/site/angelddeath/
             http://angel-death.newmail.ru/

