#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from openmm68.vfs.vfsmanager import VFSManager

from openmm68.resource.filemanager import FileManager
from openmm68.resource.imagemanager import ImageManager
from openmm68.resource.texturemanager import TextureManager
from openmm68.resource.configfilemanager import ConfigFileManager

class ResourceSystem(object):
    """ Manages all resource managers """

    def __init__(self, vfs_manager):
        self.vfs = vfs_manager
        self.resource_managers = []

        self.file_manager = FileManager(vfs_manager)
        self.addResourceManager(self.file_manager)

        self.image_manager = ImageManager(vfs_manager)
        self.addResourceManager(self.image_manager)

        self.texture_manager = TextureManager(self.image_manager)
        self.addResourceManager(self.texture_manager)

        self.configfile_manager = ConfigFileManager(vfs_manager)
        self.addResourceManager(self.configfile_manager)

    @classmethod
    def getObject(cls):
        man = VFSManager()
        man.buildIndex()
        return cls(man)

    def analyzeMemoryUsage(self, silent=False):
        if not silent: print("Analyzing ResourceSystem memory usage...")

        # Get resource managers cache usage
        if not silent: print("Analyzing ResourceManagers cache memory usage...")
        sum_usage = 0; sum_num = 0
        for i, rm in enumerate(self.resource_managers):
            rm_class = rm.__class__.__name__
            try:
                usage, num = rm.getCacheMemoryUsage()
                sum_usage += usage; sum_num += num
            except Exception:
                if not silent: logger.exception("ResourceManager %i -> class = %s, Couldn't get memory usage!" % (i, rm_class))
            else:
                if not silent: print("ResourceManager %i -> class = %s, cache = %i bytes, objects = %i" % (i, rm_class, usage, num))
        if not silent: print("All ResourceManagers -> caches_sum = %i bytes, objects_sum = %i" % (sum_usage, sum_num))

        # get VFS memory usage
        vfs_usage = self.vfs.analyzeMemoryUsage(silent=silent)
        sum_usage += vfs_usage

        # Print overall memory usage
        if not silent: print("ResourceSystem -> memory_sum = %i bytes" % (sum_usage))

        return sum_usage

    def addResourceManager(self, resource_manager):
        self.resource_managers.append(resource_manager)

    def getFileManager(self):
        return self.file_manager

    def getImageManager(self):
        return self.image_manager

    def getTextureManager(self):
        return self.texture_manager

    def getConfigFileManager(self):
        return self.configfile_manager

    def getVFS(self):
        return self.vfs

    def setExpiryDelay(self, expiry_delay):
        for rm in self.resource_managers:
            rm.setExpiryDelay(expiry_delay)

    def updateCache(self):
        """ Should be run periodicaly to delete old objects in cache """
        for rm in self.resource_managers:
            rm.updateCache()

""" Import this object variable in every file that needs to use ResourceSystem """
RESOURCE_SYSTEM = ResourceSystem.getObject()

def main():
    import argparse, os, io

    # Parasing input prarmeters
    parser = argparse.ArgumentParser(
        description='openmm68.resource.resourcesystem'
    )
    parser.add_argument(
        'paths', nargs='*',
        help='Paths to resource archives')
    parser.add_argument(
        '-l', '--list', action='store_true',
        help='List paths to resource archives')
    parser.add_argument(
        '-e', '--extract', default=None,
        help='Extract file with specifited path to current dir')
    parser.add_argument(
        '--extractbitmap', default=None,
        help='Extract bitmap with specifited path to current dir')
    parser.add_argument(
        '--extractsprite', default=None,
        help='Extract sprite with specifited path to current dir')
    parser.add_argument(
        '-d', '--debug', type=int, choices=[50, 40, 30, 20, 10, 1], default=None,
        help='Set global debug level [CRITICAL=50, ERROR=40, WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level is WARNING.')
    args = parser.parse_args()

    # Logger configuration
    logging.basicConfig()
    logger = logging.getLogger()
    if args.debug is not None:
        logger.setLevel(args.debug)
        logger.info("Set global debug level to: %i" % (args.debug,) )
    else:
        logger.setLevel(30)

    # add archives to VFS and build index
    RESOURCE_SYSTEM.getVFS().registerArchives(args.paths)
    RESOURCE_SYSTEM.getVFS().buildIndex()

    if args.list:
        RESOURCE_SYSTEM.getVFS().dumpVFSStructure()

    elif args.extract is not None:
        print("Extracting file: %s" % (args.extract,) )
        fm = RESOURCE_SYSTEM.getFileManager()
        stream = fm.getFile(args.extract)
        with open(os.path.basename(args.extract), 'wb') as f:
            f.write(stream.read())

    elif args.extractbitmap is not None:
        print("Extracting bitmap: %s" % (args.extractbitmap,) )
        iman = RESOURCE_SYSTEM.getImageManager()
        img = iman.getBitmap(args.extractbitmap)
        imgByteStr = io.BytesIO()
        img.save(imgByteStr, format='PNG')
        with open(args.extractbitmap+".png", 'wb') as f:
            imgByteStr.seek(0x0, io.SEEK_SET)
            f.write(imgByteStr.read())

    elif args.extractsprite is not None:
        print("Extracting sprite: %s" % (args.extractsprite,) )
        iman = RESOURCE_SYSTEM.getImageManager()
        img = iman.getSprite(args.extractsprite)
        imgByteStr = io.BytesIO()
        img.save(imgByteStr, format='PNG')
        with open(args.extractsprite+".png", 'wb') as f:
            imgByteStr.seek(0x0, io.SEEK_SET)
            f.write(imgByteStr.read())
