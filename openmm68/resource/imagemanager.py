#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import numpy as np
from PIL import Image # Pillow package

import openmm68.lod.lodtexture as LodTexture

from openmm68.resource.resourcemanager import ResourceManager

class ImageManager(ResourceManager):

    # Priority of image files. "lod_bitmap", "lod_sprite" and "lod_icon" should always be kept last. (last priority)
    # all compatible formats are listed on: https://pillow.readthedocs.io/en/3.2.x/handbook/image-file-formats.html
    FILE_TYPE_PRIORITY = [
        "dds", "tiff", "png", "jpg", "jpeg",
        "bmp", "pcx", "cur", "ico", # old windows formats
        "lod_bitmap", "lod_sprite", "lod_icon" # LODFile formats, should be last in list
        ]

    def createPlaceholderImage(self, width=128, height=128):
        """
        Creates pink WxH texture
        Input: width=128, height=128 of image to create
        Returns: PIL.Image()
        """
        cache_key = ":PLACEHOLDER[%ix%i]:" % (width, height)
        # return from cache if is in
        if self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # Create placeholder image
        pDest = np.zeros((width, height, 4), dtype=np.uint8)
        pDest[:,:,0] = 255
        pDest[:,:,1] = 0
        pDest[:,:,2] = 255
        pDest[:,:,3] = 255
        img = Image.fromarray(pDest, 'RGBA')

        # Add to cache
        self.cache.toCache(cache_key, img)

        return img

    def getBitmap(self, filename, cache=True):
        """
        Input: filename of bitmap
        Returns: PIL.Image()
        """
        filepath = "bitmaps/"+filename
        return self.getImageFromPath(filepath, cache)

    def getSprite(self, filename, cache=True):
        """
        Input: filename of sprite
        Returns: PIL.Image()
        """
        filepath = "sprites/"+filename
        return self.getImageFromPath(filepath, cache)

    def getIcon(self, filename, cache=True):
        """
        Input: filename of icon
        Returns: PIL.Image()
        """
        filepath = "icons/"+filename
        return self.getImageFromPath(filepath, cache)

    def getImageFromPath(self, filepath, cache=True):
        """
        Input: path of bitmap
        Returns: PIL.Image()
        """
        cache_key = self.getCacheKey(filepath)
        logger.debug("Getting image: filepath='%s', cache_key='%s'" % (filepath, cache_key))

        # return from cache if is in
        if cache and self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # get bitmap file based on extension insensitive filename
        used_filepath = self.getExtensionInsensitivePath(filepath)

        # Get Image object
        try:
            stream = self.vfs.get(used_filepath)
            file_type = self.vfs.getType(used_filepath).lower()

            if file_type == "lod_bitmap":
                img = LodTexture.parseLODBitmap(stream)

            elif file_type == "lod_sprite":
                # get palette stream
                palette_id = LodTexture.parsePaletteID(stream)
                palette_filename = "pal%s" % str(palette_id).zfill(3)
                palette_filepath = "bitmaps/"+palette_filename
                if not self.vfs.exists(palette_filepath):
                    logger.warning("Couldn't find palette '%s', defaulting to palette 'palxxx'" % palette_filepath)
                    palette_filepath = "bitmaps/palxxx"
                if not self.vfs.exists(palette_filepath):
                    raise Exception("Couldn't get even default palette 'palxxx'! Have you loaded bitmaps archive?")
                palette_stream = self.vfs.get(palette_filepath)

                img = LodTexture.parseLODSprite(stream, palette_stream)

            elif file_type == "lod_icon":
                raise NotImplementedError # TODO

            elif file_type in self.FILE_TYPE_PRIORITY and file_type not in ["lod_bitmap", "lod_sprite", "lod_icon"]:
                img = Image.open(stream)

            else:
                raise Exception("Reading image '%s' of type '%s' is not supported!" % (used_filepath, file_type))

        except Exception:
            logger.exception("Getting image '%s' failed! Using placeholder texture." % (used_filepath,))
            img = self.createPlaceholderImage()

        # Add Image object to cache
        if cache:
            self.cache.toCache(cache_key, img)

        return img
