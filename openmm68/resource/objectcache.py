#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

import pickle
import time

class ObjectCache(object):

    def __init__(self):
        self.cache_data = {}
        self.disabled = False

    def getDisabled(self):
        return self.disabled

    def setDisabled(self, disabled):
        """ Disables Cache """
        self.disabled = disabled

    def getMemoryUsage(self):
        """
        Tries to calculate memory usage of objects in cache.
        Can overestimate because of references.

        Output: (bytes_num, objects_num)
        """
        size_sum = 0
        for key in self.cache_data:
            obj_class = self.cache_data[key][0].__class__.__name__
            if obj_class == "Image" or obj_class.endswith("ImageFile"):
                # PIL.Image.Image is unpickable
                image = {
                    'pixels': self.cache_data[key][0].tostring(),
                    'size': self.cache_data[key][0].size,
                    'mode': self.cache_data[key][0].mode,
                }
                size_sum += len(pickle.dumps([image, self.cache_data[key][1]]))
            elif obj_class == "Texture":
                # panda3d.core.Texture is unpickable
                size_sum += self.cache_data[key][0].getExpectedRamImageSize()
            else:
                size_sum += len(pickle.dumps(self.cache_data[key]))

        return size_sum, len(self.cache_data)

    def toCache(self, key, cached_object, timestamp=None):
        """ Adds object to cache under specified key """
        if not self.disabled:
            if timestamp is None:
                timestamp == time.time()
            self.cache_data[key] = [cached_object, timestamp]

    def inCache(self, key):
        """ Returns True if object with given key is in cache """
        return key in self.cache_data

    def fromCache(self, key, update_timestamp=True):
        """ Returns object from cache and update it's timestamp """
        self.cache_data[key][1] = time.time()
        return self.cache_data[key][0]

    def delCache(self, key):
        """ Removes object with specified key from cache """
        if self.inCache(key):
            del(self.cache_data[key])
        else:
            logger.debug("delCache(%s): Object was already removed from cache!" % (key,))

    def clearCache(self):
        """ Removes all objects from cache """
        logger.debug("clearCache()")
        self.cache_data = {}

    def removeExpired(self, expiry_delay=0.0):
        """ Removes objects that have timestamp+expiry_delay older then current time """
        logger.debug("removeExpired(%f)" % (expiry_delay,))
        this_time = time.time()
        for key in self.cache_data:
            if self.cache_data[key][1]+expiry_delay < this_time:
                del(self.cache_data[key])
