#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import io
from PIL import Image # Pillow package
import numpy as np

from panda3d.core import PNMImage, StringStream, Texture, SamplerState

from openmm68.resource.resourcemanager import ResourceManager

class TextureManager(ResourceManager):

    def __init__(self, image_manager):
        self.image_manager = image_manager
        super().__init__(self.image_manager.getVFS())

    @staticmethod
    def imageDilatationRGB(img):
        """
        Used to fix transparent color bluring into visible pixels in Panda3D

        It's not dilatation (image processing) but 'dilatation'

        Input: PIL Image
        Output: PIL Image
        """
        # skip images without alpha channel
        if img.mode != "RGBA":
            return img

        backimg = img.copy()

        # corners
        backimg.paste(img, (-1, -1), img )
        backimg.paste(img, (-1,  1), img )
        backimg.paste(img, (1,  -1), img )
        backimg.paste(img, (1,   1), img )
        # left-right-top-bottom
        backimg.paste(img, (0, 1), img )
        backimg.paste(img, (0, -1), img )
        backimg.paste(img, (1, 0), img )
        backimg.paste(img, (-1, 0), img )
        # paste original image in center
        backimg.paste(img, (0, 0), img )
        # set original alpha mask
        alpha = img.split()[-1]
        backimg.putalpha(alpha)

        return backimg

    @classmethod
    def image2Texture(cls, img, transparent_dilate=False):
        """
        Input: PIL Image
        Output: Panda3D Texture
        """
        # Fix transparent color bluring into visible pixels, by creating border of correctly colored pixels around visible ones.
        # Not required when filters are set to SamplerState.FT_nearest
        if transparent_dilate:
            img = cls.imageDilatationRGB(img)

        # get PNG byte string
        ram_file = io.BytesIO()
        img.save(ram_file, "PNG") # https://www.panda3d.org/manual/index.php?title=Transparency_and_Blending&oldid=4702
        contents = ram_file.getvalue()
        ram_file.close()

        # create PNMImage
        pnimg = PNMImage()
        pnimg.read(StringStream(contents))

        # create Texture
        tex = Texture()
        tex.load(pnimg)

        return tex

    def getPlaceholderTexture(self, width=128, height=128):
        cache_key = ":PLACEHOLDER[%ix%i]:" % (width, height)
        # return from cache if is in
        if self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # Get placeholder texture
        img = self.image_manager.createPlaceholderImage(width=width, height=height)
        tex = self.image2Texture(img)

        # Add to cache
        self.cache.toCache(cache_key, tex)

        return tex

    def getTexture(self, filepath, background_filepath=None, cache=True, img_cache=True):
        """ Input: full VFS path """
        cache_key = self.getCacheKey(filepath, background_filepath)
        logger.log(1, "Getting texture: filepath='%s', background_filepath=%s, cache_key='%s'" % (filepath, background_filepath, cache_key))

        # Return Texture from cache (if exists)
        if cache and self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # Get Image
        img = self.image_manager.getImageFromPath(filepath, cache=img_cache)

        # Get background Image and put it behind foreground image
        if background_filepath is not None:
            # needs to use .copy() to not edit Image saved in cache
            backimg = self.image_manager.getImageFromPath(background_filepath, cache=img_cache).copy()
            backimg.paste(img, (0, 0), img )
            img = backimg

        # Create Texture from Image
        tex = self.image2Texture(img)

        # Add Texture to cache (Replaces Image object added by parent class)
        if cache:
            self.cache.toCache(cache_key, tex)

        return tex

    def getSpriteTexture(self, filepath, cache=True, img_cache=True, pixelate=False):
        """
        Input:
            filepath - full VFS path
            cache - t/f
            img_cache - t/f
            pixelate - nearest / bluring filter
        """
        cache_key = self.getCacheKey(filepath, ":SPRITE:")
        logger.log(1, "Getting sprite texture: filepath='%s', cache_key='%s'" % (filepath, cache_key))

        # Return Texture from cache (if exists)
        if cache and self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # Create Texture from Image
        img = self.image_manager.getImageFromPath(filepath, cache=img_cache)
        tex = self.image2Texture(img, transparent_dilate=not pixelate)

        # fix thin border bug (at sides of img), https://www.panda3d.org/manual/index.php/Texture_Wrap_Modes
        # breaks ODM Models => they need Texture.WM_repeat
        tex.setWrapU(Texture.WM_clamp)
        tex.setWrapV(Texture.WM_clamp)

        # Make sprites pixelated
        # fixes transparent color bluring into visible pixels
        if pixelate:
            tex.setMagfilter(SamplerState.FT_nearest)
            tex.setMinfilter(SamplerState.FT_nearest)

        # Force disable texture compression for sprites, creates weird colors (compression artefacts)
        tex.setCompression(Texture.CMOff)

        # Add Texture to cache (Replaces Image object added by parent class)
        if cache:
            self.cache.toCache(cache_key, tex)

        return tex

    def getTextureSize(self, filepath, cache=True):
        """ Needs raw image (not texture) in cache"""
        img = self.image_manager.getImageFromPath(filepath=filepath, cache=cache)
        width, height = img.size
        return width, height
