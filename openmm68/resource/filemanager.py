#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

from openmm68.resource.resourcemanager import ResourceManager

class FileManager(ResourceManager):
    """ Just a wrapper with cache for VFS """

    def exists(self, path):
        return  self.vfs.exists(path)

    def getFile(self, path, cache=True):
        """ Throws exception if getting file fails """
        cache_key = self.getCacheKey(path)
        logger.debug("Getting file: filepath='%s', cache_key='%s'" % (path, cache_key))

        # from cache if is in
        if cache and self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # get file stream
        stream = self.vfs.get(path)

        # to cache
        if cache:
            self.cache.toCache(cache_key, stream)

        return stream
