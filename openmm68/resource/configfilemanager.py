#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from openmm68.resource.resourcemanager import ResourceManager

from openmm68.misctools.csv import readCSVStream
from openmm68.config.binparser import BinParser
from openmm68.config.txtparser import TxtParser

class ConfigFileManager(ResourceManager):

    FILE_TYPE_PRIORITY = ["csv", "txt", "bin"]

    def getConfig(self, filepath, cache=True):
        """ Throws exception if getting file fails """
        cache_key = self.getCacheKey(filepath)
        logger.debug("Getting config: filepath='%s', cache_key='%s'" % (filepath, cache_key))

        # from cache if is in
        if cache and self.cache.inCache(cache_key):
            return self.cache.fromCache(cache_key)

        # get bitmap file based on extension insensitive filename
        used_filepath = self.getExtensionInsensitivePath(filepath)

        # Get Config File stream and parse it
        stream = self.vfs.get(used_filepath)
        file_type = self.vfs.getType(used_filepath)

        if file_type.lower() == "csv":
            parsedconfig = readCSVStream(stream)

        elif file_type.lower() == "txt":
            parsedconfig = TxtParser.parse(stream, used_filepath)

        elif file_type.lower() == "bin":
            parsedconfig = BinParser.parse(stream, used_filepath)

        else:
            raise Exception("Reading config '%s' of type '%s' is not supported!" % (used_filepath, file_type))

        # Add object to cache
        if cache:
            self.cache.toCache(cache_key, parsedconfig)

        return parsedconfig
