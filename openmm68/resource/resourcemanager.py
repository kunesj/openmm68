#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from openmm68.resource.objectcache import ObjectCache

class ResourceManager(object):
    """ Base resource manager class that should be inherited by any resource manager classes. """

    # Priority of file extensions, used by getExtensionInsensitivePath() method
    FILE_TYPE_PRIORITY = []

    def __init__(self, vfs_manager):
        self.vfs = vfs_manager
        self.cache = ObjectCache()
        self.expiry_delay = 0.0

    def getExtensionInsensitivePath(self, filepath):
        """ Gets path of file with same basename but different extension, based on FILE_TYPE_PRIORITY list """
        ext_paths = self.vfs.getExtensionInsensitivePathList(filepath)
        used_filepath = filepath
        for ftype in self.FILE_TYPE_PRIORITY:
            if ftype in ext_paths:
                used_filepath = ext_paths[ftype]
                if used_filepath != filepath:
                    logger.debug("Changed path based on extension priority: %s, %s" % (used_filepath, ftype))
                break
        return used_filepath

    def getCacheKey(self, *args):
        """ Input: any number of string variables """
        cache_key = "|".join([str(x) for x in args])
        return cache_key

    def disableCache(self, disable):
        self.cache.setDisabled(disable)

    def getCacheMemoryUsage(self):
        """Output: (bytes_num, objects_num)"""
        return self.cache.getMemoryUsage()

    def updateCache(self):
        self.cache.removeExpired(self.expiry_delay)

    def setExpiryDelay(self, expiry_delay):
        self.expiry_delay = expiry_delay

    def getVFS(self):
        return self.vfs
