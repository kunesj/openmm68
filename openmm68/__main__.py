#!/usr/bin/python3
#coding: utf-8

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

import argparse
import numpy as np

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM
from openmm68.settings.settingsmanager import SETTINGS
from openmm68.config.gameconfigmanager import GAMECONFIG

from openmm68.odm.odmfile import ODMFile

from openmm68.renderer.renderer import renderer_setup
from openmm68.renderer.model import Model
from openmm68.renderer.entity import Entity
from openmm68.renderer.terrain import Terrain

def main():
    """
    Main function of OpenMM68 engine

    To see list of commandline arguments run:

        $ python3 -m openmm68 --help

    """
    # Parasing input prarmeters
    parser = argparse.ArgumentParser(
        description='OpenMM68'
    )
    parser.add_argument(
        'paths', nargs='*',
        help='Paths to resource archives')
    parser.add_argument(
        '-d', '--debug', type=int, choices=[50, 40, 30, 20, 10, 1], default=None,
        help='Set global debug level [CRITICAL=50, ERROR=40, WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level is WARNING.')
    parser.add_argument(
        '--debug_lod', type=int, choices=[50, 40, 30, 20, 10, 1], default=None,
        help='Set openmm68.lod module debug level. Overrides global debug level.')
    args = parser.parse_args()

    # Logger configuration
    logger = logging.getLogger()
    if args.debug is not None:
        logger.setLevel(args.debug)
        logger.info("Set global debug level to: %i" % (args.debug,))
    else:
        logger.setLevel(30)

    if args.debug_lod is not None:
        logger = logging.getLogger("openmm68.lod")
        logger.setLevel(args.debug_lod)
        logger = logging.getLogger()
        logger.info("Set openmm68.lod debug level to: %i" % (args.debug_lod,))

    # add archives to VFS and build index
    RESOURCE_SYSTEM.getVFS().registerArchives(args.paths)
    RESOURCE_SYSTEM.getVFS().buildIndex()

    # init GameConfig
    GAMECONFIG.loadDefaultConfig(SETTINGS.getInt("General", "GameVersion"))

    # get "maps/out01.odm" and parse it
    odm_stream = RESOURCE_SYSTEM.getFileManager().getFile("maps/out01.odm")
    odmf = ODMFile(odm_stream)

    ## prepare terrain
    logger.info("Getting Tile Texture names...")
    tex_names = odmf.getTileTextureNames() # TODO - slow +-5s
    # add +1 to x,y axis
    hmap_shape = odmf.data.terrain.heightmap.shape
    new_heightmap = np.zeros((hmap_shape[0]+1,hmap_shape[1]+1))
    new_heightmap[:-1,:-1] = odmf.data.terrain.heightmap

    terrain = Terrain(new_heightmap, odmf.data.terrain.tilemap, tex_names)

    # prepare models
    logger.info("Preparing models...")
    models = []
    for odmmodel in odmf.data.models:
        models.append(Model.fromODMModel(odmmodel))

    # prepare sprites
    logger.info("Preparing sprites...")
    sprites = []
    for odmentity in odmf.data.entites:
        sprites.append(Entity.fromODMEntity(odmentity))

    # render
    logger.info("Starting renderer...")
    app = renderer_setup()
    app.setTerrain(terrain)
    app.setModels(models)
    app.setSprites(sprites)
    app.rebuildScene()
    app.run()

if __name__ == "__main__":
    main()
