#!/usr/bin/python3
#coding: utf-8

from openmm68.misctools.attrdict import AttrDict
from openmm68.config.gameconfigmanager import GAMECONFIG

## Bits

def uint32ToBitIndex(uint32):
    """
    Get numerical index of bit attribute from value parsed as uint32
    Returns index of first bit with value == 1
    """
    # flip bytes backwards (uint32 is saved backwards in stream)
    hex32 = "{:08x}".format(uint32)
    uint32_new = int(hex32[6:8]+hex32[4:6]+hex32[2:4]+hex32[0:2], 16)

    for i, bit_str in enumerate("{:032b}".format(uint32_new)):
        if int(bit_str) == 1:
            return i
    raise Exception("No bit with value == 1 in given uint32!")

def uint16ToBitIndex(uint16):
    """
    Get numerical index of bit attribute from value parsed as uint16
    Returns index of first bit with value == 1
    """
    # flip bytes backwards (uint16 is saved backwards in stream)
    hex16 = "{:04x}".format(uint16)
    uint16_new = int(hex16[2:4]+hex16[0:2], 16)
    for i, bit_str in enumerate("{:016b}".format(uint16_new)):
        if int(bit_str) == 1:
            return i
    raise Exception("No bit with value == 1 in given uint16!")

def parseBitAttributes(stream, bit_type, byte_num=4):
    """
    Ignores fact that some attributes are not defined in some MM versions
    bit_type = FacetBits,
    byte_num = number of bytes to read (only 2,4 allowed)
    """
    if byte_num not in [2,4]:
        raise Exception("Ilegal byte_num %i" % byte_num)

    attrs_bits = stream.readBits8(byte_num)
    attrs = AttrDict()

    if GAMECONFIG.hasSection(bit_type):
        for item in GAMECONFIG.options(bit_type):
            number = GAMECONFIG.getInt(bit_type, item)
            if byte_num == 2:
                attrs[item] = attrs_bits[uint16ToBitIndex(number)]
            elif byte_num == 4:
                attrs[item] = attrs_bits[uint32ToBitIndex(number)]

    return attrs

## Unit conversions
# Define Scale:
# Sizes must be equal => MM_UNITS == PANDA3D_UNITS == METER_UNITS
PANDA3D_UNITS = 1
MM_UNITS = 512 # Might and Magic units
METER_UNITS = 2

def mm_2_panda3d(val):
    # python 3 uses float division by default.
    # python 2 would need float() in in first steps of conversion.
    return val/(MM_UNITS/PANDA3D_UNITS)

def mm_2_meter(val):
    return val/(MM_UNITS/METER_UNITS)

def panda3d_2_mm(val):
    return val/(PANDA3D_UNITS/MM_UNITS)

def panda3d_2_meter(val):
    return val/(PANDA3D_UNITS/METER_UNITS)

def meter_2_mm(val):
    return val/(METER_UNITS/MM_UNITS)

def meter_2_panda3d(val):
    return val/(METER_UNITS/PANDA3D_UNITS)
