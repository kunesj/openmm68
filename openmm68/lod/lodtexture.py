#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

import numpy as np
from PIL import Image # Pillow package

import io
from openmm68.misctools.binarydatastream import BinaryDataStream

def processAlphaChannel(img, transparent_color=(255, 0, 255)):
    """
    Creates alpha channel from transparent color value.
    Transparent color is always first in palette.
    Input:
        img - PIL.Image()
        transparent_color - tupple (r,g,b) of color that should be transparent
    Returns: PIL.Image()
    """
    data = np.array(img)   # "data" is a height x width x 4 numpy array
    red, green, blue, alpha = data.T # Temporarily unpack the bands for readability

    # Makes trans_color pixels transparent (leaves color values alone)
    transparent_areas = (red == transparent_color[0]) & \
                        (green == transparent_color[1]) & (blue == transparent_color[2])
    data[..., 2:][transparent_areas.T] = (0) # Transpose back needed

    img2 = Image.fromarray(data)
    return img2

def parsePalette(stream):
    """
    Input: BinaryDataStream() of LOD palette file
    Returns: list of tupples (r,g,b)
    """
    stream.seek(0x0, io.SEEK_SET)
    size = stream.getLength()

    if size % 3 != 0:
        logger.warning("Palette size is not dividable by 3!")
    colors_num = int(size/3)

    palette_colors = []
    for i in range(0, colors_num):
        rgb = stream.readUInt8(3)
        palette_colors.append(rgb)

    return palette_colors

def parseLODBitmap(stream):
    """
    Input: BinaryDataStream() of LOD bitmap file
    Returns: PIL.Image()
    """
    # parse header
    stream.seek(0, io.SEEK_SET)
    signature = stream.readString(0xa)
    data_offset = stream.readUInt32()
    pixeldata_size = stream.readUInt32()
    palette_size = stream.readUInt32()
    bitmap_width = stream.readUInt16()
    bitmap_height = stream.readUInt16()

    # get palette
    stream.seek(data_offset+pixeldata_size, io.SEEK_SET)
    palette_stream = BinaryDataStream(stream.readByteArray(palette_size))
    palette = parsePalette(palette_stream)

    # Create Image object
    stream.seek(data_offset, io.SEEK_SET)
    pDest = np.zeros((bitmap_height, bitmap_width, 4), dtype=np.uint8)

    for i in range(0, bitmap_height):
        for j in range(0, bitmap_width):
            color_index = stream.readUInt8()
            pDest[i][j][0] = palette[color_index][0] # red
            pDest[i][j][1] = palette[color_index][1] # gren
            pDest[i][j][2] = palette[color_index][2] # blue
            pDest[i][j][3] = 255 # alpha

    img = Image.fromarray(pDest, 'RGBA')
    img = processAlphaChannel(img, transparent_color=palette[0])

    return img

def parsePaletteID(stream):
    """
    Input: BinaryDataStream() of LOD sprite file
    Returns: Index of palette. (Example: Returns number 548 for palette 'pal548')
    """
    stream.seek(0xa+3*0x4+2*0x2, io.SEEK_SET)
    sprite_palette_id = stream.readUInt16()
    return sprite_palette_id

def parseLODSprite(sprite_stream, palette_stream):
    """
    Input:
        sprite_stream - BinaryDataStream() of LOD sprite file
        palette_stream - BinaryDataStream() of LOD palette file
    Returns: PIL.Image()
    """
    # read header
    sprite_stream.seek(0x0, io.SEEK_SET)
    signature = sprite_stream.readString(0xa)
    data_offset = sprite_stream.readUInt32()
    pixeldata_offset = sprite_stream.readUInt32()
    pixeldata_size = sprite_stream.readUInt32()
    sprite_width = sprite_stream.readUInt16()
    sprite_height = sprite_stream.readUInt16()
    sprite_palette_id = sprite_stream.readUInt16()
    sprite_yskip = sprite_stream.readUInt16()

    # get palette and background color
    palette = parsePalette(palette_stream)
    background = palette[0]

    # create empty image
    pDest = np.zeros((sprite_height, sprite_width, 4), dtype=np.uint8)
    pDest[:,:,0] = background[0]
    pDest[:,:,1] = background[1]
    pDest[:,:,2] = background[2]
    pDest[:,:,3] = 255

    # fill colored pixels
    for h in range(0, sprite_height):
        index = h*8
        sprite_stream.seek(data_offset+index, io.SEEK_SET)

        a1 = sprite_stream.readInt16() # start pixel
        a2 = sprite_stream.readInt16() # end pixel
        pos = sprite_stream.readInt32() # position in pixeldata list

        # if empty line
        if a1 == -1 or a2 == -1:
            continue

        sprite_stream.seek(pixeldata_offset+pos, io.SEEK_SET)
        for w in range(a1, a2+1):
            color_index = sprite_stream.readUInt8()

            pDest[h][w][0] = palette[color_index][0] # red
            pDest[h][w][1] = palette[color_index][1] # green
            pDest[h][w][2] = palette[color_index][2] # blue
            pDest[h][w][3] = 255 # alpha

    # create Image object
    img = Image.fromarray(pDest, 'RGBA')
    img = processAlphaChannel(img, transparent_color=palette[0])

    return img
