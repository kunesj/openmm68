#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import os, io
import zlib
import struct

from collections import OrderedDict
from openmm68.misctools.attrdict import AttrDict
from openmm68.misctools.caseinsensitivedict import CaseInsensitiveDict
from openmm68.misctools.binarydatastream import BinaryDataStream

class LODFile:
    """
    Class for extracting data from LOD archives.

    The layout of a LOD archive is as follows:

    ---------- start of header block -----------

    - 0x120 bytes header, contains:
         signature - First Null terminated string in next 0x4 bytes. Should allways be 'LOD'.
         version - First Null terminated string in next 0x50 bytes. Can be '', 'MMVI', 'GameMMVI', 'MMVII' or 'MMVIII'.
         description - First Null terminated string in next 0x50 bytes. Is description of LOD file.
         unknown_1 - UInt32. Value of 100 (version?).
         unknown_2 - UInt32. Value of 0.
         archives_count - UInt32. Should have value of 1, it's number of archives in LOD.
         unknown_3 - 0x50 bytes.
         lod_type - First Null terminated string in next 0x10 bytes. Can be '', 'bitmaps', 'icons', 'sprites08', 'maps', 'chapter' or 'language'.
         archive_start - UInt32. Offset of start of archive (directory block).
         archive_size - UInt32. Size of archive.
         unknown_4 - UInt32. Value of 0 (bits?).
         count - UInt16. Number of files in archive (and records in directory block).
         unknown_5 - UInt16. Value of 0 (type of data?).

    ---------- end of header block -------------
    ---------- start of directory block -----------

    (If LOD.version in ['MMVI', 'GameMMVI', 'MMVII']):
    - 0x20 bytes*LOD.count, each record contains:
         name - First Null terminated string in next 0x10 bytes. Filename of file.
         off - UInt32. File offset in archive.
         size - UInt32. Size of file in archive
         hz - UInt32.
         num - UInt32.

    (If LOD.version in ['MMVIII']):
    - 0x4c bytes*LOD.count, each record contains:
         name - First Null terminated string in next 0x40 bytes. Filename of file.
         off - UInt32. File offset in archive.
         size - UInt32. Size of file in archive
         hz - UInt32.

    ---------- end of directory block -------------
    ----------- start of data buffer --------------

    - The rest of the archive is file data, indexed by the
        offsets in the directory block. The offsets start at 0 at
        the beginning of the directory block => offset in LOD file is offset value from
        directory block + LOD.archive_start.
    """

    # height of minimal header
    header_length = 0x120

    # possible lod versions and types + their numerical values
    lod_versions = CaseInsensitiveDict({ "MMVIII": 8, "MMVII": 7, "MMVI": 6, "GameMMVI": 6, })
    lod_types = CaseInsensitiveDict({
        "bitmaps":0, "icons":1, "sprites08":2, "maps":3, "chapter":4,
        "language":5,
        })

    def __init__(self, filepath=None):
        self.filepath = None
        self.filename = None
        self.filesize = None
        self.header = AttrDict()
        self.files = OrderedDict()

        if filepath is not None:
            self.open(filepath)

    def getDataStream(self, offset=0, size=-1):
        """
        Input:
            offset - position in LOD file at which should reading start
            size - how many bytes shoul be read. Reads until EOF on default
        """
        with io.open(self.filepath, 'rb') as f:
            f.seek(0x0, io.SEEK_END) # seek EOF
            file_size = f.tell()

            # test if offset or size are not invalid values
            if (offset+size) > file_size:
                raise ValueError("Invalid offset or size argument! offset=%i, size=%i, filename=%s" % (offset, size, self.filename))

            # read data from file to stream
            f.seek(offset, io.SEEK_SET) # seek start of file
            if size == -1:
                size = file_size-offset
            datastream = BinaryDataStream(f.read(size))

        # check that I read correct number of bytes
        size_read = datastream.getLength()
        logger.debug("Read %i bytes from file to BinaryDataStream in memory." % (size_read,) )
        if size_read != size:
            logger.warning("Incorrect number of bytes read. Wanted to read %i, but read %i bytes." % (size, size_read) )

        datastream.seek(0x0, io.SEEK_SET) # seek start of file
        return datastream

    def open(self, filepath):
        """ Just tests if LOD file is readable, and reads header """
        logger.info("Opening LOD file: %s" % (filepath,))

        # get correct filepath and filename
        if not (os.path.isfile(filepath) and os.path.exists(filepath)):
            raise FileNotFoundError(filepath)
        self.filepath = os.path.abspath(filepath)
        self.filename = os.path.basename(filepath)

        # get filesize
        ds = self.getDataStream()
        self.filesize = ds.getLength()

        # test filesize
        if self.filesize < self.header_length:
            raise Exception("File is too small to be a valid LOD archive!")

        # read header and directory block
        self.readHeader()
        self.readDirectoryBlock()

    def readHeader(self):
        """ Reads header into self.header arrDict """
        logger.info("Reading header...")
        stream = self.getDataStream(0, self.header_length)
        stream.seek(0x0, io.SEEK_SET)

        self.header = AttrDict()
        self.header.signature =     stream.readString(0x4, strip_null=True)
        self.header.version =       stream.readString(0x50, strip_null=True)
        self.header.description =   stream.readString(0x50, strip_null=True)
        self.header.unknown_1 =     stream.readUInt32()
        self.header.unknown_2 =     stream.readUInt32()
        self.header.archives_count = stream.readUInt32()
        self.header.unknown_3 =     stream.readString(0x50, strip_null=True, raw=True)
        self.header.lod_type =      stream.readString(0x10, strip_null=True)
        self.header.archive_start = stream.readUInt32()
        self.header.archive_size =  stream.readUInt32()
        self.header.unknown_4 =     stream.readUInt32()
        self.header.count =         stream.readUInt16()
        self.header.unknown_5 =     stream.readUInt16()
        logger.debug(self.header)

        # test header values
        if self.header.signature != "LOD":
            raise Exception("Unrecognized LOD signature!")
        if self.header.archives_count > 1:
            raise Exception("Combined archives are not supported!")

        # get numerical value of LOD version
        if not self.header.version in self.lod_versions:
            logger.warning("Unknown LOD version: '%s', (Defaulting to version = 6)" % (self.header.version,) )
        else:
            self.header.version_int = self.lod_versions[self.header.version]

        # get numerical value of LOD type + check if LOD type is supported
        if not self.header.lod_type in self.lod_types:
            raise Exception("Unsupported LOD type: '%s'" % (self.header.lod_type,) )
        else:
            self.header.lod_type_int = self.lod_types[self.header.lod_type]

    def readDirectoryBlock(self):
        """ Reads directory block into self.files list """
        logger.info("Reading directory block...")

        # Get length of records in directory block
        if self.header.version_int == 8:
            record_length = 0x4c
        else:
            record_length = 0x20

        # test if file is long enough
        block_length = record_length*self.header.count
        if (block_length + self.header_length) > self.filesize:
            raise Exception("Directory block and header are together longer than entire file!")

        # get data stream
        stream = self.getDataStream(self.header_length, block_length)
        stream.seek(0x0, io.SEEK_SET)

        # read records
        self.files = {}
        for i in range(0, self.header.count):
            frecord = {}
            if self.header.version_int == 8:
                frecord['name'] = stream.readString(0x40, strip_null=True) # split may remove some unused data after filename
                frecord['offset'] =  stream.readInt32()
                frecord['size'] = stream.readInt32()
                frecord['hz'] =   stream.readInt32()
            else:
                frecord['name'] = stream.readString(0x10, strip_null=True)  # split may remove some unused data after filename
                frecord['offset'] =  stream.readInt32()
                frecord['size'] = stream.readInt32()
                frecord['hz'] =   stream.readInt32()
                frecord['num'] =  stream.readInt32()

            # Make file offset start at 0 at the beginning of file insted of archive
            frecord['offset'] = frecord['offset'] + self.header.archive_start

            # Check if file is outside of filesize
            if frecord['offset'] + frecord['size'] > self.filesize:
                raise Exception("Archive contains offsets outside itself!")

            # Add the file name to the lookup table
            logger.log(1, frecord)
            self.files[frecord['name']] = frecord

        # sort files
        self.files = OrderedDict(sorted(self.files.items()))
        logger.debug("Read %i file records" % (len(self.files),) )

    def getFileList(self):
        """ Returns list of filenames in LOD file. """
        return list(self.files.keys())

    def getLODType(self):
        """ Returns type of LOD file. (string) """
        return self.header.lod_type

    ## Extracting files

    def exists(self, filename):
        """ Returns True is filename is in LOD file filelist. """
        return filename in self.files

    def getFile(self, filename):
        """ Returns BinaryDataStream of extracted file """
        logger.info("Reading file '%s' from LOD '%s'" % (filename, self.filename))
        if not self.exists(filename):
            raise FileNotFoundError(filename)

        # get stream
        frecord = self.files[filename]
        logger.log(1, "File record: %s" % (str(frecord),))
        stream = self.getDataStream(frecord["offset"], frecord["size"])
        stream.seek(0x0, io.SEEK_SET)

        if self.header.lod_type_int == self.lod_types["maps"]:
            return self.extract_maps(stream)

        elif self.header.lod_type_int == self.lod_types["language"]:
            return self.extract_generic(stream)

        elif self.header.lod_type_int == self.lod_types["bitmaps"]:
            return self.extract_bitmaps(stream)

        elif self.header.lod_type_int == self.lod_types["icons"]:
            return self.extract_generic(stream)

        elif self.header.lod_type_int == self.lod_types["sprites08"]:
            return self.extract_sprites08(stream)

        elif self.header.lod_type_int == self.lod_types["chapter"]:
            if filename.lower().strip().split(".")[-1] in ["blv", "dlv", "ddm"]:
                return self.extract_maps(stream) # decompress maps
            else:
                return self.extract_raw(stream)
            # There could still be more exceptions

        else:
            raise Exception("Unknown LOD type? Couldn't unpack files...")

    def decompress(self, stream, header_height, packed_size, unpacked_size):
        logger.log(1, "decompress()")
        stream.seek(header_height, io.SEEK_SET)
        packed_data = stream.readByteArray(packed_size)
        unpacked_data = zlib.decompress(packed_data)
        unpacked_stream = BinaryDataStream(unpacked_data)
        unpacked_stream.seek(0x0, io.SEEK_SET)
        if unpacked_stream.getLength() != unpacked_size:
            logger.warning("Length of unpacked stream (%i) is different from unpacked_size variable (%i)." % (unpacked_stream.getLength(),unpacked_size))
        return unpacked_stream

    def extract_maps(self, stream):
        """
        LodType='maps' data structure:
            MM6GamesFile (header_height 0x8)
                DataSize: int32
                UnpackedSize: int32
            MM7GamesFile (header_height 0x10)
                Sig1: \\x41\\x67\\x01\\x00
                Sig2: \\x6D\\x76\\x69\\x69 (=="mvii")
                DataSize: int32
                UnpackedSize: int32
        """
        logger.log(1, "extract_maps()")
        stream.seek(0x0, io.SEEK_SET)
        size = stream.getLength()

        # get map type
        mdata_var1 = stream.readString(0x4, raw=True) # 0x0,0x1,0x2,0x3
        mdata_var2 = stream.readString(0x4, raw=True) # 0x4,0x5,0x6,0x7

        if mdata_var1 == b"\x41\x67\x01\x00" and mdata_var2 == b"\x6D\x76\x69\x69": # mdata_var2=="mvii"
            header_height = 0x10
            stream.seek(0x8, io.SEEK_SET)
            # get packed and unpacked size
            packed_size = stream.readUInt32() # 0x8,0x9,0xa,0xb
            unpacked_size = stream.readUInt32() # 0xc,0xd,0xe,0xf
            # test if packed_size has correct value
            if (packed_size + header_height) != size:
                logger.warning("Invalid map7 size. packed_size: %i, header_height: %i, size: %i" % (packed_size, header_height, size) )

        else: # map type -> mmvi (or other) # TODO untested
            header_height = 0x8
            stream.seek(0x0, io.SEEK_SET)
            # get packed and unpacked size
            packed_size = stream.readUInt32() # 0x0,0x1,0x2,0x3
            unpacked_size = stream.readUInt32() # 0x4,0x5,0x6,0x7
            # test if packed_size has correct value
            if (packed_size + header_height) != size:
                logger.warning("Invalid map6 size. packed_size: %i, header_height: %i, size: %i" % (packed_size, header_height, size) )

        # unpack file data
        unpacked_stream = self.decompress(stream, header_height, packed_size, unpacked_size)

        return unpacked_stream

    def extract_raw(self, stream):
        """
        Used by chapter LOD type
        - has no header, is not compressed
        """
        logger.log(1, "extract_raw()")
        stream.seek(0x0, io.SEEK_SET)
        return stream

    def extract_generic(self, stream):
        """ Used by language, icons and called by extract_bitmaps() """
        logger.log(1, "extract_generic()")
        stream.seek(0x0, io.SEEK_SET)
        size = stream.getLength()

        # Define length of filename and get header_height
        if self.header.version_int == 8:
            filename_length = 0x40
        else:
            filename_length = 0x10
        header_height = filename_length + 0x20

        # Get general info
        stream.seek(0x0, io.SEEK_SET)
        filename = stream.readString(filename_length, strip_null=True) # 0x0=>0xf/0x0=>0x3f
        unpacked_size1 = stream.readUInt32() # filename + 0x0,0x1,0x2,0x3
        packed_size = stream.readUInt32() # filename + 0x4,0x5,0x6,0x7
        stream.seek(filename_length+0x18, io.SEEK_SET)
        unpacked_size2 = stream.readUInt32() # filename + 0x18,0x19,0x1a,0xab

        # Get correct value of unpacked_size
        if unpacked_size2 == 0:
            unpacked_size = packed_size
        else:
            unpacked_size = unpacked_size2

        # Get adddata info
        adddata_offset = header_height + packed_size
        adddata_size = size - (header_height + packed_size)

        # print debug info
        logger.log(1, "filename=%s, packed_size=%i, unpacked_size=%i, unpacked_size1=%i, unpacked_size2=%i, adddata_offset=%i, adddata_size=%i," % \
            (filename, packed_size, unpacked_size, unpacked_size1, unpacked_size2, adddata_offset, adddata_size) )

        # test sizes
        if unpacked_size2!=0 and unpacked_size2 < unpacked_size1:
            logger.debug("unpacked_size2 < unpacked_size1")
        if (header_height+packed_size+adddata_size) != size:
            raise Exception("Incorect data size -> (header_height+packed_size+addsize)!=size")

        # unpack file data
        if unpacked_size == packed_size:
            # if data is not packed
            stream.seek(header_height, io.SEEK_SET)
            unpacked_stream = BinaryDataStream(stream.readByteArray(packed_size))
        else:
            unpacked_stream = self.decompress(stream, header_height, packed_size, unpacked_size)

        # copy adddata to the end of unpacked_stream
        if adddata_size > 0:
            unpacked_data = unpacked_stream.readByteArray(unpacked_stream.getLength())
            stream.seek(adddata_offset, io.SEEK_SET)
            adddata_data = stream.readByteArray(adddata_size)
            unpacked_stream = BinaryDataStream(unpacked_data+adddata_data)

        return unpacked_stream

    def extract_bitmaps(self, stream):
        """
        Extracts bitmap data and adds small header with values required for futher processing.

        First color in palette is transparent color.

        Custom created header:
            string(0xa) signature == b"LOD_BITMAP"
            uint32 data_offset == length of header
            uint32 pixeldata_size
            uint32 palette_size
            uint16 bitmap_width
            uint16 bitmap_height
        Data structure:
            header
            pixeldata - uint8 indexes of colors in palette data
            palette - list of [uint8 R, uint8 G, uint8 B] colors
        """
        logger.log(1, "extract_bitmaps()")
        stream.seek(0x0, io.SEEK_SET)
        size = stream.getLength()
        header_height = 0x30 # first 0x10 is "filename"

        # never saw bersion=8 LOD with 'bitmaps' type
        if self.header.version_int == 8:
            raise Exception("LOD file version=8 and lod_type='bitmaps' is not supported ")

        # Parse header
        filename = stream.readString(0x10, strip_null=True) # 0x0=>0xf
        unpacked_size1 = stream.readUInt32() # 0x10,0x11,0x12,0x13
        packed_size = stream.readUInt32() # 0x14,0x15,0x16,0x17
        bitmap_width = stream.readUInt16() # 0x18,0x19
        bitmap_height = stream.readUInt16() # 0x1a,0x1b
        unk_1_1 = stream.readUInt16() # 0x1c,0x1d
        unk_1_2 = stream.readUInt16() # 0x1e,0x1f
        unk_2_1 = stream.readUInt16() # 0x20,0x21
        unk_2_2 = stream.readUInt16() # 0x22,0x23
        unk_3 = stream.readUInt32() # 0x24,0x25,0x26,0x27
        unpacked_size2 = stream.readUInt32() # 0x28,0x29,0x2a,0x2b
        unk_4 = stream.readUInt32() # 0x2c,0x2c,0x2e,0x2f

        # Get correct value of unpacked_size
        if unpacked_size2 == 0:
            unpacked_size = packed_size
        else:
            unpacked_size = unpacked_size2

        # calculated additional variables
        palette_offset = unpacked_size
        adddata_offset = header_height + packed_size
        adddata_size = size - (header_height + packed_size)

        # detect if file is palette
        palette = ( filename.lower().startswith("pal") and packed_size == 0 )

        # get unpacked_stream and add custom header if file is not palette
        unpacked_stream = self.extract_generic(stream)
        if not palette:
            # create custom header
            signature = b"LOD_BITMAP"
            data_offset = len(signature)+3*0x4+2*0x2
            pixeldata_size = unpacked_size
            palette_size = adddata_size

            custom_header = bytearray(0)
            custom_header += struct.pack("<"+str(len(signature))+"s", signature)
            custom_header += struct.pack("<I", data_offset)
            custom_header += struct.pack("<I", pixeldata_size)
            custom_header += struct.pack("<I", palette_size)
            custom_header += struct.pack("<H", bitmap_width)
            custom_header += struct.pack("<H", bitmap_height)

            # add custom header to unpacked_data
            unpacked_data = unpacked_stream.readByteArray(unpacked_stream.getLength())
            unpacked_stream = BinaryDataStream(custom_header+unpacked_data)
        else:
            logger.log(1, "File was palette. Not adding custom header.")

        return unpacked_stream


    def extract_sprites08(self, stream):
        """
        Extracts sprite08 data and adds small header with values required for futher processing.

        First color in palette is transparent color.

        Custom created header:
            string(0xa) signature == b"LOD_SPRITE"
            uint32 data_offset == length of header, offset of extended header
            uint32 pixeldata_offset == length of header + extended header
            uint32 pixeldata_size == (size of file - size of header - size of extended header)
            uint16 sprite_width
            uint16 sprite_height
            uint16 sprite_palette_id == index number of palette to use
            uint16 sprite_yskip == number of clear lines at the bottom => height of empty/transparent space at the bottom of the final image
        Data structure:
            header
            extended header - has values needed for reading sprite
            pixeldata - uint8 indexes of colors in palette data. starts at pixeldata_offset
        """
        logger.log(1, "extract_sprites08()")
        stream.seek(0x0, io.SEEK_SET)
        size = stream.getLength()

        # General header variables
        header_height = 0x20
        stream.seek(0xc, io.SEEK_SET)
        packed_size = stream.readUInt32() # 0xc,0xd,0xe,0xf

        # Sprite only variables
        stream.seek(0x10, io.SEEK_SET)
        sprite_width = stream.readUInt16() # 0x10,0x11
        sprite_height = stream.readUInt16() # 0x12,0x13
        sprite_palette_id = stream.readUInt16() # 0x14,0x15
        sprite_unk_1 = stream.readUInt16() # 0x16,0x17
        sprite_yskip = stream.readUInt16() # 0x18,0x19
        sprite_unk_2 = stream.readUInt16() # 0x1a,0x1b - used in runtime only, for bits ???
        sprite_data_size = stream.readUInt32() # 0x1c,0x1d,0x1e,0x1f

        # compute other variables
        sprite_pixeldata_offset = sprite_height*8 # extended header height, includes sprite information, (does not exist in header)
        header_height_ext = header_height + sprite_pixeldata_offset
        unpacked_size = sprite_data_size

        # test sizes
        if (packed_size + header_height_ext) != size:
            raise Exception("Incorect data size -> (packed_size+header_height)!=size")

        # get unpacked_stream
        unpacked_stream = self.decompress(stream, header_height_ext, packed_size, unpacked_size)

        # Get extended header data
        stream.seek(header_height, io.SEEK_SET)
        extended_header_data = stream.readByteArray(sprite_pixeldata_offset)

        # create custom header
        signature = b"LOD_SPRITE"
        data_offset = len(signature)+3*0x4+4*0x2
        pixeldata_offset = data_offset + len(extended_header_data)
        pixeldata_size = unpacked_size

        custom_header = bytearray(0)
        custom_header += struct.pack("<"+str(len(signature))+"s", signature)
        custom_header += struct.pack("<I", data_offset)
        custom_header += struct.pack("<I", pixeldata_offset)
        custom_header += struct.pack("<I", pixeldata_size)
        custom_header += struct.pack("<H", sprite_width)
        custom_header += struct.pack("<H", sprite_height)
        custom_header += struct.pack("<H", sprite_palette_id)
        custom_header += struct.pack("<H", sprite_yskip)

        # add custom sprite header and copy back original part of extended header
        unpacked_stream.seek(0x0, io.SEEK_SET)
        unpacked_data = unpacked_stream.readByteArray(unpacked_stream.getLength())
        unpacked_stream = BinaryDataStream(custom_header+extended_header_data+unpacked_data)

        return unpacked_stream

def main():
    import argparse

    import openmm68.lod.lodtexture as lodtexture

    # Parasing input prarmeters
    parser = argparse.ArgumentParser(
        description='openmm68.lod.lodfile'
    )
    parser.add_argument(
        'lodfile',
        help='Path to LOD file')
    parser.add_argument(
        '-i', '--info', action='store_true',
        help='Print info about LOD file')
    parser.add_argument(
        '-l', '--list', action='store_true',
        help='List files in LOD file')
    parser.add_argument(
        '-e', '--extract', default=None,
        help='Extract file with specifited filename from LOD file to current dir')
    parser.add_argument(
        '--extractbitmap', default=None,
        help='Extract bitmap file with specifited filename from LOD file to current dir and convert it to PNG')
    parser.add_argument(
        '--extractsprite', default=None,
        help='Extract sprite file with specifited filename from LOD file to current dir and convert it to PNG. Requires palette "pall???" file in current directory')
    parser.add_argument(
        '--extractall', action='store_true',
        help='Extracts all files from LOD file into folder with same name')
    parser.add_argument(
        '-d', '--debug', type=int, choices=[50, 40, 30, 20, 10, 1], default=None,
        help='Set global debug level [CRITICAL=50, ERROR=40, WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level is WARNING.')
    args = parser.parse_args()

    # Logger configuration
    logging.basicConfig()
    logger = logging.getLogger()
    if args.debug is not None:
        logger.setLevel(args.debug)
        logger.info("Set global debug level to: %i" % (args.debug,) )
    else:
        logger.setLevel(30)

    # load LOD file
    lf = LODFile(args.lodfile)

    if args.info:
        print("filepath: %s" % lf.filepath)
        print("filename: %s" % lf.filename)
        print("filesize: %i" % lf.filesize)
        print(lf.header)

    elif args.list:
        for filename in lf.getFileList():
            print(filename)

    elif args.extract is not None:
        print("Extracting file: %s" % (args.extract,) )
        stream = lf.getFile(args.extract)
        with open(args.extract, 'wb') as f:
            f.write(stream.read())

    elif args.extractbitmap is not None:
        print("Extracting bitmap file: %s" % (args.extractbitmap,) )
        stream = lf.getFile(args.extractbitmap)

        img = lodtexture.parseLODBitmap(stream)
        imgByteStr = io.BytesIO()
        img.save(imgByteStr, format='PNG')

        with open(args.extractbitmap+".png", 'wb') as f:
            imgByteStr.seek(0x0, io.SEEK_SET)
            f.write(imgByteStr.read())

    elif args.extractsprite is not None:
        print("Extracting sprite file: %s" % (args.extractsprite,) )
        stream = lf.getFile(args.extractsprite)

        palette_id = lodtexture.parsePaletteID(stream)
        palette_filename = "pal%s" % str(palette_id).zfill(3)
        if not os.path.exists(os.path.join("./", palette_filename)):
            logger.warning("Couldn't find palette '%s' in current directory, defaulting to palette 'palxxx'" % palette_filename)
            palette_filename = "palxxx"
            if not os.path.exists(os.path.join("./", palette_filename)):
                raise Exception("Couldn't find even default palette 'palxxx' in current directory!")
        with open(os.path.join("./", palette_filename), 'rb') as pf:
            palette_stream = BinaryDataStream(pf.read())

        img = lodtexture.parseLODSprite(stream, palette_stream)
        imgByteStr = io.BytesIO()
        img.save(imgByteStr, format='PNG')

        with open(args.extractsprite+".png", 'wb') as f:
            imgByteStr.seek(0x0, io.SEEK_SET)
            f.write(imgByteStr.read())

    elif args.extractall:
        extract_dir = os.path.join(".", os.path.basename(args.lodfile)+"_dir")
        print("Extracting all files from LOD file to directory: %s" % (extract_dir, ) )
        os.makedirs(extract_dir, exist_ok=True)
        for key in lf.getFileList():
            stream = lf.getFile(key)
            with open(os.path.join(extract_dir, key), 'wb') as f:
                f.write(stream.read())

    else:
        print("Nothing to do...")

if __name__ == "__main__":
    main()
