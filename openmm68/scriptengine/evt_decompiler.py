#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from collections import OrderedDict

import io
from openmm68.misctools.binarydatastream import BinaryDataStream

from openmm68.scriptengine.evt_instructions import EvtInstructions
from openmm68.scriptengine.evt_variables import EvtVariables

from openmm68.config.gameconfigmanager import GameConfigManager

class EvtDecompiler(object):

    def __init__(self, mm_version=8, translate_names=True):
        if mm_version not in [6,7,8]:
            raise Exception("Unsupported might and magic version %i, only versions 6,7,8 are supported." % mm_version)
        self.mm_version = mm_version

        self.translate_names = translate_names

        self.instructions = EvtInstructions(mm_version=self.mm_version)
        self.vars = EvtVariables(mm_version=self.mm_version)

        self.gameconfig = GameConfigManager()
        self.gameconfig.loadDefaultConfig(self.mm_version) # loads default might and magic configs

    @staticmethod
    def parseEvtStream(stream):
        """
        Returns: [ OrderedDict(ADDRESS, LINE_LENGTH, EVENT_NUMBER, LINE_NUMBER, INSTRUCTION, PARAMETERS(bytes)), ... ]
        """
        stream.seek(0, io.SEEK_SET)

        parsed_evt = []
        while stream.tell() < stream.getLength():
            evt_line = OrderedDict()

            evt_line["ADDRESS"] = stream.tell()
            evt_line["LINE_LENGTH"] = stream.readUInt8()
            evt_line["EVENT_NUMBER"] = stream.readUInt16()
            evt_line["LINE_NUMBER"] = stream.readUInt8()
            evt_line["INSTRUCTION"] = stream.readUInt8()
            evt_line["PARAMETERS"] = stream.readByteArray(evt_line["LINE_LENGTH"]-4)

            parsed_evt.append(evt_line)

        return parsed_evt

    ## Dumping

    def dumpEvtStream(self, stream):
        parsed_evt = self.parseEvtStream(stream)
        return self.dumpParsedEvt(parsed_evt)

    def dumpParsedEvt(self, parsed_evt):
        lines = [ "####: ADDRESS LENGTH EVT_NUM LINE INSTRUCTION PARAMETERS" ]
        for i, evt_line in enumerate(parsed_evt):
            lines.append("%04i: 0x%04x  %03i    %03i     %02i   0x%02x        %s" % (i, evt_line["ADDRESS"], evt_line["LINE_LENGTH"], evt_line["EVENT_NUMBER"],
                evt_line["LINE_NUMBER"], evt_line["INSTRUCTION"], " ".join("0x%02x" % b for b in evt_line["PARAMETERS"]) ))
        return "\n".join(lines)

    ## Decompiling

    def decompileEvtStream(self, stream):
        parsed_evt = self.parseEvtStream(stream)
        return self.decompileParsedEvt(parsed_evt)

    def decompileParsedEvt(self, parsed_evt):
        lines = []
        last_event_number = None
        for i, evt_line in enumerate(parsed_evt):

            # new event started
            if evt_line["EVENT_NUMBER"] != last_event_number:
                new_event = "event %i" % evt_line["EVENT_NUMBER"]
                if last_event_number is not None:
                    new_event = "end\n\n"+new_event
                lines.append(new_event)
                last_event_number = evt_line["EVENT_NUMBER"]

            ## parse instructions

            if not self.instructions.isDefined(evt_line["INSTRUCTION"]):
                logger.warning("Undefined instruction number %04i with value 0x%02x" % (i, evt_line["INSTRUCTION"]))
                decompiled_line = "%04i: 0x%04x  %03i    %03i     %02i   0x%02x        %s" % (i, evt_line["ADDRESS"], evt_line["LINE_LENGTH"], evt_line["EVENT_NUMBER"],
                evt_line["LINE_NUMBER"], evt_line["INSTRUCTION"], " ".join("0x%02x" % b for b in evt_line["PARAMETERS"]) )

            else:
                line_num = evt_line["LINE_NUMBER"]
                name = self.instructions.getName(evt_line["INSTRUCTION"])
                params = self.instructions.parseParameters(BinaryDataStream(evt_line["PARAMETERS"]), evt_line["INSTRUCTION"])

                for k in params:
                    # add quotes to strings
                    if type(params[k]) == type(""):
                        params[k] = '"%s"' % params[k]

                    # replace indexes with names
                    if k.lower() == "var_index" and self.translate_names:
                        params[k] = '"%s"' % self.vars.getNameById(params[k])

                    elif k.lower() == "face_bit" and self.translate_names:
                        params[k] = '"const.FacetBits.%s"' % self.gameconfig.getOptionByIntValue("FacetBits", params[k])

                    elif k.lower() == "chest_bit" and self.translate_names:
                        params[k] = '"const.ChestBits.%s"' % self.gameconfig.getOptionByIntValue("ChestBits", params[k])

                    elif k.lower() == "monster_bit" and self.translate_names:
                        params[k] = '"const.MonsterBits.%s"' % self.gameconfig.getOptionByIntValue("MonsterBits", params[k])

                    elif k.lower() == "item_type" and self.translate_names:
                        params[k] = '"const.ItemType.%s"' % self.gameconfig.getOptionByIntValue("ItemType", params[k])

                    elif k.lower() == "damage_type" and self.translate_names:
                        params[k] = '"const.Damage.%s"' % self.gameconfig.getOptionByIntValue("Damage", params[k])

                    elif k.lower() == "season" and self.translate_names:
                        params[k] = '"const.Season.%s"' % self.gameconfig.getOptionByIntValue("Season", params[k])

                    elif k.lower() == "face_animation" and self.translate_names:
                        params[k] = '"const.FaceAnimation.%s"' % self.gameconfig.getOptionByIntValue("FaceAnimation", params[k])

                    elif k.lower() == "player" and self.translate_names:
                        params[k] = '"Players.%s"' % self.gameconfig.getOptionByIntValue("Players", params[k])

                    # everything to string
                    if type(params[k]) != type(b"") or type(params[k]) == type(""):
                        params[k] = str(params[k])

                params = ", ".join([ "%s=%s" % (k, params[k]) for k in params ])
                decompiled_line = "  %i: %s(%s)" % (line_num, name, params)

            lines.append(decompiled_line)

        lines.append("end\n") # end lats event

        return "\n".join(lines)


def main():
    import argparse, os, sys
    from openmm68.resource.resourcesystem import RESOURCE_SYSTEM

    # Parasing input prarmeters
    parser = argparse.ArgumentParser(
        description='openmm68.scriptengine.evt'
    )
    parser.add_argument(
        'paths', nargs='*',
        help='Paths to resource archives')
    parser.add_argument(
        '-l', '--list', action='store_true',
        help='List paths to resources')
    parser.add_argument(
        '-e', '--evtfile', default=None,
        help='Path to evt file')
    parser.add_argument(
        '-v', '--mmversion', type=int, default=8,
        help='Version of might and magic used by evt files (6,7,8)')
    parser.add_argument(
        '-a', '--allevtfiles', action='store_true',
        help='Process all evt files')
    parser.add_argument(
        '--extract', action='store_true',
        help='Extract evt file to current dir')
    parser.add_argument(
        '--dump', action='store_true',
        help='Dump data from evt file to current dir')
    parser.add_argument(
        '--decompile', action='store_true',
        help='Decompile evt file to current dir')
    parser.add_argument(
        '-d', '--debug', type=int, choices=[50, 40, 30, 20, 10, 1], default=None,
        help='Set global debug level [CRITICAL=50, ERROR=40, WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level is WARNING.')
    args = parser.parse_args()

    # Logger configuration
    logging.basicConfig()
    logger = logging.getLogger()
    if args.debug is not None:
        logger.setLevel(args.debug)
        logger.info("Set global debug level to: %i" % args.debug)
    else:
        logger.setLevel(30)

    # add archives to VFS and build index
    RESOURCE_SYSTEM.getVFS().registerArchives(args.paths)
    RESOURCE_SYSTEM.getVFS().buildIndex()

    if args.list:
        RESOURCE_SYSTEM.getVFS().dumpVFSStructure()

    if (args.evtfile is None and not args.allevtfiles) and (args.extract or args.dump or args.decompile):
        print("--extract, --dump and --decompile modes require --evtfile or --allevtfiles parameter")
        sys.exit(2)

    files_to_process = []
    if args.evtfile is not None:
        files_to_process = [args.evtfile]
    elif args.allevtfiles:
        files_to_process = []
        for rpath in RESOURCE_SYSTEM.getVFS().index.keys():
            if rpath.endswith(".evt"):
                files_to_process.append(rpath)
    else:
        files_to_process = []

    decompiler = EvtDecompiler(mm_version=args.mmversion)
    for evtfile in files_to_process:
        # get evt file stream
        print("Getting evt file stream: %s" % evtfile )
        fm = RESOURCE_SYSTEM.getFileManager()
        stream = fm.getFile(evtfile)

        # extract
        if args.extract:
            print("Extracting evt file to current dir")
            with open(os.path.basename(evtfile), 'wb') as f:
                f.write(stream.read())

        # dump
        if args.dump:
            print("Dumping data from evt file to current dir")
            with open(os.path.basename(evtfile)+"_dump.txt", 'wb') as f:
                f.write(decompiler.dumpEvtStream(stream).encode('utf-8'))

        # decompile
        if args.decompile:
            print("Decompilling evt file to current dir")
            with open(os.path.basename(evtfile)+"_decompiled.txt", 'wb') as f:
                f.write(decompiler.decompileEvtStream(stream).encode('utf-8'))

if __name__ == "__main__":
    main()
