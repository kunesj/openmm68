#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from collections import OrderedDict
from openmm68.misctools.caseinsensitivedict import CaseInsensitiveDict

from openmm68.config.gameconfigmanager import GameConfigManager

class EvtVariables(object):
    """
    Most information based on MMExtension: https://sites.google.com/site/sergroj/mm/mmextension
    """

    def __init__(self, mm_version):
        if mm_version not in [6,7,8]:
            raise Exception("Unsupported might and magic version %i, only versions 6,7,8 are supported." % mm_version)
        self.mm_version = mm_version

        gameconfig = GameConfigManager()
        gameconfig.loadDefaultConfig(self.mm_version) # loads default might and magic configs

        ## Define Variables
        self.vars = OrderedDict()

        self.vars[0x01] = "SexIs"                     # 01
        self.vars[0x02] = "ClassIs"                   # 02
        self.vars[0x03] = "HP"                        # 03
        self.vars[0x04] = "HasFullHP"                 # 04
        self.vars[0x05] = "SP"                        # 05
        self.vars[0x06] = "HasFullSP"                 # 06
        self.vars[0x07] = "ArmorClass"                # 07
        self.vars[0x08] = "ArmorClassBonus"           # 08
        self.vars[0x09] = "BaseLevel"                 # 09
        self.vars[0x0a] = "LevelBonus"                # 0A
        self.vars[0x0b] = "AgeBonus"                  # 0B
        self.vars[0x0c] = "Awards"                    # 0C
        self.vars[0x0d] = "Experience"                # 0D
        self.vars[0x0e] = None                        # 0E : not used
        self.vars[0x0f] = None                        # 0F : not used
        self.vars[0x10] = "QBits"                     # 10
        self.vars[0x11] = "Inventory"                 # 11
        self.vars[0x12] = "HourIs"                    # 12
        self.vars[0x13] = "DayOfYearIs"               # 13
        self.vars[0x14] = "DayOfWeekIs"               # 14
        self.vars[0x15] = "Gold"                      # 15
        self.vars[0x16] = "GoldAddRandom"             # 16
        self.vars[0x17] = "Food"                      # 17
        self.vars[0x18] = "FoodAddRandom"             # 18
        self.vars[0x19] = "MightBonus"                # 19
        self.vars[0x1a] = "IntellectBonus"            # 1A
        self.vars[0x1b] = "PersonalityBonus"          # 1B
        self.vars[0x1c] = "EnduranceBonus"            # 1C
        self.vars[0x1d] = "SpeedBonus"                # 1D
        self.vars[0x1e] = "AccuracyBonus"             # 1E
        self.vars[0x1f] = "LuckBonus"                 # 1F
        self.vars[0x20] = "BaseMight"                 # 20
        self.vars[0x21] = "BaseIntellect"             # 21
        self.vars[0x22] = "BasePersonality"           # 22
        self.vars[0x23] = "BaseEndurance"             # 23
        self.vars[0x24] = "BaseSpeed"                 # 24
        self.vars[0x25] = "BaseAccuracy"              # 25
        self.vars[0x26] = "BaseLuck"                  # 26
        self.vars[0x27] = "CurrentMight"              # 27
        self.vars[0x28] = "CurrentIntellect"          # 28
        self.vars[0x29] = "CurrentPersonality"        # 29
        self.vars[0x2a] = "CurrentEndurance"          # 2A
        self.vars[0x2b] = "CurrentSpeed"              # 2B
        self.vars[0x2c] = "CurrentAccuracy"           # 2C
        self.vars[0x2d] = "CurrentLuck"               # 2D
        index = 0x2d # define index offset variable

        if self.mm_version == 6:
            self.vars[index+0x01] = "FireResistance"          # 2E
            self.vars[index+0x02] = "ElecResistance"          # 2F
            self.vars[index+0x03] = "ColdResistance"          # 30
            self.vars[index+0x04] = "PoisonResistance"        # 31
            self.vars[index+0x05] = "MagicResistance"         # 32
            self.vars[index+0x06] = "FireResBonus"            # 33
            self.vars[index+0x07] = "ElecResBonus"            # 34
            self.vars[index+0x08] = "ColdResBonus"            # 35
            self.vars[index+0x09] = "PoisonResBonus"          # 36
            self.vars[index+0x0a] = "MagicResBonus"           # 37
            index += 0x0a
        else:
            self.vars[index+0x01] = "FireResistance"          # 2E
            self.vars[index+0x02] = "AirResistance"           # 2F
            self.vars[index+0x03] = "WaterResistance"         # 30
            self.vars[index+0x04] = "EarthResistance"         # 31
            self.vars[index+0x05] = "SpiritResistance"        # 32
            self.vars[index+0x06] = "MindResistance"          # 33
            self.vars[index+0x07] = "BodyResistance"          # 34
            self.vars[index+0x08] = None                      # 35 : LightResistance
            self.vars[index+0x09] = None                      # 36 : DarkResistance
            self.vars[index+0x0a] = None                      # 37 : PhysResistance
            self.vars[index+0x0b] = None                      # 38 : MagicResistance
            self.vars[index+0x0c] = "FireResBonus"            # 39
            self.vars[index+0x0d] = "AirResBonus"             # 3A
            self.vars[index+0x0e] = "WaterResBonus"           # 3B
            self.vars[index+0x0f] = "EarthResBonus"           # 3C
            self.vars[index+0x10] = "SpiritResBonus"          # 3D
            self.vars[index+0x11] = "MindResBonus"            # 3E
            self.vars[index+0x12] = "BodyResBonus"            # 3F
            self.vars[index+0x13] = None                      # 40 : LightResBonus
            self.vars[index+0x14] = None                      # 41 : DarkResBonus
            self.vars[index+0x15] = None                      # 42 : PhysResistance
            self.vars[index+0x16] = None                      # 43 : MagicResistance
            index += 0x16

        # get required number of skills
        if self.mm_version == 6:
            skill_num = 31
        elif self.mm_version == 7:
            skill_num = 37
        else: # >=8
            skill_num = 39

        skills = gameconfig.options("Skills")

        # check if skill list is too long
        if skill_num < len(skills):
            logger.warning("Skill list is longer then was expected")

        # add skill variables
        for i in range(0, skill_num):                       # 38-56/44-68/44-6A
            if not gameconfig.hasOptionByIntValue("Skills", i):
                logger.warning("Missing skill name with index %i" % i)
                self.vars[index+i+1] = None
            else:
                self.vars[index+i+1] = "%sSkill" % gameconfig.getOptionByIntValue("Skills", i)
        index += skill_num

        self.vars[index+0x01] = "Cursed"                    # 57/69/6B
        self.vars[index+0x02] = "Weak"                      # 58/6A
        self.vars[index+0x03] = "Asleep"                    # 59/6B
        self.vars[index+0x04] = "Afraid"                    # 5A/6C
        self.vars[index+0x05] = "Drunk"                     # 5B/6D
        self.vars[index+0x06] = "Insane"                    # 5C/6E
        self.vars[index+0x07] = "PoisonedGreen"             # 5D/6F
        self.vars[index+0x08] = "DiseasedGreen"             # 5E/70
        self.vars[index+0x09] = "PoisonedYellow"            # 5F/71
        self.vars[index+0x0a] = "DiseasedYellow"            # 60/72
        self.vars[index+0x0b] = "PoisonedRed"               # 61/73
        self.vars[index+0x0c] = "DiseasedRed"               # 62/74
        self.vars[index+0x0d] = "Paralysed"                 # 63/75
        self.vars[index+0x0e] = "Unconsious"                # 64/76
        self.vars[index+0x0f] = "Dead"                      # 65/77
        self.vars[index+0x10] = "Stoned"                    # 66/78
        self.vars[index+0x11] = "Eradicated"                # 67/79
        self.vars[index+0x12] = "MainCondition"             # 68/7A/7C
        index += 0x12

        for i in range(0, 100):                             # 69-CC/7B-DE/7D-E0
            self.vars[index+i+1] = "MapVar%i" % i
        index += 100

        self.vars[index+0x01] = "AutonotesBits"             # CD/DF/E1
        self.vars[index+0x02] = "IsMightMoreThanBase"       # CE/E0
        self.vars[index+0x03] = "IsIntellectMoreThanBase"   # CF/E1
        self.vars[index+0x04] = "IsPersonalityMoreThanBase" # D0/E2
        self.vars[index+0x05] = "IsEnduranceMoreThanBase"   # D1/E3
        self.vars[index+0x06] = "IsSpeedMoreThanBase"       # D2/E4
        self.vars[index+0x07] = "IsAccuracyMoreThanBase"    # D3/E5
        self.vars[index+0x08] = "IsLuckMoreThanBase"        # D4/E6
        self.vars[index+0x09] = "PlayerBits"                # D5/E7/E9
        self.vars[index+0x0a] = "NPCs" if (self.mm_version in [6,7]) else None # D6/E8
        self.vars[index+0x0b] = "ReputationIs"              # D7/E9
        index += 0x0b

        for i in range(1,7):                                # D8-DD/... : something time-related in 0x90E85C/...
            self.vars[index+i] = None
        index += 6

        self.vars[index+0x01] = "Flying"                    # DE/F0/F2
        self.vars[index+0x02] = "HasNPCProfession"          # DF/F1
        self.vars[index+0x03] = "TotalCircusPrize"          # E0/F2
        self.vars[index+0x04] = "SkillPoints"               # E1/F3
        self.vars[index+0x05] = "MonthIs"                   # E2/F4
        index += 0x05

        if self.mm_version >= 7:
            self.vars[index+0x01] = "Counter1"                # F5/F7
            self.vars[index+0x02] = "Counter2"                # F6
            self.vars[index+0x03] = "Counter3"                # F7
            self.vars[index+0x04] = "Counter4"                # F8
            self.vars[index+0x05] = "Counter5"                # F9
            self.vars[index+0x06] = "Counter6"                # FA
            self.vars[index+0x07] = "Counter7"                # FB
            self.vars[index+0x08] = "Counter8"                # FC
            self.vars[index+0x09] = "Counter9"                # FD
            self.vars[index+0x0a] = "Counter10"               # FE/100
            index += 0x0a

            for i in range(1, 21):          # FF-112/101-114 : set something to current time and play sound (not used by CMP)
                self.vars[index+i] = None   # (no metter what value you use) (the value set is never used)
            index += 20

            self.vars[index+0x01] = "Reputation"              # 113/115
            index += 1

            for i in range(1, 30):                            # 114/116
                self.vars[index+i] = "History%i" % i
            index += 29

            self.vars[index+0x01] = "MapAlert"                # 131/133
            self.vars[index+0x02] = "BankGold"                # 132/134
            self.vars[index+0x03] = "Deaths"                  # 133/135
            self.vars[index+0x04] = "MontersHunted"           # 134/136
            self.vars[index+0x05] = "PritsonTerms"            # 135/137
            self.vars[index+0x06] = "ArenaWinsPage"           # 136/138
            self.vars[index+0x07] = "ArenaWinsSquire"         # 137/138
            self.vars[index+0x08] = "ArenaWinsKnight"         # 138/13A
            self.vars[index+0x09] = "ArenaWinsLord"           # 139/13B
            self.vars[index+0x0a] = "Invisible"               # 13A/13C
            self.vars[index+0x0b] = "IsWearingItem"           # 13B/13D
            index += 0x0b

            if self.mm_version not in [6,7]:
                self.vars[index+0x01] = "Players"             # 13E
                index +=1

        # test correct indexes
        if self.mm_version == 6:
            if self.vars[0xe2] != "MonthIs": raise Exception("Bad Evt Variables! v6")
        elif self.mm_version == 7:
            if self.vars[0x13b] != "IsWearingItem": raise Exception("Bad Evt Variables! v7")
        else: # self.mm_version == 8
            if self.vars[0x13e] != "Players": raise Exception("Bad Evt Variables! v8+")

        ## Generate name lookup table
        self.name_lookup = {}
        for k in self.vars:
            if self.vars[k] is not None:
                self.name_lookup[self.vars[k]] = k
        self.name_lookup = CaseInsensitiveDict(self.name_lookup)

    def isDefined(self, var_num):
        return var_num in self.vars

    def isDefinedName(self, var_name):
        return var_name in self.name_lookup

    def getNameById(self, var_num):
        """ Returns variable name """
        if not self.isDefined(var_num):
            logger.warning("EvtVariable with index %i is not defined, returning 'Var%i'" % (var_num, var_num))
            return "Var%i" % var_num
        else:
            return self.vars[var_num]
