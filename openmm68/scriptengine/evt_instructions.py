#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from collections import OrderedDict
from openmm68.misctools.attrdict import AttrDict

class EvtInstructions(object):
    """
    1 byte instructions

    Most information based on MMExtension: https://sites.google.com/site/sergroj/mm/mmextension

    Variables have specific indexes: (Cmp, Add, Subtract, Set, CanShowTopic_Cmp) ["var_index"]
        look in openmm68.evt_variables.EvtVariables

    FacetBits: (SetFacetBit, SetFacetBitOutdoors) ["face_bit"=bytes4]
        look in openmm68.evt_constants.EvtConstants

    ChestBit: (SetChestBit) ["chest_bit"=byte4]
        look in openmm68.evt_constants.EvtConstants

    MonsterBits: (SetMonGroupBit, SetMonsterBit) ["monster_bit" = bytes4]
        look in openmm68.evt_constants.EvtConstants

    PlayerVariable: (FaceAnimation, Player) ["player"]
        look in openmm68.evt_constants.EvtConstants

    ...

    """

    def __init__(self, mm_version):
        self.mm_version = mm_version # 6,7,8
        var_num_type = "uint16" # TODO - is uint8 in mm6 ????

        ## Define Instructions
        ######################
        Instructions = AttrDict()

        Instructions.Cmd00 = {"hex":0x0, "params":OrderedDict([ ])}

        Instructions.Exit = {"hex":0x1, "params":OrderedDict([ ("unused_byte","uint8"), ])}
        # End of EVT function, parameter is allways 0x00 (???)

        Instructions.EnterHouse = {"hex":0x2, "params":OrderedDict([ ("id","uint32"), ])}
        # -- Id of 2DEvent
        # -- 600(dec) = you won
        # -- 601(dec) = you won 2 / you lost

        Instructions.PlaySound = {"hex":0x3, "params":OrderedDict([ ("id","int32"), ("x","int32"), ("y","int32") ])}

        Instructions.Hint = {"hex":0x4, "params":OrderedDict([ ("str_id", "uint8"), ])}
        # Param is index of string in *.str file

        Instructions.MazeInfo = {"hex":0x5, "params":OrderedDict([ ("str_id", "uint8"), ])}

        movetomap_houseid_type = "uint8" if (self.mm_version in [6,7]) else "uint16"
        Instructions.MoveToMap = {"hex":0x6, "params":OrderedDict([ ("x","int32"), ("y","int32"), ("z","int32"), ("direction","int32"), ("look_angle","int32"), ("speed_z","int32"), ("house_id",movetomap_houseid_type), ("icon","uint8"), ("name","nullstring") ])}
        # -- Notes:
        # -- If cancel is pressed, the execution is stopped
        # -- If X,Y,Z,Direction,LookAngle,SpeedZ are all 0, the party isn't moved
        # -- If HouseId and Icon are 0, the enter dungeon dialog isn't shown
        # 'Direction'  -- -1 = special case
        # 'HouseId'  -- 2DEvent ID
        # 'Name' -- if starts with "0" => current map

        Instructions.OpenChest = {"hex":0x7, "params":OrderedDict([ ("id","uint8"), ])}

        Instructions.FaceExpression = {"hex":0x8, "params":OrderedDict([ ("player","uint8"), ("frame","uint8"), ])}

        Instructions.DamagePlayer = {"hex":0x9, "params":OrderedDict([ ("player","uint8"), ("damage_type","uint8"), ("damage","int32") ])}

        Instructions.SetSnow = {"hex":0xa, "params":OrderedDict([ ("effect_id","uint8"), ("on","bool8") ])}
        # 'EffectId'  -- only 00 available

        Instructions.SetTexture = {"hex":0xb, "params":OrderedDict([ ("facet","int32"), ("name","nullstring") ])}

        if self.mm_version == 6:
            Instructions.SetTextureOutdoors = {"hex":0xc, "params":OrderedDict([ ("model","uint32"), ("facet","uint32"), ("name","nullstring") ])}
        else:
            Instructions.ShowMovie = {"hex":0xc, "params":OrderedDict([ ("double_size","uint8"), ("unk","bool8"), ("name","nullstring") ])}
            # 'unk'  -- use 0 most of time

        Instructions.SetSprite = {"hex":0xd, "params":OrderedDict([ ("sprite_id","int32"), ("visible","uint8"), ("name","nullstring") ])}
        # 'Visible'  -- bit 0x20 of sprite
        # 'Name' -- in case of "0" sprite is removed (?)

        Instructions.Cmp = {"hex":0xe, "params":OrderedDict([ ("var_index",var_num_type), ("value","int32"), ("jump", "uint8") ])}
        # 'VarNum'  -- variable number
        # 'Value'  -- compare with
        # YJump  '  jump'

        Instructions.SetDoorState = {"hex":0xf, "params":OrderedDict([ ("id","uint8"), ("state","uint8") ])}
        # 'State'  -- 00 - off, 01 - on, 02 - switch state if it isn't moving, 03 - switch state

        Instructions.Add = {"hex":0x10, "params":OrderedDict([ ("var_index",var_num_type), ("value","int32") ])}

        Instructions.Subtract = {"hex":0x11, "params":OrderedDict([ ("var_index",var_num_type), ("value","int32") ])}

        Instructions.Set = {"hex":0x12, "params":OrderedDict([ ("var_index",var_num_type), ("value","int32") ])}
        # sets value of variables?

        Instructions.SummonMonsters = {"hex":0x13, "params":OrderedDict([ ("type_index_in_map_stats","uint8"), ("level","uint8"), ("count","uint8"), ("x","int32"), ("y","int32"), ("z","int32") ])}
        if self.mm_version != 6:
            Instructions.SummonMonsters["params"]["npc_group"] = "int32"
            Instructions.SummonMonsters["params"]["unk"] = "int32"

        Instructions.Cmd14 = {"hex":0x14, "params":OrderedDict([ ("unk_1","int32"), ("unk_2","uint8") ])}

        Instructions.CastSpell = {"hex":0x15, "params":OrderedDict([ ("number","uint8"), ("skill_level","uint8"), ("skill","uint8"), ("from_x","int32"), ("from_y","int32"), ("from_z","int32"), ("to_x","int32"), ("to_y","int32"), ("to_z","int32") ])}
        # 'SkillLevel'  -- 0 - 3

        Instructions.SpeakNPC = {"hex":0x16, "params":OrderedDict([ ("npc","int32"), ])}

        Instructions.SetFacetBit = {"hex":0x17, "params":OrderedDict([ ("id","int32"), ("face_bit","uint32"), ("on","bool8") ])}
        # Bit=bytes4
        # 'Id'  -- id of facets group in MM7-8
        # BitsArray[0x17] = 'FacetBits'

        if self.mm_version == 6:
            Instructions.SetFacetBitOutdoors = {"hex":0x18, "params":OrderedDict([ ("model","int32"), ("facet","int32"), ("face_bit","uint32"), ("on","bool8") ])}
            # 'Model'  -- number of structure in map (those shown on left click in MapViewer)
            # 'Facet'  -- -1 = for all faces in structure
            # BitsArray[0x18] = 'FacetBits'
        else:
            Instructions.SetMonsterBit = {"hex":0x18, "params":OrderedDict([ ("monster","int32"), ("monster_bit","uint32"),("on","bool8") ])}
            # BitsArray[0x18] = 'MonsterBits'

        Instructions.RandomGoTo = {"hex":0x19, "params":OrderedDict([ ("jump_a","uint8"), ("jump_b","uint8"), ("jump_c","uint8"), ("jump_d","uint8"), ("jump_e","uint8"), ("jump_f","uint8") ])}
        # Randomly Jumps to one of the given line numbers
        # -- 0 to skip a label

        Instructions.Question = {"hex":0x1a, "params":OrderedDict([ ("question","int32"), ("answer1","int32"), ("answer2","int32"), ("jump_ok","uint8") ])}
        # YJump  '  jump(ok)'

        Instructions.Cmd1B = {"hex":0x1b, "params":OrderedDict([ ("unk_1","uint8"), ("unk_2","uint8") ])}

        Instructions.Cmd1C = {"hex":0x1c, "params":OrderedDict([ ("unk","uint8"), ])}

        Instructions.StatusText = {"hex":0x1d, "params":OrderedDict([ ("index","int32"), ])}
        # Param is index of string in npctext.txt

        Instructions.SetMessage = {"hex":0x1e, "params":OrderedDict([ ("index","int32"), ])}
        # Param is index of string in npctext.txt

        Instructions.OnTimer = {"hex":0x1f, "params":OrderedDict([ ("each_year","bool8"), ("each_month","bool8"), ("each_week","bool8"), ("start_hour","uint8"), ("start_minute","uint8"), ("start_second","uint8"), ("interval_in_half_minutes","uint16"), ("unused_bytes","uint16") ])}
        # 'EachWeek'  -- else each day

        Instructions.SetLight = {"hex":0x20, "params":OrderedDict([ ("id","int32"), ("on","bool8") ])}
        # 'Id'  -- light group id in MM7-8

        Instructions.SimpleMessage = {"hex":0x21, "params":OrderedDict([ ("unused_byte","uint8") ])}

        Instructions.SummonObject = {"hex":0x22, "params":OrderedDict([ ("type","int32"), ("x","int32"), ("y","int32"), ("z","int32"), ("speed","int32"), ("count","uint8"), ("random_angle","bool8") ])}
        # 'Type'  -- dobjlist.bin

        Instructions.Player = {"hex":0x23, "params":OrderedDict([ ("player","uint8"), ])}
        # Maybe defines on what player character should next commands be used on ?

        Instructions.GoTo = {"hex":0x24, "params":OrderedDict([ ("jump","uint8"), ])}
        # Jump to specifited line in EVT function.
        # YJump  'jump'

        Instructions.OnLoadMap = {"hex":0x25, "params":OrderedDict([ ("unused_byte","uint8"), ])}
        # Param is allways 0x00 ???

        Instructions.OnLongTimer = {"hex":0x26, "params":Instructions.OnTimer["params"] }

        Instructions.SetNPCTopic = {"hex":0x27, "params":OrderedDict([ ("npc","int32"), ("index","uint8"), ("event","int32") ])}

        Instructions.MoveNPC = {"hex":0x28, "params":OrderedDict([ ("npc","uint32"), ("house_id","uint32") ])}
        # 'HouseId' -- 2DEvents

        Instructions.GiveItem = {"hex":0x29, "params":OrderedDict([ ("strength","uint8"), ("item_type","uint8"), ("id","uint32") ])}
        # 'Strength'  -- 1-6 (like described in the end of STDITEMS.TXT)
        # -- If Id is 0, the item is chosen by random from the specified class with specified strength,
        # -- otherwise, ItemType and ItemStremgth determine the enchancements
        # -- If both unk_1 and unk_2 are 0, the item is identified and has no enchancements

        Instructions.ChangeEvent = {"hex":0x2a, "params":OrderedDict([ ("new_event","uint32"), ])}
        # 'NewEvent'  -- (global event) for barrels etc.

        Instructions.CheckSkill = {"hex":0x2b, "params":OrderedDict([ ("skill_type","uint8"), ("skill_mastery","uint8"), ("or_skill_level","int32"), ("jump_geq","uint8") ])}
        # 'OrSkillLevel'  -- Includes "Double effect" enchantments and NPC bonuses
        # YJump  '  jump(>=)'

        # Following instructions are only in MM 7,8
        if self.mm_version >= 7:
            Instructions.CanShowTopic_Cmp = {"hex":0x2c, "params":OrderedDict([ ("var_index",var_num_type), ("value","int32"), ("jump","uint8") ])}
            # More information in description of Cmp instruction"
            # YJump  '  jump'

            Instructions.CanShowTopic_Exit = {"hex":0x2d, "params":OrderedDict([ ("unused_byte","uint8"), ])}
            # Param is allways 0x00 ???

            Instructions.CanShowTopic_Set = {"hex":0x2e, "params":OrderedDict([ ("visible","bool8"), ])}

            Instructions.SetNPCGroupNews = {"hex":0x2f, "params":OrderedDict([ ("npc_group","uint32"), ("npc_news","uint32") ])}

            Instructions.SetMonsterGroup = {"hex":0x30, "params":OrderedDict([ ("monster","uint32"), ("group","uint32") ])}

            Instructions.SetNPCItem = {"hex":0x31, "params":OrderedDict([ ("npc","int32"), ("item","int32"), ("on","bool8") ])}

            Instructions.SetNPCGreeting = {"hex":0x32, "params":OrderedDict([ ("npc","int32"), ("greeting","int32") ])}

            Instructions.CheckMonstersKilled = {"hex":0x33, "params":OrderedDict([ ("check_type", "uint8"), ("id", "uint32"), ("count", "uint8") ])}
            if self.mm_version not in [6,7]:
                Instructions.CheckMonstersKilled["params"]["invisible_as_dead"] = "uint8"
            Instructions.CheckMonstersKilled["params"]["jump_geq"] = "uint8"
            # 'CheckType'  -- 0 = any monster, 1 - in group, 2 - of type, 3 - specific monster, 4 - specific monster by name (MM8)
            # 'Id'  -- 0 - not used, 1 - group id, 2 - monster type, 3 - monster id, 4 - id in placemon.txt (MM8 only)
            # 'Count'  -- 0 - all must be killed, else a number of monsters that must be killed
            # 'InvisibleAsDead'  -- a monster can be invisible, like pirates in Ravenshore in MM8 before you enter Regna
            # YJump  '  jump(>=)'

            Instructions.CanShowTopic_CheckMonstersKilled = {"hex":0x34, "params":Instructions.CheckMonstersKilled["params"] }

            Instructions.OnLeaveMap = {"hex":0x35, "params":OrderedDict([ ("unused_byte","uint8"), ])}
            # Param is allways 0x00 ???

            Instructions.ChangeGroupToGroup = {"hex":0x36, "params":OrderedDict([ ("old","int32"), ("new","int32") ])}

            Instructions.ChangeGroupAlly = {"hex":0x37, "params":OrderedDict([ ("group","int32"), ("ally","int32") ])}

            Instructions.CheckSeason = {"hex":0x38, "params":OrderedDict([ ("season","uint8"), ("jump_ok","uint8") ])}
            # YJump  '  jump(ok)'

            Instructions.SetMonGroupBit = {"hex":0x39, "params":OrderedDict([ ("npc_group", "int32"), ("monster_bit", "uint32"), ("on", "bool8") ])}
            # Bit=bytes4
            # BitsArray[0x39] = 'MonsterBits'

            Instructions.SetChestBit = {"hex":0x3a, "params":OrderedDict([ ("chest_id", "int32"), ("chest_bit", "uint32"), ("on", "bool8")])}
            # BitsArray[0x3A] = 'ChestBits'

            Instructions.FaceAnimation = {"hex":0x3b, "params":OrderedDict([ ("player","uint8"), ("face_animation","uint8") ])}

            Instructions.SetMonsterItem = {"hex":0x3c, "params":OrderedDict([ ("monster", "int32"), ("item", "int32"), ("has", "bool8") ])}

        # Following instructions are only in MM 8
        if self.mm_version >= 8:
            Instructions.OnDateTimer = {"hex":0x3d, "params":OrderedDict([ ("id","uint8"), ("hour_plus_1","uint8"), ("day_plus_1","uint8"), ("week_plus_1","uint8"), ("year_plus_1","uint8"), ("full_year_plus_1","uint16"), ("enable_after","uint8") ])}
            # 'Id'  -- 0x3D00 is added to this
            # 'EnableAfter'  -- works every half of second
            # -- By default date timer is disabled!

            Instructions.EnableDateTimer = {"hex":0x0, "params":OrderedDict([ ("id_plus_0x3D00","uint16"), ("on","bool8") ])}
            # 'IdPlus0x3D00'  -- add 0x3D00 to Id of Cmd_3D

            Instructions.StopDoor = {"hex":0x3f, "params":OrderedDict([ ("id","int32"), ])}

            Instructions.CheckItemsCount = {"hex":0x40, "params":OrderedDict([ ("min_item_index", "uint16"), ("max_item_index", "uint16"), ("count", "uint16"), ("jump_geq", "uint8") ])}
            # YJump  '  jump(>=)'

            Instructions.RemoveItems = {"hex":0x41, "params":OrderedDict([ ("min_item_index", "uint16"), ("max_item_index", "uint16"), ("count", "uint16") ])}

            Instructions.Jump = {"hex":0x42, "params":OrderedDict([ ("direction", "int16"), ("z_angle", "int16"), ("speed", "int16"), ("unk", "int16") ])}

            Instructions.IsTotalBountyInRange = {"hex":0x43, "params":OrderedDict([ ("min_gold", "int32"), ("max_gold", "int32"), ("jump_ok", "uint8") ])}
            # YJump  '  jump(ok)'

            Instructions.IsPlayerInParty = {"hex":0x44, "params":OrderedDict([ ("id","uint32"), ("jump_ok", "uint8") ])}
            # 'Id' -- Roster.txt
            # YJump  '  jump(ok)'

        self.Instructions = Instructions

        ## Build lookup table - get keys by hex value
        self.hex_lookup = {}
        for k in self.Instructions:
            self.hex_lookup[self.Instructions[k]["hex"]] = k

    def isDefined(self, instruction_value):
        return instruction_value in self.hex_lookup

    def getName(self, instruction_value):
        if self.isDefined(instruction_value):
            return self.hex_lookup[instruction_value]
        else:
            logger.warning("Undefined instruction name for instruction 0x%02x" % instruction_value)
            return "0x%02x" % instruction_value

    def getParameters(self, instruction_value):
        if self.isDefined(instruction_value):
            return self.Instructions[self.hex_lookup[instruction_value]]["params"]
        else:
            logger.warning("Undefined parameters for instruction 0x%02x" % instruction_value)
            return {}

    def parseParameters(self, stream, instruction_value):
        """ stream address should be at the start of parameter data """
        params = self.getParameters(instruction_value)

        parsed_params = OrderedDict()
        for key in params:
            val_type = params[key].lower().strip()
            parsed_params[key] = stream.readType(val_type)

        return parsed_params




