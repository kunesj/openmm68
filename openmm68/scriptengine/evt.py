#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import io
from openmm68.misctools.binarydatastream import BinaryDataStream

from openmm68.scriptengine.evt_instructions import EvtInstructions
from openmm68.scriptengine.evt_decompiler import EvtDecompiler

class EvtInterpreter(object):
    """
    There are no EVT function headers in EVT file, just EVT lines that need to be separated by EVENT_NUMBER value to functions.

    Structure of EVT bytecode line:
        LINE_LENGTH, EVENT_NUMBER, LINE_NUMBER, INSTRUCTION, PARAMETERS

        LINE_LENGTH - Length of EVT line (without LINE_LENGTH byte); 1 byte
        EVENT_NUMBER - Number (name) of EVT function; 2 bytes = uint16
        LINE_NUMBER - Line number in EVT function (there can be multiple lines in one EVT function with same line number); 1 byte
        INSTRUCTION - Instruction index; 1 byte
        PARAMETERS - Parameters of instruction; LINE_LENGTH-4 bytes

    There can be (not allways!) two first lines with LINE_NUMBER == 0, in most cases the first one is Hint instruction.
    """
    def __init__(self, stream=None, mm_version=8):
        if mm_version not in [6,7,8]:
            raise Exception("Unsupported might and magic version %i, only versions 6,7,8 are supported." % mm_version)
        self.mm_version = mm_version

        self.stream = None
        self.stream_len = 0

        self.event_index = {}
        """Start addresses of event functions and addresses of lines for JUMP instructions.
        If there are multiple lines with same line number, address of last parsed line will be used.
        { event_id: { 'addr': start_address, 'jumps': {line_id: start_address, ... } }, ... } """

        self.event_strings = []
        """ parsed *.str files, required by some instructions """

        if stream is not None:
            self.loadEvtStream(stream)

    def loadEvtStream(self, stream):
        self.stream = stream
        self.stream.seek(0, io.SEEK_SET)
        self.stream_len = stream.getLength()
        self.event_index = self.parseEventIndex()

    def loadStrStream(self, stream):
        self.event_strings = []
        # TODO

    def parseEventIndex(self):
        parsed_evt = EvtDecompiler.parseEvtStream(self.stream)

        events = {} # { event_id: { 'addr': start_address, 'jumps': {line_id: start_address, ... } }, ... }
        for i, evt_line in enumerate(parsed_evt):
            event_number = evt_line["EVENT_NUMBER"]

            if event_number not in events:
                events[event_number] = {"addr":evt_line["ADDRESS"], "jumps": {}}

            # check if line with this LINE_NUMBER was already added
            if evt_line["LINE_NUMBER"] != 0 and evt_line["LINE_NUMBER"] in events[event_number]["jumps"]:
                logger.warning("parseEventIndex(): %i# already added instruction with this LINE_NUMBER %i to EVENT_NUMBER %i jump list" % (i, evt_line["LINE_NUMBER"], event_number))

            # jump address is always pointing to last line with specific LINE_NUMBER
            events[event_number]["jumps"][evt_line["LINE_NUMBER"]] = evt_line["ADDRESS"]

        return events

    def goToEvent(self, event_number):
        """ Moves stream read position to start of event with given id. """
        if event_number not in self.event_index:
            raise Exception("Event number %i is not in index!" % event_number)
        else:
            self.stream.seek(self.event_index[event_number]["addr"], io.SEEK_SET)

    def goToLine(self, event_number, line_number):
        """ Moves stream read position to start of specific line in specific event function. """
        if event_number not in self.event_index:
            raise Exception("Event number %i is not in index!" % event_number)
        elif line_number not in self.event_index[event_number]["jumps"]:
            raise Exception("Line number %i for event %i is not in index!" % (line_number, event_number))
        else:
            self.stream.seek(self.event_index[event_number]['jumps'][line_number], io.SEEK_SET)

    def getStr(self, str_id):
        """ Get strings parsed from *.str files, required by some instructions """
        if str_id < 0 or str_id >= len(self.event_strings):
            logger.warning("Undefined string id %i" % str_id)
            return ""
        else:
            return self.event_strings[str_id]

    def executeInstruction(self, address):
        pass # TODO

    def run(self):
        pass # TODO
