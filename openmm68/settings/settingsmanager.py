#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import os
import appdirs
import pkg_resources

import configparser

class SettingsManager(object):
    """
    Engine settings. For game specific configurations go to openmm68.config.
    All keys in sections are accessible in a case-insensitive manner

    Use:
        from openmm68.settings.settingsmanager import SETTINGS
    """

    def __init__(self):
        logger.info("Initing SettingsManager...")

        self.config = None
        self.clearSettings()

        ## Load default and user settings

        self.user_config_dir = appdirs.user_config_dir('openmm68')
        self.settings_user_path = os.path.join(self.user_config_dir, "settings.cfg")
        self.settings_default_path = pkg_resources.resource_filename("openmm68.settings", "settings_default.cfg")

        # if user settings doesn't exist => create empty file
        if not os.path.exists(self.settings_user_path):
            logger.info("Creating missing user setting file in '%s'..." % self.settings_user_path)
            try:
                os.makedirs(self.user_config_dir, exist_ok=True)
                with open(self.settings_user_path, 'w') as f:
                    f.write("# User Settings File")
            except Exception:
                logger.exception("Couldn't create empty user settings file!")

        self.loadSettings(self.settings_default_path)
        self.loadSettings(self.settings_user_path)

    @classmethod
    def getObject(cls):
        return cls()

    def clearSettings(self):
        self.config = configparser.ConfigParser(interpolation=None, inline_comment_prefixes=('#',))
        # make case sensitive
        self.config.optionxform = lambda option: option

    def loadSettings(self, path, update=True):
        logger.info("Loading settings file '%s'..." % path)

        if not update:
            self.clearSettings()

        try:
            self.config.read(path, encoding="utf-8")
        except Exception:
            logger.exception("Couldn't load settings file '%s'!" % path)

    def get(self, section, option):
        val = self.config.get(section, option)
        if type(val) == type([]): # remove list container
            return val[0]
        else:
            return val

    def getBoolean(self, section, option):
        return self.config.getboolean(section, option)

    def getInt(self, section, option):
        str_num = self.config.get(section, option).strip()
        if str_num.startswith("0x"):
            return int(str_num, 16)
        else:
            return self.config.getint(section, option)

    def getFloat(self, section, option):
        return self.config.getfloat(section, option)

    def getRGB(self, section, option):
        rgb_str = self.get(section, option)
        if len(rgb_str.split(" ")) != 3:
            raise ValueError("Not valid RGB string! %s/%s = '%s'" % (section, option, rgb_str))
        return tuple([ float(x) for x in rgb_str.split(" ") ])

    def getRGBA(self, section, option):
        rgb_str = self.get(section, option)
        if len(rgb_str.split(" ")) != 4:
            raise ValueError("Not valid RGBA string! %s/%s = '%s'" % (section, option, rgb_str))
        return tuple([ float(x) for x in rgb_str.split(" ") ])

""" Import this object variable in every file that needs to use SettingsManager """
SETTINGS = SettingsManager.getObject()
