#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import openmm68.misc.mm_tools as mt

def parseInt32Vector(stream):
    return tuple([stream.readInt32() for i in range(3)])

class ODMEntity(object):

    def __init__(self, stream, name, odm_version=8):
        self.declist_id = None #WORD, 0x00
        self.AIattrMarkers = None #WORD, 0x02
        self.origin = (0,0,0) #mm_int_vec3_s, 0x04
        self.facing = None #int, 0x10
        self.evt1 = None #WORD, 0x14
        self.evt2 = None # WORD, 0x16
        self.var1 = None #WORD, 0x18
        self.var2 = None #WORD, 0x1a

        self.spec_trig = 0 # WORD
        self.padding = 0 #WORD
        self.name = None #char[self.entname_size]

        self.parseODMEntity(stream, name, odm_version)

    def parseODMEntity(self, stream, name, odm_version):
        # set name
        self.name = name

        # parse entity
        self.declist_id = stream.readUInt16()
        self.AIattrMarkers = mt.parseBitAttributes(stream, "SpriteBits", byte_num=2) # TODO - Use them
        self.origin = parseInt32Vector(stream)
        self.facing = stream.readInt32()
        self.evt1 = stream.readUInt16()
        self.evt2 = stream.readUInt16()
        self.var1 = stream.readUInt16()
        self.var2 = stream.readUInt16()

        if odm_version > 6:
            self.spec_trig = stream.readUInt16()
            self.padding = stream.readUInt16()
        else:
            self.spec_trig = 0
            self.padding = 0
