#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import os, io
import numpy as np

from openmm68.misctools.binarydatastream import BinaryDataStream
from openmm68.misctools.attrdict import AttrDict

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM

from openmm68.odm.odmmodel import ODMModel
from openmm68.odm.odmentity import ODMEntity

class ODMFile(object):

    def __init__(self, stream=None):
        """ Input: [openmm68.misctools.binarydatastream.BinaryDataStream()] """
        # init data variables
        self.clearVariables()

        if stream is not None:
            self.parseODMFile(stream)

    def clearVariables(self):
        logger.debug("Clearing ODM variables...")
        self.header = AttrDict()
        self.header.version = None
        self.header.entsize = None
        self.header.spawnsize = None

        self.data = AttrDict()
        self.data.models = []
        self.data.entites = []
        self.data.idlist = []
        self.data.OMAP = None # is made of 128*128 UInt32
        self.data.spawns = []

        self.data.terrain = AttrDict()
        # terrain data - static values
        self.data.terrain.tilesize = 512
        self.data.terrain.heightsize = 16
        self.data.terrain.ntilex = 128 # constant
        self.data.terrain.ntiley = 128 # constant
        # terrain data - maps
        self.data.terrain.heightmap = None # heightmap is ntilex*ntiley (0x80*0x80 == 128*128)
        self.data.terrain.tilemap = None # index of tile for every texture
        self.data.terrain.zeroedmap = None
        # terrain data - other variables
        self.data.terrain.mastertile = None # defines which tile file should I parse to get tilemap, 0="language/dtile.bin"
        self.data.terrain.tile_idx_tbl = None # (hz, idx)
        self.data.terrain.num_TerNorm = None
        self.data.terrain.CMAP1 = None
        self.data.terrain.CMAP2 = None
        self.data.terrain.TerNorm = None
        # terrain data - tile texture names
        # "name1" = foreground texture name, "name2" = background group (texture) name
        self.data.terrain.tiletexturenames = []

    def getTileTextureNames(self):
        """
        Requires inited RESOURCE_SYSTEM and loaded "language/*" (version==8) or "icons/*" (version!=8).
        Automaticaly sets self.data.terrain.tiletexturenames values.

        parsed mastertile format: [{'hz': [int, int, int], 'name': str}, ... ]
        """
        # Get mastertile path
        if self.header.version == 8:
            if self.data.terrain.mastertile == 0:
                tilepath = "language/dtile.bin"
            elif self.data.terrain.mastertile == 1:
                tilepath = "language/dtile2.bin"
            elif self.data.terrain.mastertile== 2:
                tilepath = "language/dtile3.bin"
            else:
                tilepath = "language/dtile.bin"
        else: # TODO - untested
            tilepath = "icons/dtile.bin"

        # get parsed mastertile
        tbl = RESOURCE_SYSTEM.getConfigFileManager().getConfig(tilepath)

        # alocate tex_names array
        tex_names = []
        for i in range(0, 256):
            tex_names.append({'name1':None, 'name2':None})

        # get texture names for specific value
        tile_idx_tbl = self.data.terrain.tile_idx_tbl
        for i in range(0, 256):
            if i >= 0xc6: # 198
                index = i - 0xc6 + tile_idx_tbl[3]['idx']
            elif i < 0x5a: # 90
                index = i
            else: # 90-197
                n = (i-0x5a)//0x24
                index = tile_idx_tbl[n]['idx'] - n*0x24 # //??? was [n*2] may be [n]
                index += i-0x5a

            # fix undefined texture names
            if tbl[index]['name'] == '':
                tbl[index]['name'] = 'pending'

            # set main texture name
            tex_names[i]['name1'] = tbl[index]['name']

            # set group name
            if tbl[index]['hz'][2] == 0x300:
                group = tbl[index]['hz'][0]
                for j in range(0, 4):
                    if tile_idx_tbl[j]['hz'] == group:
                        idx2 = tile_idx_tbl[j]['idx']
                        name2 = tbl[idx2]['name']
                        if name2 != '':
                            tex_names[i]['name2'] = name2

        self.data.terrain.tiletexturenames = tex_names
        return tex_names

    def parseODMFile(self, stream):
        """ Input: openmm68.misctools.binarydatastream.BinaryDataStream() """
        logger.info("Parsing ODM file...")
        stream.seek(0x0, io.SEEK_SET)
        self.clearVariables()

        ## Parse header
        ##############################
        logger.info("Parsing ODM file - Header")
        stream.seek(0x40, io.SEEK_SET)
        signature = stream.readString(0x10, strip_null=True, raw=True)

        if signature.startswith(b"MM6 Outdoor v1.11"):
            self.header.version = 6
            self.header.entsize = 0x1c
            self.header.spawnsize = 0x14

        elif signature.startswith(b"MM6 Outdoor v7.00"):
            self.header.version = 7
            self.header.entsize = 0x20
            self.header.spawnsize = 0x18

        else:
            for b in signature:
                if b != b'\x00':
                    raise Exception("Unknown ODM version: %s" % (signature,))
            self.header.version = 8
            self.header.entsize = 0x20
            self.header.spawnsize = 0x18

        logger.debug("header.version=%i, header.entsize=%i, header.spawnsize=%i" % (self.header.version, self.header.entsize, self.header.spawnsize))

        ## Parse Terrain
        ###############################
        logger.info("Parsing ODM file - Terrain")

        # read terrain header
        stream.seek(0x5f, io.SEEK_SET)
        self.data.terrain.mastertile = stream.readUInt8()
        stream.seek(0xa0, io.SEEK_SET)
        self.data.terrain.tile_idx_tbl = []
        for i in range(0,4):
            tile_idx = {'hz':stream.readUInt16(), 'idx':stream.readUInt16()}
            self.data.terrain.tile_idx_tbl.append(tile_idx)

        # get maps offset
        if self.header.version == 8:
            maps_offset = 0xb4
        else:
            maps_offset = 0xb0

        # get maps
        stream.seek(maps_offset, io.SEEK_SET);
        self.data.terrain.heightmap = np.reshape( np.matrix(stream.readByteArray(0x4000)), (128,128) ) # (y,x)
        self.data.terrain.tilemap = np.reshape( np.matrix(stream.readByteArray(0x4000)), (128,128) )
        self.data.terrain.zeroedmap = np.reshape( np.matrix(stream.readByteArray(0x4000)), (128,128) )

        # get other variables
        if self.header.version > 6:
            self.data.terrain.num_TerNorm = stream.readInt32()
            self.data.terrain.CMAP1 = stream.readByteArray(0x20000)
            self.data.terrain.CMAP2 = stream.readByteArray(0x10000)
            self.data.terrain.TerNorm = stream.readByteArray(self.data.terrain.num_TerNorm*12) # TODO dont know what for, should be made of 'short' C types
        else:
            self.data.terrain.num_TerNorm = 0
            self.data.terrain.CMAP1 = None
            self.data.terrain.CMAP2 = None
            self.data.terrain.TerNorm = None

        ## Parse Models
        ###############################
        logger.info("Parsing ODM file - Models")
        self.data.models = []
        num_bddata = stream.readUInt32(); logger.debug("Number of models: %i" % (num_bddata,))
        models_header_off = stream.tell()
        models_data_off = models_header_off+(num_bddata*0xbc) # one model header has size of 0xbc

        this_data_off = models_data_off
        for i in range(0, num_bddata):
            this_header_off = models_header_off+(i*0xbc)
            model = ODMModel(stream, this_header_off, this_data_off)
            self.data.models.append(model)
            this_data_off = stream.tell()

        ## Parse Entites (sprites)
        ###############################
        logger.info("Parsing ODM file - Entites")
        num_entites = stream.readUInt32(); logger.debug("Number of entities: %i" % (num_entites,))
        if num_entites > 0xbb8: # down know why it's bad number
            raise Exception("Bad number of Entities")
        entities_off = stream.tell()
        entities_names_off = entities_off+(num_entites*self.header.entsize)

        # get names of entities
        entnames = []
        stream.seek(entities_names_off, io.SEEK_SET);
        for i in range(0, num_entites):
            entnames.append(stream.readString(0x20, strip_null=True))

        # parse ent_data
        self.data.entites = []
        stream.seek(entities_off, io.SEEK_SET);
        for i in range(0, num_entites):
            self.data.entites.append(ODMEntity(stream, entnames[i], self.header.version))

        return # next part can fill whole ram+swap => crash

        ## Parse IDList
        ###############################
        logger.info("Parsing ODM file - IDList")
        num_idlist = stream.readUInt32(); logger.debug("Number of ids: %i" % (num_idlist,))
        if num_idlist>0:
            self.data.idlist = stream.readUInt16(num_idlist)
        else:
            self.data.idlist = []

        ## Parse OMAP
        ###############################
        logger.info("Parsing ODM file - OMAP")
        self.data.OMAP = np.reshape( np.matrix(stream.readUInt32(128*128)), (128,128) )

        ## Parse SPAWN
        ###############################
        logger.info("Parsing ODM file - SPAWN")
        num_spawn = stream.readUInt32(); logger.debug("Number of spawns: %i" % (num_spawn,))
        spawndata = stream.readByteArray(num_spawn*self.header.spawnsize)

        # TODO finish parsing spawn

        ## Check if everything was read
        ###############################
        if stream.tell() != stream.getLength():
            logger.warning("Whole ODM file was not read!! datasize=%i, current_offset=%i" % (stream.getLength(),stream.tell()) )
