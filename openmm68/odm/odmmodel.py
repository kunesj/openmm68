#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import io
from openmm68.misctools.attrdict import AttrDict
import openmm68.misc.mm_tools as mt

def parseInt32Vector(stream):
    return tuple([stream.readInt32() for i in range(3)])

def parseInt32Plane(stream):
    r = AttrDict()
    r.normal = parseInt32Vector(stream)
    r.dist = stream.readInt32()
    return r

def parseInt16Box(stream):
    r = AttrDict()
    minx = stream.readInt16()
    maxx = stream.readInt16()

    miny = stream.readInt16()
    maxy = stream.readInt16()

    minz = stream.readInt16()
    maxz = stream.readInt16()

    r.min = (minx, miny, minz)
    r.max = (maxx, maxy, maxz)
    return r

def parseInt32Box(stream):
    r = AttrDict()
    r.min = parseInt32Vector(stream)
    r.max = parseInt32Vector(stream)
    return r

class ODMFace(object):
    def __init__(self, stream, face_offset):
        ## init variables
        self.maxnumv_bmface = 0x14

        self.plane = None           # mm_int_plane_s 0x00 real_coord = coord/65536.0, [mm_int_vec3_s normal, int dist], int [[x,y,z], dist]
        self.z_calc = None          # short[6] 0x10 z_calc FACET_Z_CALC1 FACET_Z_CALC2 FACET_Z_CALC3
        self.attribs = None            # DWORD 0x1c FACET_ATTRIBUTES

        # vdata
        self.v_idx = None           # unsigned short[maxnumv_bmface] 0x20
        self.tex_x = None           # short[maxnumv_bmface] 0x48
        self.tex_y = None           # short[maxnumv_bmface] 0x70
        self.normal_x = None        # short[maxnumv_bmface] 0x98 X_INTERCEPT_DISPLACEMENTS
        self.normal_y = None        # short[maxnumv_bmface] 0xc0 Y_INTERCEPT_DISPLACEMENTS
        self.normal_z = None        # short[maxnumv_bmface] 0xe8 Z_INTERCEPT_DISPLACEMENTS

        self.unk02 = None           # short 0x110 FACET_BITMAP_INDEX
        self.tex_dx = None          # short 0x112
        self.tex_dy = None          # short 0x114
        self.bbox = None            # mm_short_bbox_s 0x116, short [[minx,maxx],[miny,maxy],[minz,maxz]]

        self.COG_NUMBER = None              # unsigned short 0x122 faceid for using in events (change texture..)
        self.COG_TRIGGERED_NUMBER = None    # unsigned short 0x124 event# on use
        self.COG_TRIGGER = None             # unsigned short 0x126 //event on touch?
        self.RESERVED = None                # unsigned short 0x128

        self.GRADIENT_VERTEXES = None       # BYTE[4] 0x12A
        self.numv = None                    # BYTE 0x12e, number of used vertices. (from 0x14 read)
        self.POLYGON_TYPE = None            # BYTE 0x12f
        self.SHADE = None                   # BYTE 0x130
        self.VISIBILITY = None              # BYTE 0x131
        self.PADDING = None                 # BYTE[2] 0x132 (unused?)

        ## parse face
        self.parseODMFace(stream, face_offset)

    def parseODMFace(self, stream, face_offset):
        stream.seek(face_offset, io.SEEK_SET)

        ## Parse face
        self.plane = parseInt32Plane(stream)
        self.z_calc = stream.readInt16(num=6)
        self.attribs = mt.parseBitAttributes(stream, "FacetBits", byte_num=4)

        self.v_idx = stream.readUInt16(num=self.maxnumv_bmface)
        self.tex_x = stream.readInt16(num=self.maxnumv_bmface)
        self.tex_y = stream.readInt16(num=self.maxnumv_bmface)
        self.normal_x = stream.readInt16(num=self.maxnumv_bmface)
        self.normal_y = stream.readInt16(num=self.maxnumv_bmface)
        self.normal_z = stream.readInt16(num=self.maxnumv_bmface)

        self.unk02 = stream.readInt16()
        self.tex_dx = stream.readInt16()
        self.tex_dy = stream.readInt16()
        self.bbox = parseInt16Box(stream)

        self.COG_NUMBER = stream.readUInt16()
        self.COG_TRIGGERED_NUMBER = stream.readUInt16()
        self.COG_TRIGGER = stream.readUInt16()
        self.RESERVED = stream.readUInt16()

        self.GRADIENT_VERTEXES = stream.readByteArray(4)
        self.numv = stream.readUInt8()
        self.POLYGON_TYPE = stream.readUInt8()
        self.SHADE = stream.readUInt8()
        self.VISIBILITY = stream.readUInt8()
        self.PADDING = stream.readByteArray(2)

    def __str__(self):
        return str(self.__dict__)

class ODMModel(object):
    def __init__(self, stream, model_header_offset, model_data_offset):
        ## init variables
        self.header = AttrDict()
        self.header.name1 = None       # char 0x20
        self.header.name2 = None       # char 0x20
        self.header.attribs = None      # int 0x40 attibutes
        self.header.num_vertices = None  # int 0x44
        self.header.pVertexes = None   # int* 0x48 fills on load (unused(fill on load))
        self.header.num_faces = None   # int 0x4c
        self.header.unk02 = None       # int 0x50 // CONVEX_FACETS_COUNT WORD, 2 bytes padding (suspect unused)
        self.header.pFaces = None      # int 0x54 - ORDERING_OFFSET (unused(fill on load))
        self.header.pUnkArray = None   # int* 0x58 - ORDERING_OFFSET (unused(fill on load))
        # ordering offset
        self.header.num3 = None        # int 0x5c //BSPNODE_COUNT (suspect unused)
        self.header.unk03a = None      # int 0x60 BSPNODE_OFFSET
        self.header.unk03b = None      # int 0x64 DECORATIONS_COUNT (suspect unused)
        self.header.unk03 = None       # int[2] 0x68 CENTER_X CENTER_Y
        self.header.origin1 = None     # mm_int_vec3_s 0x70, int [x,y,z]
        self.header.bbox = None        # mm_int_bbox_s 0x7c, mm_int_vec3_s [min,max], int [[x,y,z],[x,y,z]]
        self.header.unk04 = None       # int[6] 0x94 BF BBOX
        self.header.origin2 = None     # mm_int_vec3_s 0xac Bounding center, int [x,y,z]
        self.header.unk05 = None       # int 0xb8 Bounding radius

        self.vertices = [] # std::vector<mm_int_vec3_s>
        self.faces = [] # std::vector<bmodel_face_odm_t>
        self.faces_tex_names = [] # std::vector<std::string>
        self.unk_array = [] # std::vector<short> //num_faces*2
        self.bspnodes = [] # std::vector<bm_bspnode_t> //bspnode array 8*num3;
        self.origin = tuple() # <mm_int_vec3_s>
        #self.hide = False # bool, NOT PARSED
        #self.bbox = None # mm_int_bbox_s 0x7c, mm_int_vec3_s [min,max], int [[x,y,z],[x,y,z]], IS IN HEADER

        ## parse model
        self.parseODMModel(stream, model_header_offset, model_data_offset)

    def parseODMModel(self, stream, model_header_offset, model_data_offset):
        """ Binary size of one ODM model header is 0xbc """
        ## Parse header
        stream.seek(model_header_offset, io.SEEK_SET)

        self.header.name1 = stream.readString(0x20, strip_null=True)
        self.header.name2 = stream.readString(0x20, strip_null=True)
        self.header.attribs = mt.parseBitAttributes(stream, "FacetBits", byte_num=4) # TODO - not sure if has same attributes as ODMFace
        self.header.num_vertices = stream.readInt32()
        self.header.pVertexes = stream.readInt32()
        self.header.num_faces = stream.readInt32()
        self.header.unk02 = stream.readInt32()
        self.header.pFaces = stream.readInt32()
        self.header.pUnkArray = stream.readInt32()

        self.header.num3 = stream.readInt32()
        self.header.unk03a = stream.readInt32()
        self.header.unk03b = stream.readInt32()
        self.header.unk03 = stream.readInt32(num=2)
        self.header.origin1 = parseInt32Vector(stream)
        self.header.bbox = parseInt32Box(stream)
        self.header.unk04 = stream.readInt32(num=6)
        self.header.origin2 = parseInt32Vector(stream)
        self.header.unk05 = stream.readInt32()

        ## Parse data
        stream.seek(model_data_offset, io.SEEK_SET)

        # vertices
        self.vertices = []
        for n in range(0, self.header.num_vertices):
            self.vertices.append(parseInt32Vector(stream))

        # faces
        self.faces = []
        for n in range(0, self.header.num_faces):
            self.faces.append(ODMFace(stream, stream.tell()))

        # unk_array
        self.unk_array = stream.readInt16(num=self.header.num_faces)

        # parse texture names
        self.faces_tex_names = []
        for k in range(0, self.header.num_faces):
            tex_name = stream.readString(10, strip_null=True)
            if tex_name == '':
                logger.warning("Empty texture name for face in model '%s', '%s'! replacing with 'pending' texture" % (self.header.name1, self.header.name2))
                tex_name = 'pending'
            self.faces_tex_names.append(tex_name)

        # bspnodes
        self.bspnodes = []
        for n in range(0, self.header.num3):
            self.bspnodes.append(stream.readUInt32(num=2))

        # compute origin
        origin_x = (self.header.bbox.max[0]+self.header.bbox.min[0])/2
        origin_y = (self.header.bbox.max[1]+self.header.bbox.min[1])/2
        origin_z = (self.header.bbox.max[2]+self.header.bbox.min[2])/2
        self.origin = (origin_x, origin_y, origin_z)

    def __str__(self):
        ss = []
        for k in self.__dict__:
            if k == "faces":
                ssf = []
                for face in self.__dict__["faces"]:
                    ssf.append("\n\t%s," % str(face))
                ss.append("%s: [%s]," % (k, "".join(ssf)))
            else:
                ss.append("%s: %s" % (k, str(self.__dict__[k])))
        return ",\n".join(ss)
