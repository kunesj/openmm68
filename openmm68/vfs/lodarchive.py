#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

import sys, os, io

from openmm68.vfs.archive import Archive
from openmm68.misctools.binarydatastream import BinaryDataStream
from openmm68.misctools.files import getFilenameExtension
from openmm68.lod.lodfile import LODFile

class LODArchive(Archive):
    """ Member methods are described in inherited Archive class """

    def open(self, archive_path):
        logger.debug("Openning LODArchive: %s" % (archive_path,))
        # get correct path
        if not (os.path.isfile(archive_path) and os.path.exists(archive_path)):
            raise NotADirectoryError(archive_path)
        self.archive_path = os.path.abspath(archive_path)

        # init LODFIle object
        self.lodfile = LODFile(archive_path)
        self.lodtype = self.lodfile.getLODType()
        if self.lodtype.lower() == "sprites08":
            self.lodtype = "sprites"

        # get resources list
        self.resources = {}
        for filename in self.lodfile.getFileList():
            if self.lodtype.lower() == "bitmaps":
                if filename.lower().startswith("pal"):
                    filetype = None # file is palette
                else:
                    filetype = "lod_bitmap"
            elif self.lodtype.lower() == "sprites":
                filetype = "lod_sprite"
            elif self.lodtype.lower() == "icons":
                filetype = getFilenameExtension(filename)
                if filetype is None:
                    filetype = "lod_icon"
            else:
                filetype = getFilenameExtension(filename)

            resource_path = self.lodtype+"/"+filename
            self.resources[resource_path] = {"type":filetype}

    def get(self, path):
        if not self.exists(path):
            raise FileNotFoundError(path)
        filename = path.split("/")[-1]
        return self.lodfile.getFile(filename)
