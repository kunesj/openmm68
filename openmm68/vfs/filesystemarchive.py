#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

import sys, os, io

from openmm68.vfs.archive import Archive
from openmm68.misctools.binarydatastream import BinaryDataStream
from openmm68.misctools.files import getFilenameExtension

class FilesystemArchive(Archive):
    """ Member methods are described in inherited Archive class """

    def open(self, archive_path):
        logger.debug("Openning FilesystemArchive: %s" % (archive_path,))
        # get correct path
        if not (os.path.isdir(archive_path) and os.path.exists(archive_path)):
            raise NotADirectoryError(archive_path)
        self.archive_path = os.path.abspath(archive_path)

        # get resources list
        self.resources = {}
        for dir_name, subdir_list, file_list in os.walk(self.archive_path):
            for fname in file_list:
                resource_path = os.path.relpath(os.path.join(dir_name, fname), self.archive_path)
                self.resources[resource_path] = {"type":getFilenameExtension(fname)}

    def get(self, path):
        logger.info("Reading file from path '%s'" % (path,))
        if not self.exists(path):
            raise FileNotFoundError(path)
        with io.open(os.path.join(self.archive_path, path), 'rb') as af:
            bds = BinaryDataStream(af.read())
        return bds
