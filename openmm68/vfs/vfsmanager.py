#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

import os
import pickle

from collections import OrderedDict
from openmm68.misctools.files import getPathExtension, getPathExtensionless

from openmm68.vfs.lodarchive import LODArchive
from openmm68.vfs.filesystemarchive import FilesystemArchive

class VFSManager(object):

    def __init__(self):
        self.archives = []
        self.index = OrderedDict()
        self.extensionless_paths = {}

    def addArchive(self, archive_instance):
        """
        Input:
            archive_instance - Instance of class that inherited openmm68.vfs.archive.Archive class.
        """
        self.archives.append(archive_instance)

    def registerArchives(self, paths):
        """
        Input: List of paths to LOD archives or filesystem data directories. Sorted based on priority
        """
        logger.info("Registering archive paths into VFS...")
        for p in paths:
            if not os.path.exists(p):
                logger.error("Path to archive '%s' doesn't exists! Skipping..." % p)
                continue
            try:
                if os.path.isdir(p):
                    archive = FilesystemArchive(p)
                else:
                    archive = LODArchive(p)
            except Exception:
                logger.exception("Failed openning archive '%s'! Skipping..." % p)
                continue
            self.addArchive(archive)

    def buildIndex(self):
        """
        Structure of Resource Index:
        {
            normalized_path: {"path": orignal_path,
                                "type": file_type,
                                "archive": archive_index},
            ...
        }
        """
        # Build file index
        logger.info("Building file index...")
        self.index = {}
        self.extensionless_paths = {}
        for archive_index, archive in enumerate(self.archives):
            resource_index = archive.getResourceIndex()
            for filepath_key in resource_index:
                normalized_path = self.normalizePath(filepath_key)
                file_type = resource_index[filepath_key]["type"]
                self.index[normalized_path] = {
                        "path": filepath_key,
                        "type": file_type,
                        "archive": archive_index}

                # Build list of extensionless paths
                common_path = getPathExtensionless(normalized_path)
                if common_path not in self.extensionless_paths:
                    self.extensionless_paths[common_path] = {}
                self.extensionless_paths[common_path][file_type] = normalized_path


        # sort index
        self.index = OrderedDict(sorted(self.index.items()))

    def exists(self, path):
        path = self.normalizePath(path)
        return (path in self.index)

    def getIndex(self):
        return self.index

    def normalizePath(self, path):
        """
        List of changes to path string:
            - lowercase
            - replaces \ with / => unix style path
        """
        normalized_path = path.lower().replace("\\", "/")
        return normalized_path

    def get(self, path):
        """
        Returns: openmm68.files.binarydatastream.BinaryDataStream
        """
        return self.getNormalized(self.normalizePath(path))

    def getNormalized(self, normalized_path):
        """
        Returns: openmm68.files.binarydatastream.BinaryDataStream
        """
        if not self.exists(normalized_path):
            raise FileNotFoundError(normalized_path)

        archive_index = self.index[normalized_path]["archive"]
        original_path = self.index[normalized_path]["path"]
        data_stream = self.archives[archive_index].get(original_path)

        return data_stream

    def getType(self, path):
        """ Returns type of file as string. None if unknown type. """
        return self.getNormalizedType(self.normalizePath(path))

    def getNormalizedType(self, normalized_path):
        """ Returns type of file as string. None if unknown type. """
        if not self.exists(normalized_path):
            raise FileNotFoundError(normalized_path)
        return self.index[normalized_path]["type"]

    def getExtensionInsensitivePathList(self, path):
        """
        Based on given filepath returns dictionary with normalized paths of files with same name but different extensions.
        Returns dictionary: {
            "type1": "normalized_path",
            "type2": "normalized_path",
            "type3": "normalized_path",
            ...
            }
        """
        path = self.normalizePath(path)
        common_path = getPathExtensionless(path)

        if common_path in self.extensionless_paths:
            return self.extensionless_paths[common_path]
        else:
            return {}

    ## Debug functions

    def dumpVFSStructure(self): # TODO dump DIRECTLY to file
        print("---|VFS-Dump.Archives|------------------------------------------")
        for i, a in enumerate(self.archives):
            print(i, a.getPath())
        print("---|VFS-Dump.Files|---------------------------------------------")
        for k, v in self.index.items():
            print(k, '\t', v)
        print("---|VFS-Dump.End|-----------------------------------------------")

    def analyzeMemoryUsage(self, silent=False):
        if not silent: print("Analyzing VFS archive memory usage...")
        sum_usage = 0

        for i, arch in enumerate(self.archives):
            arch_class = arch.__class__.__name__
            try:
                usage = len(pickle.dumps(arch)); sum_usage += usage
            except:
                if not silent: logger.exception("Archive %i -> class = %s, Couldn't get memory usage!" % (i, arch_class))
            else:
                if not silent: print("Archive %i -> class = %s, memory = %i bytes" % (i, arch_class, usage))

        # index memory usage
        usage_index = len(pickle.dumps(self.index)); sum_usage += usage_index
        usage_extensionless = len(pickle.dumps(self.extensionless_paths)); sum_usage += usage_extensionless
        if not silent: print("Index -> index_memory = %i bytes, extensionless_index_memory = %i bytes" % (usage_index, usage_extensionless))

        if not silent: print("VFSManager-> memory_sum = %i bytes" % sum_usage)

        return sum_usage

def main():
    import argparse

    # Parasing input prarmeters
    parser = argparse.ArgumentParser(
        description='openmm68.vfs.manager'
    )
    parser.add_argument(
        'paths', nargs='*',
        help='Paths to resource archives')
    parser.add_argument(
        '-l', '--list', action='store_true',
        help='List files in VFS')
    parser.add_argument(
        '-e', '--extract', default=None,
        help='Extract file with specifited path from VFS to current dir')
    parser.add_argument(
        '-d', '--debug', type=int, choices=[50, 40, 30, 20, 10, 1], default=None,
        help='Set global debug level [CRITICAL=50, ERROR=40, WARNING=30, INFO=20, DEBUG=10, SPAM=1]. Default level is WARNING.')
    args = parser.parse_args()

    # Logger configuration
    logging.basicConfig()
    logger = logging.getLogger()
    if args.debug is not None:
        logger.setLevel(args.debug)
        logger.info("Set global debug level to: %i" % args.debug)
    else:
        logger.setLevel(30)

    # create VFS, add archives and build index
    man = VFSManager()
    man.registerArchives(args.paths)
    man.buildIndex()

    if args.list:
        man.dumpVFSStructure()

    elif args.extract is not None:
        print("Extracting file: %s" % args.extract )
        stream = man.get(args.extract)
        with open(os.path.basename(args.extract), 'wb') as f:
            f.write(stream.read())
