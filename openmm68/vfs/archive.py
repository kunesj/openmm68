#!/usr/bin/python3
#coding: utf-8

import logging, traceback
logger = logging.getLogger(__name__)

import os

class Archive(object):
    """
    Base class that should be inherited by any classes that deal with reading files from both archives and filesystem.
    """

    def __init__(self, archive_path=None):
        self.archive_path = None
        self.resources = {}

        if archive_path is not None:
            self.open(archive_path)

    def open(self, archive_path):
        """
        Executing this method should enable usage of all member methods.
        Needs to set self.archive_path and self.resources
        """
        raise NotImplementedError

    def getPath(self):
        """ Should return archive path """
        return self.archive_path

    def exists(self, path):
        """
        Should return true if file with given path exists in archive.
        Expects that archive is not being changed after inicialization.
        """
        return path in self.resources

    def get(self, path):
        """ Should return openmm68.files.binarydatastream.BinaryDataStream object. """
        raise NotImplementedError

    def getResourceIndex(self):
        """
        Returns:
            {
            resource_path: {"type": resource_type},
            resource_path: {"type": resource_type},
            ...
            }
        """
        return self.resources
