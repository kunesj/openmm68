#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import os
from openmm68.misctools.files import getFilenameExtensionless

class Parser(object):
    """
    BINs: (in 'language')
    dchest.bin, ddeclist.bin, dift.bin, dmonlist.bin,
    dobjlist.bin, doverlay.bin, dpft.bin, dsft.bin,
    dsounds.bin, dtft.bin, dtile.bin, dtile2.bin, dtile3.bin,

    BINs: (in 'icons')
    dtile.bin

    BINs: (in 'chapter')
    clock.bin, header.bin, npcdata.bin, npcgroup.bin, overlay.bin, party.bin, schedule.bin

    TXTs: (in 'language')
    2devents.txt, autonote.txt, awards.txt, class.txt,
    declist.txt, global.txt, history.txt,
    hostile.txt, ift.txt, items.txt,
    launcher.txt, mapstats.txt, merchant.txt, monlist.txt,
    monsters.txt, npcdata.txt, npcgreet.txt, npcgroup.txt,
    npcnews.txt, npctext.txt, npctopic.txt, objlist.txt,
    pcnames.txt, placemon.txt, potion.txt, potnotes.txt,
    quest.txt, quests.txt, rnditems.txt, roster.txt,
    scroll.txt, sft.txt, skilldes.txt, sounds.txt,
    spcitems.txt, spells.txt, stats.txt, stditems.txt, trans.txt

    Plain TXTs (unformated text): (in 'language')
    credits.txt, intro.txt
    """

    @classmethod
    def parse(cls, stream, filepath):
        logger.debug("Parsing config file '%s'" % filepath)
        fname = getFilenameExtensionless(os.path.basename(filepath)).lower()
        if fname[0] == "d" and fname not in ["declist"]: # remove "d" prefix from some *.bin files
            fname = fname[1:]

        if fname == "2devents":
            return cls._2devents(stream)
        elif fname == "autonote":
            return cls.autonote(stream)
        elif fname == "awards":
            return cls.awards(stream)
        elif fname == "chest":
            return cls.chest(stream)
        elif fname == "class":
            return cls._class(stream)
        elif fname == "clock":
            return cls.clock(stream)
        elif fname == "credits":
            return cls.credits(stream)
        elif fname == "declist":
            return cls.declist(stream)
        elif fname == "global":
            return cls._global(stream)
        elif fname == "header":
            return cls.header(stream)
        elif fname == "history":
            return cls.history(stream)
        elif fname == "hostile":
            return cls.hostile(stream)
        elif fname == "ift":
            return cls.ift(stream)
        elif fname == "intro":
            return cls.intro(stream)
        elif fname == "items":
            return cls.items(stream)
        elif fname == "launcher":
            return cls.launcher(stream)
        elif fname == "mapstats":
            return cls.mapstats(stream)
        elif fname == "merchant":
            return cls.marchant(stream)
        elif fname == "monlist":
            return cls.monlist(stream)
        elif fname == "monsters":
            return cls.monsters(stream)
        elif fname == "npcdata":
            return cls.npcdata(stream)
        elif fname == "npcgreet":
            return cls.npcgreet(stream)
        elif fname == "npcgroup":
            return cls.npcgroup(stream)
        elif fname == "npcnews":
            return cls.npcnews(stream)
        elif fname == "npctext":
            return cls.npctext(stream)
        elif fname == "npctopic":
            return cls.npctopic(stream)
        elif fname == "objlist":
            return cls.objlist(stream)
        elif fname == "overlay":
            return cls.overlay(stream)
        elif fname == "party":
            return cls.party(stream)
        elif fname == "pcnames":
            return cls.pcnames(stream)
        elif fname == "pft":
            return cls.pft(stream)
        elif fname == "placemon":
            return cls.placemon(stream)
        elif fname == "potion":
            return cls.potion(stream)
        elif fname == "potnotes":
            return cls.potnotes(stream)
        elif fname == "quests": # MM8 has forgotten unfinished quest.txt in datafiles which should be ignored
            return cls.quests(stream)
        elif fname == "rnditems":
            return cls.rnditems(stream)
        elif fname == "roster":
            return cls.roster(stream)
        elif fname == "schedule":
            return cls.schedule(stream)
        elif fname == "scroll":
            return cls.scroll(stream)
        elif fname == "sft":
            return cls.sft(stream)
        elif fname == "skilldes":
            return cls.skilldes(stream)
        elif fname == "sounds":
            return cls.sounds(stream)
        elif fname == "spcitems":
            return cls.spcitems(stream)
        elif fname == "spells":
            return cls.spells(stream)
        elif fname == "stats":
            return cls.stats(stream)
        elif fname == "stditems":
            return cls.stditems(stream)
        elif fname == "tft":
            return cls.tft(stream)
        elif fname in ["tile", "tile2", "tile3"]:
            return cls.tile(stream)
        elif fname == "trans":
            return cls.trans(stream)

        else:
            raise Exception("Unsupported config file with filepath '%s'" % filepath)

    @classmethod
    def _2devents(cls, stream): # HAS UNDERSCORE BEFORE NAME!
        raise NotImplementedError

    @classmethod
    def autonote(cls, stream):
        raise NotImplementedError

    @classmethod
    def awards(cls, stream):
        raise NotImplementedError

    @classmethod
    def chest(cls, stream):
        raise NotImplementedError

    @classmethod
    def _class(cls, stream): # HAS UNDERSCORE BEFORE NAME!
        raise NotImplementedError

    @classmethod
    def clock(cls, stream):
        raise NotImplementedError

    @classmethod
    def credits(cls, stream):
        raise NotImplementedError

    @classmethod
    def declist(cls, stream):
        raise NotImplementedError

    @classmethod
    def _global(cls, stream): # HAS UNDERSCORE BEFORE NAME!
        raise NotImplementedError

    @classmethod
    def header(cls, stream):
        raise NotImplementedError

    @classmethod
    def history(cls, stream):
        raise NotImplementedError

    @classmethod
    def hostile(cls, stream):
        raise NotImplementedError

    @classmethod
    def ift(cls, stream):
        raise NotImplementedError

    @classmethod
    def intro(cls, stream):
        raise NotImplementedError

    @classmethod
    def items(cls, stream):
        raise NotImplementedError

    @classmethod
    def launcher(cls, stream):
        raise NotImplementedError

    @classmethod
    def mapstats(cls, stream):
        raise NotImplementedError

    @classmethod
    def merchant(cls, stream):
        raise NotImplementedError

    @classmethod
    def monlist(cls, stream):
        raise NotImplementedError

    @classmethod
    def monsters(cls, stream):
        raise NotImplementedError

    @classmethod
    def npcdata(cls, stream):
        raise NotImplementedError

    @classmethod
    def npcgreet(cls, stream):
        raise NotImplementedError

    @classmethod
    def npcgroup(cls, stream):
        raise NotImplementedError

    @classmethod
    def npcnews(cls, stream):
        raise NotImplementedError

    @classmethod
    def npctext(cls, stream):
        raise NotImplementedError

    @classmethod
    def npctopic(cls, stream):
        raise NotImplementedError

    @classmethod
    def objlist(cls, stream):
        raise NotImplementedError

    @classmethod
    def overlay(cls, stream):
        raise NotImplementedError

    @classmethod
    def party(cls, stream):
        raise NotImplementedError

    @classmethod
    def pcnames(cls, stream):
        raise NotImplementedError

    @classmethod
    def pft(cls, stream):
        raise NotImplementedError

    @classmethod
    def placemon(cls, stream):
        raise NotImplementedError

    @classmethod
    def potion(cls, stream):
        raise NotImplementedError

    @classmethod
    def potnotes(cls, stream):
        raise NotImplementedError

    @classmethod
    def quests(cls, stream):
        raise NotImplementedError

    @classmethod
    def rnditems(cls, stream):
        raise NotImplementedError

    @classmethod
    def roster(cls, stream):
        raise NotImplementedError

    @classmethod
    def schedule(cls, stream):
        raise NotImplementedError

    @classmethod
    def scroll(cls, stream):
        raise NotImplementedError

    @classmethod
    def sft(cls, stream):
        raise NotImplementedError

    @classmethod
    def skilldes(cls, stream):
        raise NotImplementedError

    @classmethod
    def sounds(cls, stream):
        raise NotImplementedError

    @classmethod
    def spcitems(cls, stream):
        raise NotImplementedError

    @classmethod
    def spells(cls, stream):
        raise NotImplementedError

    @classmethod
    def stats(cls, stream):
        raise NotImplementedError

    @classmethod
    def stditems(cls, stream):
        raise NotImplementedError

    @classmethod
    def tft(cls, stream):
        raise NotImplementedError

    @classmethod
    def tile(cls, stream):
        raise NotImplementedError

    @classmethod
    def trans(cls, stream):
        raise NotImplementedError
