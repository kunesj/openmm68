#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import io
from openmm68.config.parser import Parser

class BinParser(Parser):
    """
    Class for parsing *.bin configuration files

    chest.bin
    clock.bin
    declist.bin
    header.bin
    ift.bin
    monlist.bin
    npcdata.bin
    npcgroup.bin
    objlist.bin
    overlay.bin
    party.bin
    pft.bin
    schedule.bin
    sft.bin
    sounds.bin
    tft.bin
    tile.bin
    tile2.bin
    tile3.bin

    For more information read openmm68.config.Parser documentation
    """

    # chest
    # clock


    @classmethod
    def declist(cls, stream):
        """
        Parse ddeclist.bin
        Information also parsable from declist.txt
        """
        stream.seek(0, io.SEEK_SET)

        dec_number = stream.readInt32()

        declist = []
        for i in range(0, dec_number):
            dec = {}
            dec['number'] = i # index should start at 0

            dec['label'] = stream.readString(0x20, strip_null=True)
            dec['name'] = stream.readString(0x20, strip_null=True)

            stream.readByteArray(0x02)

            dec['height'] = stream.readUInt16()
            dec['radius'] = stream.readUInt16()
            dec['light_rad'] = stream.readUInt16()

            stream.readByteArray(0x02)

            # Flags
            # 2 - Invisible
            # 1024 - EmitSmoke
            flag_num = stream.readUInt16()
            if flag_num == 2:
                dec['flags'] = "Invisible"
            elif flag_num == 1024:
                dec['flags'] = "EmitSmoke"
            elif flag_num == 0:
                dec['flags'] = ""
            else:
                logger.warning("Unexpected flag number '%i', for declist index '%i'" % (flag_num, dec['number']))
                dec['flags'] = ""

            dec['sound_id'] = stream.readUInt16()

            stream.readByteArray(0x02)

            dec['red'] = stream.readByteAsInt()
            dec['green']= stream.readByteAsInt()
            dec['blue'] = stream.readByteAsInt()

            stream.readByteArray(0x01)

            declist.append(dec)

        return declist

    # header
    # ift
    # monlist
    # npcdata
    # npcgroup
    # objlist # parasable from txt
    # overlay
    # party
    # pft
    # schedule
    # sft # parasable from txt
    # sounds # parasable from txt
    # tft

    @classmethod
    def tile(cls, stream):
        """ Parse dtile.bin, dtile2.bin, dtile3.bin (and dtile.bin in 'icons') """
        stream.seek(0, io.SEEK_SET)

        num = stream.readInt32()
        tbl = []
        for i in range(0, num):
            tmp = {}
            tmp['name'] = stream.readString(20, strip_null=True)
            tmp['hz'] = []
            tmp['hz'].append( stream.readUInt16() )
            tmp['hz'].append( stream.readUInt16() )
            tmp['hz'].append( stream.readUInt16() )
            tbl.append(tmp)

        return tbl


#
# def dchest(stream):
#     """ Parse dchest.bin """
#     stream.seek(0, io.SEEK_SET)
#
#     number = stream.readInt32()
#
#     chestlist = []
#     for i in range(0, number):
#         new_chest = {}
#         new_chest['name'] = stream.readString(0x20, strip_null=True)
#         new_chest['unknown'] = stream.readInt16() # TODO - what is this for ?
#         new_chest['index'] = stream.readInt16() # not sure if correct
#         chestlist.append(new_chest)
#
#     return chestlist



# def dift(stream):
#     """
#     Parse dift.bin
#     Parsable from ift.txt
#     """
#     stream.seek(0, io.SEEK_END); stream_length = stream.tell()
#     stream.seek(0, io.SEEK_SET)
#
#     number = stream.readInt32()
#
#     ift_list = []
#     for i in range(0, number): # number is too big => will allways need to be breaked from loop
#         # test if at the end of stream
#         if stream.tell() == stream_length:
#             break
#
#         label = stream.readString(0x0C, strip_null=True) # label
#
#         data = []
#         while True:
#             new_data = {}
#             new_data['name'] = stream.readString(0x0C, strip_null=True)
#             new_data['ticks'] = stream.readUInt16() # ticks
#             number = stream.readInt16() # number of subrecords following this one
#             new_data['val3'] = stream.readUInt16() # new??? if not == 1 ???
#             new_data['val4'] = stream.readUInt16() # unknown
#             data.append(new_data)
#
#             # test if at the end of stream
#             if stream.tell() == stream_length:
#                 break
#
#             # test if new label or just padding
#             padding_test = stream.readInt32()
#             stream.seek(stream.tell()-0x04, io.SEEK_SET) # move back
#
#             if padding_test != 0:
#                 break
#             else:
#                 stream.readBytes(0x0C)  # read padding
#
#         new_ift = {}
#         new_ift['label'] = label
#         new_ift['data'] = data
#         ift_list.append(new_ift)
#
#     return ift_list
#
# def dmonlist(stream):
#     """
#     Parse dmonlist.bin
#     Parsable from monlist.txt
#     """
#     stream.seek(0, io.SEEK_SET)
#
#     number = stream.readInt32()
#
#     monster_list = []
#     for i in range(0, number):
#         monster = {}
#
#         monster['height'] = stream.readUInt16()
#         monster['max_rad'] = stream.readUInt16()
#         monster['speed'] = stream.readUInt16()
#         monster['avg_rad ']= stream.readUInt16()
#         monster['blue'] = stream.readByteAsInt()
#         monster['green'] = stream.readByteAsInt()
#         monster['red'] = stream.readByteAsInt()
#         monster['alpha'] = stream.readByteAsInt()
#         monster['attack_val'] = stream.readUInt16()
#         monster['die_val'] = stream.readUInt16()
#         monster['stun_val'] = stream.readUInt16()
#         monster['fidget_val'] = stream.readUInt16()
#         monster['sfx'] = 0 # not in data ????
#
#         monster['name'] = stream.readString(0x40, strip_null=True)
#         monster['stand'] = stream.readString(0xA, strip_null=True)
#         monster['walk'] = stream.readString(0xA, strip_null=True)
#         monster['attack'] = stream.readString(0xA, strip_null=True)
#         monster['attack2'] = stream.readString(0xA, strip_null=True)
#         monster['wince'] = stream.readString(0xA, strip_null=True)
#         monster['death'] = stream.readString(0xA, strip_null=True)
#         monster['dead'] = stream.readString(0xA, strip_null=True)
#         monster['fidget'] = stream.readString(0xA, strip_null=True)
#
#         stream.readBytes(0x14) # padding?
#         monster_list.append(monster)
#
#     return monster_list
#
# def dobjlist(stream):
#     """
#     Parse dobjlist.bin
#     Parsable from objlist.txt
#     """
#     pass # TODO
#
# def doverlay(stream):
#     """ Parse doverlay.bin """
#     stream.seek(0, io.SEEK_SET)
#
#     number = stream.readInt32()
#
#     overlay_list = []
#     for i in range(0, number):
#         bytestring = stream.readBytes(0x8) # TODO - what does the values mean
#
#         overlay_list.append( [ int(x) for x in bytestring ] )
#
#     return overlay_list
#
# def dpft(stream):
#     """ Parse dpft.bin """
#     stream.seek(0, io.SEEK_SET)
#
#     number = stream.readInt32()
#
#     pft_list = []
#     for i in range(0, number):
#         pft = {}
#
#         pft['index'] = stream.readUInt16() # sometimes is 0 and sometimes skips values
#         pft['val1'] = stream.readUInt16() # another index ???, 0 in index makes this have wierd values
#         pft['val2'] = stream.readUInt16() # 2,4,8,16
#         pft['val3'] = stream.readUInt16() # 0,2,8,12,16,18,24
#         pft['val4'] = stream.readUInt16() # 0,1,4,5
#
#         pft_list.append( pft ) # TODO - what does the values mean
#
#     return pft_list
#
# def dsft(stream):
#     """
#     Parse dsft.bin
#     Parsable from sft.txt
#     """
#     pass # TODO
#
# def dsounds(stream):
#     """
#     Parse dsounds.bin
#     Parsable from sounds.txt
#     """
#     pass # TODO
#
# def dtft(stream):
#     """ Parse dtft.bin """
#     stream.seek(0, io.SEEK_SET)
#
#     number = stream.readInt32()
#
#     tft_list = []
#     for i in range(0, number):
#         tft = {}
#
#         tft['name'] = stream.readString(0xE, strip_null=True)
#         tft['val1'] = stream.readUInt16()
#         tft['val2'] = stream.readUInt16()
#         tft['val3'] = stream.readUInt16()
#
#         tft_list.append(tft)
#
#     return tft_list
#

