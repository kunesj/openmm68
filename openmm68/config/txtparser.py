#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import os, io

from openmm68.config.parser import Parser

class TxtParser(Parser):
    """
    Class for parsing *.txt configuration files

    2devents.txt
    autonote.txt
    awards.txt
    class.txt
    credits.txt
    declist.txt
    global.txt
    history.txt
    hostile.txt
    ift.txt
    intro.txt
    items.txt
    launcher.txt
    mapstats.txt
    merchant.txt
    monlist.txt
    monsters.txt
    npcdata.txt
    npcgreet.txt
    npcgroup.txt
    npcnews.txt
    npctext.txt
    npctopic.txt
    objlist.txt
    pcnames.txt
    placemon.txt
    potion.txt
    potnotes.txt
    quest.txt
    quests.txt
    rnditems.txt
    roster.txt
    scroll.txt
    sft.txt
    skilldes.txt
    sounds.txt
    spcitems.txt
    spells.txt
    stats.txt
    stditems.txt
    trans.txt

    For more information read openmm68.config.Parser documentation
    """

    ## TXT Parsing tools
    ####################

    @classmethod
    def parseTxt(cls, stream, categories, skiplines=1, cut_after_empty = False):
        """
        skiplines - how many lines to skip at the start of file (used to not parse categories)
        cut_after_empty - cut data after first empty line
        """
        stream.setStringEncoding('cp1252')
        stream.seek(0, io.SEEK_SET)
        lines = stream.readText().split('\r')[skiplines:] # records are split with CR ('\r') line ending

        data = []
        for l in lines:
            # skip empty lines
            if l.strip() == '':
                # cut after first empty line
                if len(data)!=0 and cut_after_empty:
                    break
                else:
                    continue
            # skip commented lines
            if l.strip().startswith('/'):
                continue

            # split line by tab
            lp = l.split('\t')
            lp = [ x.strip() for x in lp ]

            # init new empty array
            new_data = []
            for i in range(0, len(categories)):
                new_data.append('')

            # fill new_data array with stuff
            for i in range(0, len(categories)):
                if len(lp) == i:
                    break # pokud jsou data kratsi nez list categorii
                else:
                    new_data[i] = lp[i]

            # pokud jsou data vetsi nez kategorie -> pridej k posledni
            if len(lp) > len(categories):
                new_data[-1] += "".join(lp[len(categories):])

            data.append(new_data)

        return data

    @classmethod
    def parseCategories(cls, stream, category_line=0):
        stream.seek(0, io.SEEK_SET)
        line_cat = stream.readText().split('\r')[category_line]

        # remove / at the start of category line
        while line_cat.startswith('/'):
            line_cat = line_cat[1:]

        categories = []
        for c in line_cat.split('\t'):
            c = c.strip().lower().replace(" ", "_")
            categories.append(c)

        return categories

    @classmethod
    def parseTypes(cls, parsed_lines, types_list):
        """
        example:
            types_list = [('num', 1), ('str', 3), ('num', 6)]
            can be shorter then categories list
        """
        new_data = []
        for dl in parsed_lines:
            i = 0
            for n in range(0, len(types_list)):
                if i+1 >= len(dl):
                    break

                for j in range(0, types_list[n][1]):
                    if i+1 >= len(dl):
                        break

                    try:
                        if types_list[n][0].lower() == 'str':
                            pass
                        elif types_list[n][0].lower() in ['num', 'int', 'float']:
                            if dl[i].strip() == '':
                                dl[i] = 0
                            else:
                                dl[i] = float( dl[i].strip().replace(',','.').replace('"', '').strip() )
                                if types_list[n][0].lower() == "int":
                                    dl[i] = int(dl[i])
                        else:
                            raise Exception("Unknown type "+str(types_list[n][0]))
                    except Exception as e:
                        logger.debug("Failed to parseType '%s'", str(types_list[n][0]))
                        logger.exception(e)
                    i += 1

            new_data.append(dl)

        return new_data

    @classmethod
    def linesToDict(cls, parsed_lines, categories):
        parsed = []
        for l in parsed_lines:
            pl = {}
            for i, c in enumerate(categories):
                pl[c] = l[i]
            parsed.append(pl)
        return parsed

    ## Parsing functions
    ####################

    # _2devents
    # autonote
    # awards
    # _class
    # credits

    @classmethod
    def declist(cls, stream):
        """ declist.txt - parsable from BIN """
        categories = cls.parseCategories(stream, category_line=0)
        parsed_lines = cls.parseTxt(stream, categories, skiplines=2)
        types_list = [('num', 1), ('str', 2), ('num', 7)]

        parsed_lines = cls.parseTypes(parsed_lines, types_list)
        parsed_dicts = cls.linesToDict(parsed_lines, categories)
        # index should start at 0, not 1 (as in text file)
        for pd in parsed_dicts:
            pd['number']-=1

        return parsed_dicts

    # _global
    # history
    # hostile
    # ift
    # intro
    # items
    # launcher
    # mapstats
    # merchant
    # monlist
    # monsters
    # npcdata
    # npcgreet
    # npcgroup
    # npcnews
    # npctext
    # npctopic
    # objlist
    # pcnames
    # placemon
    # potion
    # potnotes
    # quest
    # quests
    # rnditems
    # roster
    # scroll

    @classmethod
    def sft(cls, stream):
        """ sft.txt - parsable from BIN """
        stream.setStringEncoding('cp1252')

        # get categories and lines
        categories = cls.parseCategories(stream, category_line=0)
        for i, c in enumerate(categories):
            if c.lower() == "(new)":
                categories[i] = "new"
        parsed_lines = cls.parseTxt(stream, categories, skiplines=1)

        # parse types
        types_list = [('str', 4), ('float', 1), ('int', 9)]
        parsed_lines = cls.parseTypes(parsed_lines, types_list)

        # to dict
        parsed_dicts = cls.linesToDict(parsed_lines, categories)

        # sort data to groups
        data_groups = {}; group = None
        for pd in parsed_dicts:
            # skip lines if first group didnt start yet
            if (group is None) and pd['new'].lower()!="new":
                logger.warning("No sft data group yet started! skipping line...")
                continue

            # new group
            if pd['new'].lower()=="new":
                group = pd['label'].lower()
                data_groups[group] = []

            # add data line to group
            line_dict = dict(pd)
            del(line_dict['label'])
            del(line_dict['new'])
            data_groups[group].append(line_dict)

        return data_groups

    # skilldes
    # sounds
    # spcitems
    # spells
    # stats
    # stditems
    # trans





#####################################################################################################################################################

# def _2devents(filedata):
#     categories = parseCategories(filedata, category_line=1)
#     parsed = parseTxt(filedata, categories, skiplines=2)
#     types_list = [('num', 2), ('str', 1), ('num', 2), ('str', 3), ('num', 6), ('str', 1), ('num', 1), ('str', 2), ('num', 6)]
#     return parseTypes(parsed, types_list)
#
# def autonote(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def awards(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1), ('str', 1), ('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def _class(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     return parsed # only string types

# def _global(filedata):
#     categories = ['global', 'text']
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def history(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def hostile(filedata):
#     """ Its table of who is hostile against who """
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('str', 1), ('num', len(parsed['data'][0])-1 )]
#     return parseTypes(parsed, types_list)
#
# #TODO - ift.txt - parsable from BIN
#
# def items(filedata):
#     """
#     "1-199 equipable items, 200- potions, 300- scrolls, 400-books, 500- artifacts, 600 quest items 700- message scrolls "
#     """
#     categories = parseCategories(filedata, category_line=1)
#     parsed = parseTxt(filedata, categories, skiplines=2)
#     types_list = [('num', 1), ('str', 2), ('num', 1), ('str', 3), ('num', 1), ('str', 1), ('num', 1), ('str', 1), ('num', 1), ('str', 1), ('num', 3)]
#     return parseTypes(parsed, types_list)
#
# def launcher(filedata):
#     categories = ['id', 'text']
#     parsed = parseTxt(filedata, categories, skiplines=2)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def mapstats(filedata):
#     categories = parseCategories(filedata, category_line=2)
#     parsed = parseTxt(filedata, categories, skiplines=3)
#     types_list = [('num', 1), ('str', 2), ('num', 13), ('str', 2), ('num', 1), ('str', 3), ('num', 1), ('str', 3), ('num', 1), ('str', 1), ('num', 1), ('str', 1), ('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def merchant(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     return parsed # only string types
#
# def monlist(filedata):
#     """ monlist.txt - parsable from BIN """
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('str', 8), ('num', 13)]
#     return parseTypes(parsed, types_list)
#
# def monsters(filedata):
#     categories = parseCategories(filedata, category_line=1)
#     parsed = parseTxt(filedata, categories, skiplines=4)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list) # TODO - parse rest of value types
#
# def npcdata(filedata):
#     categories = parseCategories(filedata, category_line=1)
#     parsed = parseTxt(filedata, categories, skiplines=2)
#     types_list = [('num', 1),  ('str', 1), ('num', 14)]
#     return parseTypes(parsed, types_list)
#
# def npcgreet(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def npcgroup(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 2)]
#     return parseTypes(parsed, types_list)
#
# def npcnews(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def npctext(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def npctopic(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list) # TODO - parse rest of value types
#
# def objlist(filedata):
#     """ objlist.txt - parsable from BIN """
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('str', 2), ('num', 5)]
#     return parseTypes(parsed, types_list)
#
# def pcnames(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     return parsed # only string types
#
# def placemon(filedata):
#     categories = ['Index', 'Placed Monster Names']
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def potion(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list) # TODO - parse rest of value types
#
# def potnotes(filedata):
#     # TODO - there is bunch of unindexed data at the end of file (will have 0 index)
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list) # TODO - parse rest of value types
#
# def quest(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def quests(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def rnditems(filedata):
#     """
#     Bonus chance by level %     1   2   3   4   5   6
#     Standard    0   40  40  40  40  75
#     Special 0   0   10  15  20  25
#     Weapons Special %   0   0   10  20  30  50
#     """
#     categories = parseCategories(filedata, category_line=2)
#     parsed = parseTxt(filedata, categories, skiplines=4, cut_after_empty = True)
#     types_list = [('num', 1), ('str', 1), ('num', 6)]
#     return parseTypes(parsed, types_list)
#
# def roster(filedata):
#     """
#     Skills - Column 1 is B, E, M, G for Basic, Expert, Master, Grandmaster.  Column 2 is the skill level
#     Spells - The number in each column is the number of spells in that school known (max 11)
#     These are Max 4
#     Item ID + ( 1000 * enchantment level [1 - 6] )
#     """
#     categories = parseCategories(filedata, category_line=1)
#     parsed = parseTxt(filedata, categories, skiplines=2)
#
#     types_list = [('num', 1), ('str', 2), ('num', 19)]
#     for i in range(0, 39): # 39 skills * 2 values
#         types_list.append( ('str', 1) )
#         types_list.append( ('num', 1) )
#     types_list.append( ('num', 12+11) )
#
#     return parseTypes(parsed, types_list)
#
# def scroll(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('num', 1)]
#     return parseTypes(parsed, types_list)

# def skilldes(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     return parsed # only string types
#
# def sounds(filedata):
#     """
#     sounds.txt - parsable from BIN
#
#     * First column is the filename of the .WAV file to be played backed during the game.
#     * Second column is the ID number of the sound.
#     * Third column is the type of sound.
#         (System - sounds gets loaded into memory at game startup, Swap - sounds gets loaded into memory when used then flushed when done, 0 - sound must be loaded by direct calls, locked - ?.
#     * Fourth column denotes whether the sound should be handled as 3D in Hardware.
#     * Fifth column is for general notes.  This column is NOT loaded by the game.
#     """
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('str', 1), ('num', 1)]
#     return parseTypes(parsed, types_list)
#
# def spcitems(filedata):
#     """
#     495 495 505 345 315 310 245 305 200 185 375 370
#
#     Treasure Level  If treasure Level = 3 add all A and B
#     A= 3 to 4   If treasure Level = 4 add all A and B and C
#     B= 3 to 5   If treasure Level = 5 add al B and C and D
#     C= 4 to 5   If treasure Level = 6 add all D
#     D= 5 to 6
#     """
#     categories = parseCategories(filedata, category_line=2)
#     parsed = parseTxt(filedata, categories, skiplines=4, cut_after_empty = True)
#     types_list = [('str', 2), ('num', 12)]
#     return parseTypes(parsed, types_list)
#
# def spells(stream):
#     stream.setStringEncoding('cp1252')
#     categories = parseCategories(stream, category_line=1)
#     stream.seek(0, io.SEEK_SET)
#     lines = stream.readText().split('\r')
#
#     # get spell separators by magic skill
#     spell_types = [1]
#     spell_types_names = [categories[2]]
#     for i in range(2, len(lines)):
#         split_l = lines[i].split('\t')
#         if len(split_l) < 3:
#             continue
#         elif split_l[0].strip() == '':
#             spell_types.append(i)
#             spell_types_names.append(split_l[2].strip())
#
#     parsed = []
#     for i in range(0, len(spell_types)):
#         # get modifited categories
#         spell_categories = list(categories)
#         spell_categories[2] = spell_types_names[i]
#
#         # get lines for spell type
#         if i+1 == len(spell_types):
#             end_cut = len(spell_types)
#         else:
#             end_cut = spell_types[i+1]
#         spell_lines = lines[spell_types[i]+1:end_cut]
#
#         data = parseTxt_lines(spell_lines, spell_categories)
#         spell_parsed = {'data':data, 'categories':list(spell_categories)}
#         spell_parsed = parseTypes(spell_parsed, [('num', 2)])
#
#         parsed.append(spell_parsed)
#
#     return parsed
#
# def stats(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     return parsed # only string types
#
# def stditems(filedata):
#     """
#     Bonus range for Standard by Level,     Value Mod= (+100 / Plus of Item)
#     lvl min max
#     1   0   0
#     2   1   5
#     3   3   8
#     4   6   12
#     5   10  17
#     6   15  25
#     (note weapons can only have Special Bonuses)
#     """
#     categories = parseCategories(filedata, category_line=2)
#     parsed = parseTxt(filedata, categories, skiplines=4, cut_after_empty = True)
#     types_list = [('str', 2), ('num', 9)]
#     return parseTypes(parsed, types_list)
#
# def trans(filedata):
#     categories = parseCategories(filedata, category_line=0)
#     parsed = parseTxt(filedata, categories, skiplines=1)
#     types_list = [('str', 1)]
#     return parseTypes(parsed, types_list)
#
