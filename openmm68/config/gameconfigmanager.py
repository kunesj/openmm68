#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import pkg_resources
import configparser

class GameConfigManager(object):
    """
    Game specific configurations. For Engine settings go to openmm68.settings.
    All keys in sections are accessible in a case-insensitive manner

    Use:
        from openmm68.config.gameconfigmanager import GAMECONFIG
    """

    def __init__(self):
        logger.info("GameConfigManager...")

        self.config = None
        self.clearConfig()

    @classmethod
    def getObject(cls):
        return cls()

    def clearConfig(self):
        self.config = configparser.ConfigParser(interpolation=None, inline_comment_prefixes=('#',))
        # make case sensitive
        self.config.optionxform = lambda option: option

        cfg_path = pkg_resources.resource_filename("openmm68.config", "empty.cfg")
        try:
            self.config.read(cfg_path, encoding="utf-8")
        except Exception:
            logger.exception("Couldn't load default empty config file '%s'!" % cfg_path)

    def loadConfig(self, path, update=True):
        logger.info("Loading game config file '%s'..." % path)

        if not update:
            self.clearConfig()

        try:
            self.config.read(path, encoding="utf-8")
        except Exception:
            logger.exception("Couldn't load config file '%s'!" % path)

    def loadDefaultConfig(self, mm_version):
        """ Allways clears old config """
        if mm_version == 6:
            cfg_path = pkg_resources.resource_filename("openmm68.config", "mm6_default.cfg") # TODO create mm6_default.cfg

        elif mm_version == 7:
            cfg_path = pkg_resources.resource_filename("openmm68.config", "mm7_default.cfg") # TODO create mm7_default.cfg

        elif mm_version == 8:
            cfg_path = pkg_resources.resource_filename("openmm68.config", "mm8_default.cfg")

        else:
            logger.error("Unsupported MM version %i. No config loaded!" % mm_version)
            return

        try:
            self.clearConfig()
            self.config.read(cfg_path, encoding="utf-8")
        except Exception:
            logger.exception("Couldn't load default config file '%s' for MM version %i" % (cfg_path, mm_version))

    def get(self, section, option):
        val = self.config.get(section, option)
        if type(val) == type([]): # remove list container
            return val[0]
        else:
            return val

    def getBoolean(self, section, option):
        return self.config.getboolean(section, option)

    def getInt(self, section, option):
        str_num = self.get(section, option).strip()
        if str_num.startswith("0x"):
            return int(str_num, 16)
        else:
            return self.config.getint(section, option)

    def getFloat(self, section, option):
        return self.config.getfloat(section, option)

    def sections(self):
        return self.config.sections()

    def hasSection(self, section):
        return  self.config.has_section(section)

    def options(self, section):
        return self.config.options(section)

    def hasOption(self, section, option):
        return self.config.has_option(section, option)

    def getOptionByIntValue(self, section, value):
        """ Used mainly by openmm68.scriptengine.evt_decompiler """
        for i, option in enumerate(self.options(section)):
            if self.getInt(section, option) == value:
                return option

        logger.warning("Config option in section '%s' with value %i is not defined" % (section, value))
        return "0x%02x" % value

    def hasOptionByIntValue(self, section, value):
        """ Used mainly by openmm68.scriptengine.evt_variables """
        for i, option in enumerate(self.options(section)):
            if self.getInt(section, option) == value:
                return True
        return False

""" Import this object variable in every file that needs to use GameConfigManager """
GAMECONFIG = GameConfigManager.getObject()
