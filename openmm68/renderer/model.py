#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from panda3d.core import *
from direct.task import Task

import time

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM

import openmm68.misc.mm_tools as mt
from openmm68.misctools.attrdict import AttrDict

class Face(object):
    def __init__(self, vertex_ids=[], normals=[], texcoords=[], texture_path=None):
        """
        Input:
            vertex_ids - list of indexes of vertices in Model data
            normals - list of tuples (x,y,z)
            texcoords - list of tuples (u,v)
            texture_path - relative path to texture in game data files
        """
        self.vertex_ids = vertex_ids
        self.normals = normals
        self.texcoords = texcoords
        self.texture_path = texture_path

    def __str__(self):
        return str(self.__dict__)

class Model(object):

    # scroll animation speed
    # 0.1 => 10% of texture moved in one second
    TEXTURE_SCROLL_SPEED = 1.0

    def __init__(self):
        """
        self.attribs - (odm) face attributes
        """
        self.name = None
        self.bbox = AttrDict()
        self.bbox.min = (0,0,0)
        self.bbox.max = (0,0,0)
        self.origin = (0,0,0)

        self.vertices = []
        self.faces = []

        # model attributes
        self.attribs = AttrDict({ # TODO - more default self.attribs values
            "Invisible":False,
            "ScrollDown":False,
            "ScrollUp":False,
            "ScrollLeft":False,
            "ScrollRight":False
            })

        # needed by animations - time of last animate() call
        self.this_animation_time = time.time()
        self.animation_task_ptr = None

        # this is used for ODMFaces, since they have separate attributes from rest of model
        self.submodels = []

        self.node_path = None

    @classmethod
    def fromODMModel(cls, odmmodel):
        """ Requires textures with identical size compared to original MM textures """

        main_model = cls()

        # set name
        main_model.name = "%s;%s" % (odmmodel.header.name1, odmmodel.header.name2)

        # set bounding box
        main_model.bbox = odmmodel.header.bbox
        main_model.bbox.min = tuple([ mt.mm_2_panda3d(x) for x in main_model.bbox.min ])
        main_model.bbox.max = tuple([ mt.mm_2_panda3d(x) for x in main_model.bbox.max ])

        # set origin
        main_model.origin = tuple([ mt.mm_2_panda3d(x) for x in odmmodel.origin ])

        # process vertices
        vertices = []
        for vertex in odmmodel.vertices:
            vertex = tuple([ mt.mm_2_panda3d(v) for v in vertex ])
            vertices.append((vertex[0]-main_model.origin[0], vertex[1]-main_model.origin[1], vertex[2]-main_model.origin[2]))

        # set attributes
        main_model.attribs.update(odmmodel.header.attribs)

        # process ODMFacets
        main_model.submodels = []
        for i, odmface in enumerate(odmmodel.faces):
            face_model = cls()

            # set name
            face_model.name = "%s;face%i" % (main_model.name, i)

            # set bounding box
            face_model.bbox = odmface.bbox
            face_model.bbox.min = tuple([ mt.mm_2_panda3d(x) for x in face_model.bbox.min ])
            face_model.bbox.max = tuple([ mt.mm_2_panda3d(x) for x in face_model.bbox.max ])

            # set origin
            face_model.origin = main_model.origin

            # set vertices
            face_model.vertices = vertices

            # set attributes
            face_model.attribs.update(odmface.attribs)

            ## Make Face
            face_model.faces = []

            texture_path = "bitmaps/%s" % odmmodel.faces_tex_names[i]
            vertex_ids = odmface.v_idx[:odmface.numv]
            normals = [ odmface.plane.normal for i in range(odmface.numv) ]

            # Compute UV
            texture_xsize, texture_ysize = RESOURCE_SYSTEM.getTextureManager().getTextureSize(texture_path)
            texcoords = []
            for n in range(odmface.numv):
                u = (odmface.tex_dx + odmface.tex_x[n])/texture_xsize
                v = - (odmface.tex_dy + odmface.tex_y[n])/texture_ysize
                # ODM Models dont have uv <0;1>, but <-inf,inf> => they require repeat texture mode!
                texcoords.append((u,v))

            # Create face
            face_model.faces.append(Face(vertex_ids, normals, texcoords, texture_path))

            # add face model to submodels
            main_model.submodels.append(face_model)

        return main_model

    # Panda3D
    def getNodePath(self, force_rebuild=False):
        # return nodepath if already generated once
        if self.node_path is not None and not force_rebuild:
            return self.node_path

        node = GeomNode('gnode')

        # init geoms. one for every face
        geoms = []
        for gi in range(len(self.faces)):
            gvd = GeomVertexData('gvd%i' % gi, GeomVertexFormat.getV3n3t2(), Geom.UHStatic)
            geom = Geom(gvd)
            prim = GeomTrifans(Geom.UHStatic) #GeomTristrips(Geom.UHStatic) # GeomTriangles(Geom.UHStatic)
            vertex = GeomVertexWriter(gvd, 'vertex')
            normal = GeomVertexWriter(gvd, 'normal')
            texcoord = GeomVertexWriter(gvd, 'texcoord')
            geoms.append({ 'geom':geom, 'prim':prim, 'gvd':gvd,
                'vertex':vertex, 'normal':normal,'texcoord':texcoord })

        # fill geoms with face data and add to node
        for gi, face in enumerate(self.faces):
            # write vertices
            for vid in face.vertex_ids:
                abs_vertex = (self.origin[0]+self.vertices[vid][0], self.origin[1]+self.vertices[vid][1], self.origin[2]+self.vertices[vid][2])
                geoms[gi]['vertex'].addData3f(*abs_vertex)

            # write normals
            for normal in face.normals:
                geoms[gi]['normal'].addData3f(*normal)

            # write texcoords
            for uv in face.texcoords:
                geoms[gi]['texcoord'].addData2f(*uv)

            # write primitive
            [ geoms[gi]['prim'].addVertex(i) for i in range(len(face.vertex_ids)) ]
            geoms[gi]['prim'].closePrimitive()
            geoms[gi]['geom'].addPrimitive(geoms[gi]['prim'])

            # add geom to node
            node.addGeom(geoms[gi]['geom'])

            # set geom texture
            tex = RESOURCE_SYSTEM.getTextureManager().getTexture(face.texture_path)
            node.setGeomState(gi, node.getGeomState(gi).addAttrib(TextureAttrib.make(tex)))

        # create nodepath
        self.node_path = render.attachNewNode(node)

        # set name
        if self.name is not None:
            self.node_path.setName(self.name)

        # flatten created model - fixes performance issues
        self.node_path.flattenStrong()

        # make and reparent submodels
        for submodel in self.submodels:
            sub_nodepath = submodel.getNodePath()
            sub_nodepath.reparentTo(self.node_path)

        # update attributes
        self.applyAttributes()

        # TODO - replace self.bbox with values from self.node_path.getTightBounds() # (LPoint3f(-37.0938, -34.7188, 0), LPoint3f(-36.9062, -34.5938, 0.15625))

        return self.node_path

    def loadTextures(self):
        texman = RESOURCE_SYSTEM.getTextureManager()
        [ texman.getTexture(face.texture_path) for face in self.faces ]
        # odm model requires Texture.WM_repeat => cant use clamp fix https://www.panda3d.org/manual/index.php/Texture_Wrap_Modes

        # call this function on submodels
        [ submodel.loadTextures() for submodel in self.submodels ]

    def applyAttributes(self): # TODO - support more attributes
        """ for attributes that needs to be applied only once """
        node_path = self.getNodePath()

        # Invisibility
        if self.attribs.Invisible:
            node_path.hide()
        else:
            node_path.show()

        ## Animation attributes - stuff that requires regual updates
        animated = False

        # reset texture scroll position
        if self.attribs.ScrollDown or self.attribs.ScrollUp or self.attribs.ScrollLeft or self.attribs.ScrollRight:
            animated = True
        else:
            node_path.setTexOffset(TextureStage.getDefault(), 0, 0) # TODO - may not be expected by some scripts???

        # start/end animation task
        if animated:
            self.createAnimationTask()
        else:
            self.removeAnimationTask()

        # call this function on submodels
        [ submodel.applyAttributes() for submodel in self.submodels ]

    def animationTask(self, task): # TODO - SHADERS, SHADERS, SHADERS
        """ For attributes that result in object animation """
        node_path = self.getNodePath()

        # get time diff and set new animation time
        time_diff = time.time()-self.this_animation_time
        self.this_animation_time = time.time()

        # Scroll texture
        if self.attribs.ScrollDown or self.attribs.ScrollUp or self.attribs.ScrollLeft or self.attribs.ScrollRight:
            u = 0; v = 0;
            if self.attribs.ScrollDown: v += 1
            if self.attribs.ScrollUp: v += -1
            if self.attribs.ScrollLeft: u += 1
            if self.attribs.ScrollRight: u += -1

            if not (u == 0 and v == 0):
                # get new texture offset
                scroll_distance = time_diff*self.TEXTURE_SCROLL_SPEED
                old_offset = node_path.getTexOffset(TextureStage.getDefault())
                new_u_off = old_offset[0] + u*scroll_distance
                new_v_off = old_offset[1] + v*scroll_distance
                if new_u_off >= 1.0: new_u_off += -1.0
                elif new_u_off <= -1.0: new_u_off += 1.0
                if new_v_off >= 1.0: new_v_off += -1.0
                elif new_v_off <= -1.0: new_v_off += 1.0

                # set texture offset
                node_path.setTexOffset(TextureStage.getDefault(), new_u_off, new_v_off)

        return task.cont

    def createAnimationTask(self):
        self.removeAnimationTask() # remove old animation task (if exists)
        self.animation_task_ptr = taskMgr.add(self.animationTask, "Model(id=%i)_AnimationTask" % id(self))

    def removeAnimationTask(self):
        if self.animation_task_ptr is not None:
            taskMgr.remove(self.animation_task_ptr)
            self.animation_task_ptr = None

    def __str__(self):
        ss = []
        for k in self.__dict__:
            if k == "faces":
                ssf = []
                for face in self.__dict__["faces"]:
                    ssf.append("\n\t%s," % str(face))
                ss.append("%s: [%s]" % (k, "".join(ssf)))
            elif k == "submodels":
                ssm = []
                for model in self.__dict__["submodels"]:
                    ssm.append("\n\t%s," % str(model))
                ss.append("%s: [%s]" % (k, "".join(ssm)))
            else:
                ss.append("%s: %s" % (k, str(self.__dict__[k])))
        return ",\n".join(ss)
