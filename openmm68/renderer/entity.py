#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import time
from panda3d.core import *
from direct.task import Task

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM

import openmm68.misc.mm_tools as mt
from openmm68.misctools.attrdict import AttrDict

class Billboard(object):
    def __init__(self, origin, width, height, texture_path=None, billboard_type="axis"):
        self.origin = origin # in panda units
        self.width = width # in panda units
        self.height = height # in panda units
        self.texture_path = texture_path
        if billboard_type.strip().lower() not in ["axis", "world", "eye"]:
            raise Exception("Unexpected billboard type '%s'!" % billboard_type)
        self.billboard_type = billboard_type.strip().lower()

        self.node_path = None

    # Panda3D
    def getNodePath(self, force_rebuild=False):
        # return nodepath if already generated once
        if self.node_path is not None and not force_rebuild:
            return self.node_path

        # init new node
        node = GeomNode('gnode')

        # init vertex data
        gvd = GeomVertexData('gvd', GeomVertexFormat.getV3n3t2(), Geom.UHStatic)
        geom = Geom(gvd)
        prim = GeomTrifans(Geom.UHStatic)
        vertex = GeomVertexWriter(gvd, 'vertex')
        normal = GeomVertexWriter(gvd, 'normal')
        texcoord = GeomVertexWriter(gvd, 'texcoord')

        ## fill vertex data
        # 0,0,0
        vertex.addData3f(-self.width/2,0,0)
        normal.addData3f(0,0,1)
        texcoord.addData2f(0,0)
        # 1,0,0
        vertex.addData3f(self.width/2,0,0)
        normal.addData3f(0,0,1)
        texcoord.addData2f(1,0)
        # 1,0,1
        vertex.addData3f(self.width/2,0,self.height)
        normal.addData3f(0,0,1)
        texcoord.addData2f(1,1)
        # 0,0,1
        vertex.addData3f(-self.width/2,0,self.height)
        normal.addData3f(0,0,1)
        texcoord.addData2f(0,1)

        # write primitive
        prim.addVertices(0,1,2,3)
        prim.closePrimitive()
        geom.addPrimitive(prim)

        # add geom to node
        node.addGeom(geom)

        # create nodepath
        node_path = render.attachNewNode(node)

        # set billboard effect
        if self.billboard_type == "axis":
            node_path.setBillboardAxis()
        elif self.billboard_type == "world":
            node_path.setBillboardPointWorld()
        elif self.billboard_type == "eye":
            node_path.setBillboardPointEye()

        # enable transparency
        node_path.setTransparency(TransparencyAttrib.MAlpha)

        # set texture from both sides => fixes some shadow issues
        node_path.setTwoSided(True)

        # set origin
        node_path.setPos(*self.origin)

        # flatten created model - fixes some performance issues
        node_path.flattenStrong()

        # set texture - NEEDS TO BE RUN LAST (after flattening?)
        if self.texture_path is not None:
            tex = RESOURCE_SYSTEM.getTextureManager().getSpriteTexture(self.texture_path)
        else:
            tex = RESOURCE_SYSTEM.getTextureManager().getPlaceholderTexture()
        node_path.setTexture(tex)

        self.node_path = node_path
        return node_path


class Entity(object):

    # animation tick scale
    # 0.1 => 1 tick == 0.1 second
    TICK_SCALE = 0.1

    def __init__(self):
        # General info
        self.name = None
        self.origin = (0,0,0)
        self.visible = True

        ## Declist data
        self.dec_label = None # texture name?
        self.dec_name = None # generic name
        self.radius = 0
        self.height = 0

        self.light_rad = 0
        self.light_rgb = (0,0,0) # (red, green, blue)

        self.sound_id = None

        self.flags = "" # "Invisible", "EmitSmoke" # TODO - replaced by self.attribs??? not smoke ???
        self.comments = ""

        ## Textures and animation list, => 'parsed' sft.txt
        # [{"texture_path":?, "scale":1.0, "tics":?, ...}, ...]
        self.sft_data = []
        self.this_sft_id = None
        self.this_sft_time = None # time.time(), the moment when was this sft_id set
        self.animation_task_ptr = None

        # sprite attributes
        self.attribs = AttrDict({ # TODO - more default self.attribs values
            "Invisible":False
            })

        self.node_path = None

    @classmethod
    def fromODMEntity(cls, odment): # Needs parse sft.txt for animated textures
        """ Requires declist config """
        sprite = cls()

        # set name
        sprite.name = odment.name
        # set origin
        sprite.origin = tuple([ mt.mm_2_panda3d(x) for x in odment.origin ])

        # Get Declist info
        declist = RESOURCE_SYSTEM.getConfigFileManager().getConfig("language/declist")
        decinfo = None
        for i in range(len(declist)):
            if declist[i]['number']==odment.declist_id:
                decinfo = declist[i]
        if decinfo is None:
            logger.error("Missing Declist index for number '%i' with name '%s'!" % (odment.declist_id, odment.name))
            sprite.label = None
            sprite.radius = 0.5
            sprite.height = 0.5
            return sprite

        sprite.dec_label = decinfo['label'] # label for group in sft.txt
        sprite.dec_name = decinfo['name']
        sprite.radius = mt.mm_2_panda3d(decinfo['radius']) # TODO - colision box
        sprite.height = mt.mm_2_panda3d(decinfo['height']) # TODO - colision box
        sprite.light_rad = mt.mm_2_panda3d(decinfo['light_rad'])
        sprite.light_rgb = (decinfo['red']/256, decinfo['green']/256, decinfo['blue']/256)
        sprite.sound_id = decinfo['sound_id'] # TODO - use this sprite.sound_id
        sprite.flags = decinfo['flags'] # TODO - use this sprite.flags
        sprite.comments = decinfo['comments']

        # set attributes
        sprite.attribs.update(odment.AIattrMarkers) # TODO - not sure if these are really attribs

        # get textures and animation
        sftgroups = RESOURCE_SYSTEM.getConfigFileManager().getConfig("language/sft")
        if decinfo['label'].lower() not in sftgroups:
            logger.warning("Couldn't find group with label '%s' in sft config file!" % decinfo['label'].lower())
            sprite.sft_data = [{"texture_path":("sprites/%s" % decinfo['label']), "scale":1.0, "ticks":2, "facing":1}]
        else:
            for sft in sftgroups[decinfo['label'].lower()]:
                tex_dict = {"texture_path":("sprites/%s" % sft['filename']), "scale":sft['scale'], "ticks":sft['ticks'], "facing":sft['facing']}
                # "light", "x", "y", "alpha", "glow", "center", "flag4", "//"
                sprite.sft_data.append(tex_dict)

        return sprite

    # Panda3D
    def getNodePath(self, force_rebuild=False):
        # return nodepath if already generated once
        if self.node_path is not None and not force_rebuild:
            return self.node_path

        # compute billboard width and height
        tex_x, tex_y = RESOURCE_SYSTEM.getTextureManager().getTextureSize(self.sft_data[0]["texture_path"])
        height = mt.mm_2_panda3d(tex_y)
        width = (height/tex_y)*tex_x

        # create billboard
        bb = Billboard(self.origin, width, height)
        self.node_path = bb.getNodePath()
        self.setSftId(0) # set initial texture, also inits some values required by animationTask(...)

        # add light
        if self.light_rad > 0:
            plight = PointLight("plight")
            plight.setColor(Vec4(self.light_rgb[0], self.light_rgb[1], self.light_rgb[2], 1))
            # Light Falloff - setAttenuation(A, B, C) where the net attentuation is defined by A + Bd + Cd^2
            plight.setAttenuation(Point3(1, 0, 1/(self.light_rad*2)))  # TODO - find out correct equation
            plight_np = self.node_path.attachNewNode(plight)
            plight_np.setPos(0,0,height) # light starts at the top of the entity, required because normals are set for light shinning from top
            render.setLight(plight_np)

        # set name
        if self.name is not None:
            self.node_path.setName(self.name)

        # update attributes
        self.applyAttributes()

        return self.node_path

    def applyAttributes(self): # TODO - support more attributes
        node_path = self.getNodePath()

        if self.attribs.Invisible:
            node_path.hide()
        else:
            node_path.show()

        # Create animation task
        if not len(self.sft_data)<=1:
            self.createAnimationTask()

    def showLightFrustum(self):
        for plight_np in self.getNodePath().findAllMatches("plight"):
            plight_np.node().show_frustum()

    def loadTextures(self):
        texman = RESOURCE_SYSTEM.getTextureManager()
        for texture_dict in self.sft_data:
            texman.getSpriteTexture(texture_dict["texture_path"])

    def setSftId(self, sft_id): # TODO - change also sft values: palete. light, facing, x, y, alpha, glow, center, flag4
        """ Input: id of row in self.sft_data """
        node_path = self.getNodePath()

        # change texture
        texture = RESOURCE_SYSTEM.getTextureManager().getSpriteTexture(self.sft_data[sft_id]["texture_path"])
        node_path.setTexture(texture)

        # change scale
        node_path.setScale(self.sft_data[sft_id]["scale"])

        # update this id and time
        self.this_sft_id = sft_id
        self.this_sft_time = time.time()

    def animationTask(self, task):
        # change sft id (texture animation, etc.)
        if (time.time()-self.this_sft_time) > self.sft_data[self.this_sft_id]['ticks']*self.TICK_SCALE:
            if self.this_sft_id == len(self.sft_data)-1:
                next_id = 0
            else:
                next_id = self.this_sft_id+1
            self.setSftId(next_id)

        return task.again

    def createAnimationTask(self):
        self.removeAnimationTask() # remove old animation task (if exists)
        self.animation_task_ptr = taskMgr.doMethodLater(self.TICK_SCALE, self.animationTask, "Entity(id=%i)_AnimationTask" % id(self))

    def removeAnimationTask(self):
        if self.animation_task_ptr is not None:
            taskMgr.remove(self.animation_task_ptr)
            self.animation_task_ptr = None







