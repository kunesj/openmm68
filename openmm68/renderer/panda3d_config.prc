###########################################################
###                                                     ###
### Panda3D Configuration File -  User-Editable Portion ###
###                                                     ###
###########################################################

# Sets Panda3D debug level ["spam", "debug", "info", "warning", and "error"]
#notify-level info

# Enable texture compression -> can make wierd colors (compression artifacts)
compressed-textures #t
#model-cache-dir /tmp/panda-cache
model-cache-compressed-textures #t

# non-power of 2 textures fix => scale up
textures-power-2 up

# enable multithread
threading-model Cull/Draw
