#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import sys
import pkg_resources

from direct.showbase.ShowBase import ShowBase
#from direct.showbase import DirectObject
from panda3d.core import *
from direct.task import Task

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM
from openmm68.settings.settingsmanager import SETTINGS

from openmm68.renderer.skybox import SkyBox

# TODO - have this in separate class
game_time = 0.25*86400 # start at dawn
time_speed_multi = 1000 # 1 == real time speed, 100 = 100x real time speed

class MyApp(ShowBase):

    def __init__(self):
        ShowBase.__init__(self)

        # init panda3d basic settings
        self.loadPanda3DConfig()
        base.camLens.setFar(SETTINGS.getFloat("Camera", "LensFar"))
        base.camLens.setNear(SETTINGS.getFloat("Camera", "LensNear")) # set camera min render distance
        base.setBackgroundColor(Vec3(SETTINGS.getRGB("Fog", "FogColor")))

        self.enableFrameRateMeter(SETTINGS.getBoolean("General", "ShowFPS"))
        self.enableWireframeMode(SETTINGS.getBoolean("General", "ShowWireframe"))
        self.enableFilledWireframeMode(SETTINGS.getBoolean("General", "ShowFilledWireframe"))

        # init required variables
        self.terrain = None # openmm68.renderer.terrain.Terrain
        self.models = [] # openmm68.renderer.model.Model
        self.sprites = [] # openmm68.renderer.entity.Entity
        # create new node trees
        self.terrain_node = render.attachNewNode("terrain-node")
        self.models_node = render.attachNewNode("models-node")
        self.sprites_node = render.attachNewNode("sprites-node")

        ## Create player nodes
        self.player = NodePath(PandaNode("Player"))
        self.player.reparentTo(self.render)
        # TODO - init player position

        # Fog
        globalFog = Fog("GlobalFog")
        globalFog.setColor(Vec3(SETTINGS.getRGB("Fog", "FogColor")))
        globalFog.setExpDensity(SETTINGS.getFloat("Fog", "GlobalFogExpDensity"))
        render.setFog(globalFog)

        if SETTINGS.getBoolean("Fog", "LocalFogEnabled"):
            localFog = Fog("LocalFog")
            localFog.setColor(Vec3(SETTINGS.getRGB("Fog", "FogColor")))
            localFog.setLinearRange(SETTINGS.getFloat("Fog", "LocalFogOnsetDistance"),SETTINGS.getFloat("Fog", "LocalFogOpaqueDistance"))
            base.camLens.setFar(SETTINGS.getFloat("Fog", "LocalFogOpaqueDistance"))
            render.setFog(localFog)
            # TODO - add 'fog' after cut empty space

        ## Lightning
        # ambient
        alight = render.attachNewNode(AmbientLight("AmbientLight"))
        alight.node().setColor(Vec4(.3, .3, .3, 1))
        render.setLight(alight)

        # sunlight
        directionalLight = DirectionalLight("SunLight")
        directionalLight.setColor(Vec4( 0.8, 0.8, 0.8, 1.0 ))
        directionalLight.getLens().setFilmSize( 128, 128 ) # -> x,y size of light; reset at new terrain load
        directionalLight.getLens().setNearFar( -(128/2), (128/2)) # -> z position and max light distance; reset at new terrain load
        self.sun_light = render.attachNewNode(directionalLight)
        render.setLight(self.sun_light)

        # Having spotlight in scene magicaly fixes directional light shadows.
        sp = Spotlight("SP")
        sp.setColor(( 0.0, 0.0, 0.0, 1.0 ))
        sp.setShadowCaster(False)
        spn = render.attachNewNode(sp)
        render.setLight(spn)

        ## Init Skybox
        #self.skybox_node = NodePath(SkyBox(self, self.camera))

        # init sky position
        self.setDayNightCycle(0)

        ## Shadows - EATS FPS LIKE CRAZY! From stable 60 fps to 10-18 fps
        self.sun_light.node().setShadowCaster(SETTINGS.getBoolean("Shadows", "Enabled"), SETTINGS.getInt("Shadows", "Resolution"), SETTINGS.getInt("Shadows", "Resolution"))
        # Enable the shader generator for the receiving nodes
        self.terrain_node.setShaderAuto()
        self.models_node.setShaderAuto()
        self.sprites_node.setShaderAuto() # looks ugly

        ## Init keys
        self.keymap = {"left":0, "right":0, "forward":0, "backward":0, "jump":0, "crouch":0 }
        # triggering of keys
        self.accept("escape", sys.exit)
        self.accept("arrow_left", self.setKey, ["left", True])
        self.accept("arrow_right", self.setKey, ["right", True])
        self.accept("arrow_up", self.setKey, ["forward", True])
        self.accept("arrow_down", self.setKey, ["backward", True])
        self.accept("space", self.setKey, ["jump", True])
        self.accept("control", self.setKey, ["crouch", True])
        self.accept("arrow_left-up", self.setKey, ["left", False])
        self.accept("arrow_right-up", self.setKey, ["right", False])
        self.accept("arrow_up-up", self.setKey, ["forward", False])
        self.accept("arrow_down-up", self.setKey, ["backward", False])
        self.accept("space-up", self.setKey, ["jump", False])
        self.accept("control-up", self.setKey, ["crouch", False])
        # disable default camera controls
        self.disableMouse()
        self.camera.setPos(self.player.getX(), self.player.getY(), self.player.getZ())

        ## Tasks
        taskMgr.add(self.flyTask, "flyTask")
        taskMgr.doMethodLater(0.1, self.updateTask, 'updateTask')
        taskMgr.doMethodLater(0.5, self.dayNightCycleTask, 'dayNightCycleTask')

    def analyze(self):
        print("---------------------------------------------------------------")
        render.analyze()
        RESOURCE_SYSTEM.analyzeMemoryUsage()
        print("---------------------------------------------------------------")

    def loadPanda3DConfig(self, config_path=None):
        logger.info("Loading Panda3D configuration file...")
        if config_path is None:
            # get default config
            config_path=pkg_resources.resource_filename("openmm68.renderer", "panda3d_config.prc")
        loadPrcFile(config_path)

    def enableFrameRateMeter(self, enable=True):
        base.setFrameRateMeter(enable)

    def enableWireframeMode(self, enable=True):
        if enable:
            render.setRenderModeWireframe()
        # else:
        #     render.clearRenderMode()

    def enableFilledWireframeMode(self, enable=True):
        if enable:
            render.setRenderModeFilledWireframe(Vec4( 1.0, 0.0, 0.0, 1.0 ))
        # else:
        #     render.clearRenderMode()

    def showLightFrustum(self):
        """ Draw light dirrection and size """
        self.sun_light.node().show_frustum()
        [ s.showLightFrustum() for s in self.sprites ]

    def showModelBounds(self):
        """ Spheres with a lot of padding => not very useful """
        for mod in self.models:
            mod.getNodePath().showBounds()

    def showTightModelBounds(self):
        """ Cubes with zero padding """
        for mod in self.models:
            mod.getNodePath().showTightBounds()

    ## Scene

    def setTerrain(self, terrain):
        self.terrain = terrain
        # set correct size for sun light
        size_x, size_y = self.terrain.getSize()
        self.sun_light.node().getLens().setFilmSize( size_x, size_y )
        self.sun_light.node().getLens().setNearFar( -(size_x/2), (size_x/2))

    def setModels(self, models=[]):
        self.models = models

    def setSprites(self, sprites=[]):
        self.sprites = sprites

    def cleanScene(self):
        # remove old node trees
        [ c.removeNode() for c in self.terrain_node.getChildren() ]
        [ c.removeNode() for c in self.models_node.getChildren()  ]
        [ c.removeNode() for c in self.sprites_node.getChildren() ]

    def rebuildScene(self):
        """ Call this after you finished adding new models, terrain, etc... """
        # clear old stuff
        self.cleanScene()

        # LoadingTextures
        logger.info("Loading Textures")
        self.terrain.loadTextures()
        [ mod.loadTextures() for mod in self.models ]
        [ spr.loadTextures() for spr in self.sprites ]

        # Terrain
        logger.info("Rendering terrain")
        self.terrain.getNodePath().reparentTo(self.terrain_node)

        # Models
        logger.info("Rendering models")
        for mod in self.models:
            mod.getNodePath().reparentTo(self.models_node)

        # Sprites (entities)
        logger.info("Rendering sprites (entities)")
        for spr in self.sprites:
            spr.getNodePath().reparentTo(self.sprites_node)

        # TODO - temp
        self.analyze()
        #self.showLightFrustum()
        #self.showModelBounds()
        #self.showTightModelBounds()

    ## Functions

    def setKey(self, key, value):
        """ Records the state of the arrow keys """
        self.keymap[key] = value

    def setDayNightCycle(self, game_time):
        """
        Input:
            game_time - in seconds. 00:00:00 = 0, 24:00:00 = 86400
        """
        day_perc = game_time/86400.0
        sun_pos = 90 + day_perc*360 # sun range in 0-360 degrees
        self.sun_light.setHpr( -90.0, sun_pos, 0.0 ) # (-90,0, sun_pos, 0.0)

    ## Tasks

    def flyTask(self, task):
        dt = globalClock.getDt()

        view_speed = 75 # degrees per second
        move_speed = 5 # units per second

        # rotate view
        if self.keymap["left"]:
            self.player.setH(self.player.getH() + (view_speed * dt))
        if self.keymap["right"]:
            self.player.setH(self.player.getH() - (view_speed * dt))

        # move backward/forward
        if self.keymap["forward"]:
            self.player.setY(self.player, + (move_speed * dt))
        if self.keymap["backward"]:
            self.player.setY(self.player, - (move_speed * dt))

        # move up/down
        if self.keymap["jump"]:
            self.player.setZ(self.player, + (move_speed * dt))
        if self.keymap["crouch"]:
            self.player.setZ(self.player, - (move_speed * dt))

        # Move camera to same position as player
        self.camera.setPos(self.player.getPos())
        self.camera.setH(self.player.getH())

        return task.cont

    def dayNightCycleTask(self, task):
        """ update sun position """
        self.setDayNightCycle(game_time)
        return task.again

    def updateTask(self, task):
        """ Task that is autocalled 'every frame' """

        # update game_time # TODO - move to game logic?
        global game_time
        dt = globalClock.getDt()
        game_time += time_speed_multi*dt
        if game_time >= 86400: # 24:00:00
            game_time -= 86400

        return task.again


    #
    #     # Moving - Collisions
    #     self.cTrav = CollisionTraverser()
    #
    #     self.playerGroundRay = CollisionRay()
    #     self.playerGroundRay.setOrigin(0, 0, 90)
    #     self.playerGroundRay.setDirection(0, 0, -1)
    #     self.playerGroundCol = CollisionNode('playerRay')
    #     self.playerGroundCol.addSolid(self.playerGroundRay)
    #     self.playerGroundCol.setFromCollideMask(CollideMask.bit(0))
    #     self.playerGroundCol.setIntoCollideMask(CollideMask.allOff())
    #     self.playerGroundColNp = self.player.attachNewNode(self.playerGroundCol)
    #     self.playerGroundHandler = CollisionHandlerQueue()
    #     self.cTrav.addCollider(self.playerGroundColNp, self.playerGroundHandler)
    #
    #     # Uncomment this line to see the collision rays
    #     #self.playerGroundColNp.show()
    #     # Uncomment this line to show a visual representation of the collisions occuring
    #     #self.cTrav.showCollisions(render)
    #
    #
    # # Accepts arrow keys to move the player,
    # # Also deals with grid checking and collision detection
    # def move(self, task):
    #     # Get the time that elapsed since last frame.  We multiply this with
    #     # the desired speed in order to find out with which distance to move
    #     # in order to achieve that desired speed.
    #     dt = globalClock.getDt()
    #
    #     # If the left key is pressed, move camera left.
    #     # If the right key is pressed, move camera right.
    #     if self.keyMap["left"]:
    #         self.player.setH(self.player.getH() + 75 * dt)
    #     if self.keyMap["right"]:
    #         self.player.setH(self.player.getH() - 75 * dt)
    #
    #     # save player's initial position so that we can restore it,
    #     # in case he falls off the map or runs into something.
    #     startpos = self.player.getPos()
    #
    #     # If a move-key is pressed, move player in the specified direction.
    #     if self.keyMap["forward"]:
    #         self.player.setY(self.player, +10 * dt)
    #     if self.keyMap["backward"]:
    #         self.player.setY(self.player, -10 * dt)
    #
    #     # Adjust player's Z coordinate.  If player's ray hit anything
    #     # update his Z cordinates, else put him back where he was last frame.
    #     entries = list(self.playerGroundHandler.getEntries())
    #     entries.sort(key=lambda x: x.getSurfacePoint(render).getZ())
    #
    #     if len(entries) > 0:
    #         maxz = entries[0].getSurfacePoint(render).getZ()
    #         for e in entries:
    #             maxz = max(maxz, e.getSurfacePoint(render).getZ())
    #         self.player.setZ(maxz)
    #     else:
    #         self.player.setPos(startpos)
    #
    #     # Move camera to same position as player
    #     self.camera.setPos(self.player.getPos())
    #     self.camera.setH(self.player.getH())
    #     # Keep the camera above player
    #     self.camera.setZ(self.player.getZ() + 0.4)
    #
    #     return task.cont

def renderer_setup():
    app = MyApp()
    return app

if __name__ == "__main__":
    app = renderer_setup()
    app.run()

