#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

from panda3d.core import *

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM

class SkyBox(PandaNode):
    def __init__(self, game_app, camera):
        super().__init__("SkyBox")
        self.game_app = game_app
        self.camera = camera

        node_path = NodePath(self)
        node_path.reparentTo(self.camera) # move together with camera
        node_path.setCompass(self.game_app.render) # inherit rotation from root node
        node_path.setBin("background", 0) # draw first
        node_path.setDepthWrite(False) # turn off depth buffer for this node

        # add textures
        cube_node_path = self.getCube(size=2, tex_num=5)
        cube_node_path.reparentTo(node_path)
        cube_node_path.setTexture(RESOURCE_SYSTEM.getTextureManager().getTexture("bitmaps/cloudsabove")) # "bitmaps/skybox/mm/cloudsabove"

    def getSkyGeometry(self):
        pass


    def getCube(self, size=2, tex_num=1):
        # init new geome node
        node = GeomNode('gnode')

        # init vertex data
        gvd = GeomVertexData('gvd', GeomVertexFormat.getV3t2(), Geom.UHStatic)
        geom = Geom(gvd)
        prim = GeomTrifans(Geom.UHStatic)
        vertex = GeomVertexWriter(gvd, 'vertex')
        texcoord = GeomVertexWriter(gvd, 'texcoord')

        vertices = {
            "000":(-size/2, -size/2, -size/2),
            "100":(size/2,  -size/2, -size/2),
            "110":(size/2,  size/2,  -size/2),
            "010":(-size/2, size/2,  -size/2),
            "001":(-size/2, -size/2, size/2),
            "101":(size/2,  -size/2, size/2),
            "111":(size/2,  size/2,  size/2),
            "011":(-size/2, size/2,  size/2)
            }

        ## fill vertex data # TODO - fix
        # down square
        vertex.addData3f(*vertices["000"])
        texcoord.addData2f(0, 0)
        vertex.addData3f(*vertices["100"])
        texcoord.addData2f(tex_num, 0)
        vertex.addData3f(*vertices["110"])
        texcoord.addData2f(tex_num, tex_num)
        vertex.addData3f(*vertices["010"])
        texcoord.addData2f(0, tex_num)
        # up square
        vertex.addData3f(*vertices["001"])
        texcoord.addData2f(0, 0)
        vertex.addData3f(*vertices["101"])
        texcoord.addData2f(tex_num, 0)
        vertex.addData3f(*vertices["111"])
        texcoord.addData2f(tex_num, tex_num)
        vertex.addData3f(*vertices["011"])
        texcoord.addData2f(0, tex_num)
        # front square
        vertex.addData3f(*vertices["000"])
        texcoord.addData2f(0, 0)
        vertex.addData3f(*vertices["100"])
        texcoord.addData2f(tex_num, 0)
        vertex.addData3f(*vertices["101"])
        texcoord.addData2f(tex_num, tex_num)
        vertex.addData3f(*vertices["001"])
        texcoord.addData2f(0, tex_num)
        # back square
        vertex.addData3f(*vertices["010"])
        texcoord.addData2f(0, 0)
        vertex.addData3f(*vertices["110"])
        texcoord.addData2f(tex_num, 0)
        vertex.addData3f(*vertices["111"])
        texcoord.addData2f(tex_num, tex_num)
        vertex.addData3f(*vertices["011"])
        texcoord.addData2f(0, tex_num)
        # left square
        vertex.addData3f(*vertices["000"])
        texcoord.addData2f(0, 0)
        vertex.addData3f(*vertices["010"])
        texcoord.addData2f(tex_num, 0)
        vertex.addData3f(*vertices["011"])
        texcoord.addData2f(tex_num, tex_num)
        vertex.addData3f(*vertices["001"])
        texcoord.addData2f(0, tex_num)
        # right square
        vertex.addData3f(*vertices["100"])
        texcoord.addData2f(0, 0)
        vertex.addData3f(*vertices["110"])
        texcoord.addData2f(tex_num, 0)
        vertex.addData3f(*vertices["111"])
        texcoord.addData2f(tex_num, tex_num)
        vertex.addData3f(*vertices["101"])
        texcoord.addData2f(0, tex_num)

        # write primitive
        for i in range(0,6):
            prim.addVertices(*range(4*i, 4*(i+1)))
            prim.closePrimitive()
        geom.addPrimitive(prim)

        # add geom to node
        node.addGeom(geom)

        # create nodepath
        node_path = render.attachNewNode(node)

        # set texture from both sides
        node_path.setTwoSided(True)

        return node_path
