#!/usr/bin/python3
#coding: utf-8

import logging
logger = logging.getLogger(__name__)

import numpy as np
from panda3d.core import *

from openmm68.resource.resourcesystem import RESOURCE_SYSTEM

class Terrain(object):
    def __init__(self, heightmap, tilemap, texture_list=[], heightmap_scale=16, tile_size=(1,1)):
        """
        Input:
            heightmap - numpy matrix with tile height, shape must be bigger then number of tiles by 1
            tilemap - numpy matrix with indexes of tile textures, shape must be smaller by 1 then heightmap shape
            texture_list - indexed list of texture names, [{"name1":"", "name2":""}, ...]
            heightmap_scale - height size of one panda3d unit => height = heightmap[y, x]/heightmap_scale
                Default height scale in Might and Magic games is 16.
            tile_size - size (x, y) of one tile in panda3d units
        """
        self.heightmap = heightmap # [y,x] indexed
        self.tilemap = tilemap # [y,x] indexed
        self.texture_list = texture_list
        self.heightmap_scale = float(heightmap_scale)
        self.tile_size = tile_size

        if tuple(x-1 for x in self.heightmap.shape) != self.tilemap.shape:
            raise Exception("Uncompatible heightmap and tilemap shape! [%ix%i]-1 != [%ix%i]" % (self.heightmap.shape + self.tilemap.shape))

    def getSize(self):
        return (self.tilemap.shape[0]*self.tile_size[0], self.tilemap.shape[1]*self.tile_size[1])

    def getNodePath(self):
        # init geoms, one for every used texture type
        geoms = {}
        for gi in np.unique(np.array(self.tilemap)):
            gvd = GeomVertexData('gvd%i' % gi, GeomVertexFormat.getV3n3t2(), Geom.UHStatic)
            geom = Geom(gvd)
            prim = GeomTriangles(Geom.UHStatic)
            vertex = GeomVertexWriter(gvd, 'vertex')
            normal = GeomVertexWriter(gvd, 'normal')
            texcoord = GeomVertexWriter(gvd, 'texcoord')
            geoms[gi] = {'geom':geom, 'prim':prim, 'gvd':gvd,
                'vertex':vertex, 'normal':normal,'texcoord':texcoord,
                'count':0, # number of rows in vertex data
                }

        # process gemetry and fill geoms
        max_y = self.tilemap.shape[0]
        max_x = self.tilemap.shape[1]
        # Numpy arrays are indexed [y, x] with 0,0 in top left corner
        # Panda3D has 0,0 in bottom left corner
        # MM has 0,0 at the center of the map
        for y in range(0, max_y):
            for x in range(0, max_x):
                ## get heights from heightmap
                # tile cordinate points, local 0,0 cordinates at 'a'
                # a b
                # c d
                ah = self.heightmap[y,   x]   / self.heightmap_scale
                bh = self.heightmap[y,   x+1] / self.heightmap_scale
                ch = self.heightmap[y+1, x]   / self.heightmap_scale
                dh = self.heightmap[y+1, x+1] / self.heightmap_scale

                # get tile texture
                texture_id = self.tilemap[y,x]

                # compute and save vertexes and normals
                panda3d_y = max_y-y # panda has 0,0 in bottom left corner

                ### First triangle
                vertices = [
                    (x*self.tile_size[0]-((max_x/2)*self.tile_size[0]),     panda3d_y*self.tile_size[1]-((max_y/2)*self.tile_size[1]), ah),
                    ((x+1)*self.tile_size[0]-((max_x/2)*self.tile_size[0]), (panda3d_y-1)*self.tile_size[1]-((max_y/2)*self.tile_size[1]), dh),
                    (x*self.tile_size[0]-((max_x/2)*self.tile_size[0]),     (panda3d_y-1)*self.tile_size[1]-((max_y/2)*self.tile_size[1]), ch)
                    ]
                norm_dir = np.cross( tuple(c-a for c, a in zip(vertices[2], vertices[0])), tuple(b-a for b, a in zip(vertices[1], vertices[0])) )
                norm = norm_dir / np.linalg.norm(norm_dir)

                # np 0,0, panda 0,1
                geoms[texture_id]['vertex'].addData3f(*vertices[0])
                geoms[texture_id]['normal'].addData3f(*norm)
                geoms[texture_id]['texcoord'].addData2f(0, 1)

                # np 0,1, panda 0,0
                geoms[texture_id]['vertex'].addData3f(*vertices[2])
                geoms[texture_id]['normal'].addData3f(*norm)
                geoms[texture_id]['texcoord'].addData2f(0, 0)

                # np 1,1, panda 1,0
                geoms[texture_id]['vertex'].addData3f(*vertices[1])
                geoms[texture_id]['normal'].addData3f(*norm)
                geoms[texture_id]['texcoord'].addData2f(1, 0)

                ### Second triangle
                vertices = [
                    ((x+1)*self.tile_size[0]-((max_x/2)*self.tile_size[0]), panda3d_y*self.tile_size[1]-((max_y/2)*self.tile_size[1]), bh),
                    (x*self.tile_size[0]-((max_x/2)*self.tile_size[0]),     panda3d_y*self.tile_size[1]-((max_y/2)*self.tile_size[1]), ah),
                    ((x+1)*self.tile_size[0]-((max_x/2)*self.tile_size[0]), (panda3d_y-1)*self.tile_size[1]-((max_y/2)*self.tile_size[1]), dh)
                    ]
                norm_dir = np.cross( tuple(b-a for b, a in zip(vertices[1], vertices[0])), tuple(c-a for c, a in zip(vertices[2], vertices[0])) )
                norm = norm_dir / np.linalg.norm(norm_dir)

                # np 1,1, panda 1,0
                geoms[texture_id]['vertex'].addData3f(*vertices[2])
                geoms[texture_id]['normal'].addData3f(*norm)
                geoms[texture_id]['texcoord'].addData2f(1, 0)

                # np 1,0, panda 1,1
                geoms[texture_id]['vertex'].addData3f(*vertices[0])
                geoms[texture_id]['normal'].addData3f(*norm)
                geoms[texture_id]['texcoord'].addData2f(1, 1)

                # np 0,0, panda 0,1
                geoms[texture_id]['vertex'].addData3f(*vertices[1])
                geoms[texture_id]['normal'].addData3f(*norm)
                geoms[texture_id]['texcoord'].addData2f(0, 1)

                # count: index displace, becouse we use one vertex pool for the all geoms
                count = geoms[texture_id]['count']
                geoms[texture_id]['prim'].addVertices(count, count + 1, count + 2)
                geoms[texture_id]['prim'].addVertices(count +3, count+4, count + 5)
                geoms[texture_id]['count'] += 6

        # Close a primitive, add them to the geom and add the texture attribute
        node = GeomNode('gnode')
        geom_index = 0
        for gi in geoms:
            geoms[gi]['prim'].closePrimitive()
            geoms[gi]['geom'].addPrimitive(geoms[gi]['prim'])
            node.addGeom(geoms[gi]['geom'])

            # set geom texture
            tex = self.getTileTexture(gi)
            node.setGeomState(geom_index, node.getGeomState(geom_index).addAttrib(TextureAttrib.make(tex)))
            geom_index += 1

        # create nodepath
        node_path = render.attachNewNode(node)

        # flatten created model - fixes some performance issues
        node_path.flattenStrong()

        return node_path

    def getTileTexture(self, texture_id):
        texman = RESOURCE_SYSTEM.getTextureManager()
        if texture_id >= len(self.texture_list):
            tex =texman.getPlaceholderTexture()
        else:
            tex_name = "bitmaps/"+self.texture_list[texture_id]["name1"]
            if self.texture_list[texture_id]["name2"] is not None:
                tex_name_backgound = "bitmaps/"+self.texture_list[texture_id]["name2"]
            else:
                tex_name_backgound = None
            tex = texman.getTexture(tex_name, tex_name_backgound)
        return tex

    def loadTextures(self):
        for i in np.unique(np.array(self.tilemap)):
            texture = self.getTileTexture(i)
            # fix thin border bug (at sides of img), https://www.panda3d.org/manual/index.php/Texture_Wrap_Modes
            texture.setWrapU(Texture.WM_clamp)
            texture.setWrapV(Texture.WM_clamp)



