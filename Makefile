
APTGET_RUN_DEP=python3 python3-pip python3-numpy
PIP_RUN_DEP=Pillow
# Requires Panda3D manually compilled for Python3

APTGET_TEST_DEP=python3-nose python3-coverage
PIP_TEST_DEP=

ARCH=$(shell arch)
VERSION=$(shell python3 -c "import openmm68; print(openmm68.__version__)")

help:
	@echo "clean -> Will remove some files crated by install"
	@echo "install -> Will install application on system"
	@echo "uninstall -> Will uninstall application from system (does not uninstall dependencies)"
	@echo "install_dep -> Will install application dependencies with apt-get and pip"
	@echo "install_test_dep -> Will install dependencies for testing with apt-get and pip"
	@echo "test -> Will run tests"

all: clean install_dep install install_test_dep

clean:
	sudo rm -rf build dist

install: clean
	sudo python3 setup.py build install

uninstall:
	sudo pip3 uninstall -y openmm68

reinstall: uninstall install

install_dep:
	sudo apt-get install $(APTGET_RUN_DEP)
	sudo pip3 install $(PIP_RUN_DEP)

install_test_dep:
	sudo apt-get install $(APTGET_TEST_DEP)
	#sudo pip3 install $(PIP_TEST_DEP)

test:
	# Dynamicaly adds development folder to python path, for correct sub-package imports in development.
	PYTHONPATH=. nosetests3 -v --nocapture --with-coverage

## Stuff for development only
#############################

DATA_FILES=Data_all/bitmaps.lod Data_all/sprites.lod Data_all/EnglishD.lod Data_all/EnglishT.lod Data_all/games.lod Data_all/icons.lod Data_all/new.lod Data_all/sprites.lod "Data_all/00 patch.bitmaps.lod"
DATA_FILES_UP=../Data_all/bitmaps.lod ../Data_all/sprites.lod ../Data_all/EnglishD.lod ../Data_all/EnglishT.lod ../Data_all/games.lod ../Data_all/icons.lod ../Data_all/new.lod ../Data_all/sprites.lod "../Data_all/00 patch.bitmaps.lod"

run:
	# Dynamicaly adds development folder to python path, for correct sub-package imports in development.
	PYTHONPATH=. python3 -m openmm68 $(DATA_FILES) -d 20
run_debug:
	# Dynamicaly adds development folder to python path, for correct sub-package imports in development.
	PYTHONPATH=. python3 -m openmm68 $(DATA_FILES) -d 10

run_evt_decompile:
	# Dynamicaly adds development folder to python path, for correct sub-package imports in development.
	mkdir -p __EVT_DECOMPILED__
	cd __EVT_DECOMPILED__ ; PYTHONPATH=.. python3 ../openmm68/scriptengine/evt_decompiler.py $(DATA_FILES_UP) -d 20 --allevtfiles --decompile
