#!/usr/bin/python3
#coding: utf-8

import unittest
from nose.plugins.attrib import attr

import sys, os
import tempfile, shutil

from openmm68.vfs.archive import Archive as VFSArchive
from openmm68.vfs.filesystemarchive import FilesystemArchive as FSA
from openmm68.vfs.vfsmanager import VFSManager

filesystem_files = [ # path, data, type
    ["archive1/f1", 0, None],
    ["archive1/f2.wav", 1, "wav"],
    ["archive1/f3.txt.", 2, None],
    ["archive1/dir1/f4", 3, None],
    ["archive1/dir2/f5", 4, None],
    ["archive2/f1", 5, None],
    ["archive2/DIR1/f4", 6, None],
    ["archive2/DIR1/f6", 7, None],
    ["archive3/F3.TxT.", 8, None],
    ]
filesystem_result = [
    ["f1", 5, None],
    ["f2.wav", 1, "wav"],
    ["F3.TxT.", 8, None],
    ["dir1/f4", 6, None],
    ["DIR1/f6", 7, None],
    ["dir2/f5", 4, None],
    ]

class VFSTest(unittest.TestCase):

    def buildTestFilesystem(self):
        tmp_dir = tempfile.mkdtemp()

        for f in filesystem_files:
            full_path = os.path.join(tmp_dir, f[0])
            if not os.path.exists(os.path.dirname(full_path)):
                os.makedirs(os.path.dirname(full_path))
            with open(full_path, 'w') as af:
                af.write(str(f[1]))

        return tmp_dir

    def Archive_test(self):
        a = VFSArchive()
        with self.assertRaises(NotImplementedError):
            a.open("")
        with self.assertRaises(NotImplementedError):
            a.getPath()
        with self.assertRaises(NotImplementedError):
            a.exists("")
        with self.assertRaises(NotImplementedError):
            a.get("")
        with self.assertRaises(NotImplementedError):
            a.getResourceIndex()

    def FilesystemArchive_test(self):
        # create tempdr
        tmp_dir = self.buildTestFilesystem()

        # test open()
        fsa = FSA(tmp_dir)

        # test getPath()
        self.assertTrue( tmp_dir == fsa.getPath() )

        # test getResourceIndex()
        ri = fsa.getResourceIndex()
        for file_info in filesystem_files:
            self.assertTrue( file_info[0] in ri )
            self.assertTrue( file_info[2] == ri[file_info[0]]["type"] )

        # test get() + exists()
        bds = fsa.get(filesystem_files[1][0])
        self.assertTrue( bds.read(1) == str(filesystem_files[1][1]) )

        # remove tempdir
        shutil.rmtree(tmp_dir)


    def Manager_test(self):
        # create tempdr
        tmp_dir = self.buildTestFilesystem()

        # init manager and add archives
        man = VFSManager()
        man.addArchive(FSA(os.path.join(tmp_dir, "archive1")))
        man.addArchive(FSA(os.path.join(tmp_dir, "archive2")))
        man.addArchive(FSA(os.path.join(tmp_dir, "archive3")))

        # build index
        man.buildIndex()

        # test get(), getNormalized, exists(), normalizePath()
        for result in filesystem_result:
            file_stream = man.get(result[0])
            file_type = man.getType(result[0])
            self.assertTrue( file_stream.read(1) == str(result[1]) )
            self.assertTrue( file_type == result[2] )

        # remove tempdir
        shutil.rmtree(tmp_dir)
