#!/usr/bin/env python3
# encoding: utf-8

from setuptools import setup, find_packages
import os

import openmm68

setup(name='openmm68',
        version = openmm68.__version__,
        description = 'OpenMM68 - (very experimental) attempt at recreating the engine used by "Might and Magic VI", "Might and Magic VII", "Might and Magic VIII" games',
        long_description = open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
        author = 'Jiří Kuneš',
        author_email = 'jirka642@gmail.com',
        url = 'https://gitlab.com/kunesj/openmm68',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: End Users/Desktop',
            'Intended Audience :: Other Audience',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Natural Language :: English',
            'Operating System :: OS Independent',
            'Programming Language :: Python :: 3',
            'Topic :: Games/Entertainment',
            'Topic :: Games/Entertainment :: Role-Playing',
            'Topic :: Utilities',
        ],
        packages = find_packages(),
        package_data = {
            'openmm68.renderer': ['*.prc'],
        },
        license = "GPL3",
        entry_points = {
        'console_scripts': ['openmm68 = openmm68.__main__:main',
                            'openmm68-vfs = openmm68.vfs.__main__:main',
                            'openmm68-resource = openmm68.resource.__main__:main',
                            'openmm68-lod = openmm68.lod.__main__:main',
                            ],
        },
        install_requires = [
          'setuptools',
          'numpy',
          'Pillow'
          # panda3d - compilled for Python3
        ],
    )
